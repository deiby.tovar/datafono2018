package com.cofrem.transacciones.models.pojo;

public class Configuracion {

    private String codigo;
    private String host;
    private int port;

    public Configuracion() {
    }

    public Configuracion(String codigo, String host, int port) {
        this.codigo = codigo;
        this.host = host;
        this.port = port;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}