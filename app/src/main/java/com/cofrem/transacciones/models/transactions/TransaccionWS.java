package com.cofrem.transacciones.models.transactions;

import android.util.Log;

import com.cofrem.transacciones.core.global.KeyResponse;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class TransaccionWS {

    private String statusTransaction;
    private String messageTransaction;
    private JsonObject dataTransaction;

    public TransaccionWS(JsonObject responseTransaction) {

        this.statusTransaction = responseTransaction.get(KeyResponse.KEY_STATUS).getAsString();

        this.messageTransaction = responseTransaction.get(KeyResponse.KEY_MESSAGE).getAsString();

        try {

            this.dataTransaction = responseTransaction.getAsJsonObject(KeyResponse.KEY_DATA);

        } catch (JsonParseException jsonException) {

            Log.e("JsonParseException", jsonException.getMessage());

        }

    }


    public String getStatusTransaction() {
        return statusTransaction;
    }

    public String getMessageTransaction() {
        return messageTransaction;
    }

    public JsonObject getDataTransaction() {
        return dataTransaction;
    }

}
