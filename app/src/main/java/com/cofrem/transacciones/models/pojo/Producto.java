package com.cofrem.transacciones.models.pojo;

import com.cofrem.transacciones.core.utils.CofremUtilsDates;

public class Producto {

    private String id;
    private String nombre;
    private String descripcion;
    private String registro;
    private String estado;

    public Producto() {
    }

    public Producto(String id, String nombre, String descripcion, String registro, String estado) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.registro = registro;
        this.estado = estado;
    }

    /**
     * Registro inicial de productos
     *
     * @return
     */
    public static Producto[] initialProductos() {

        Producto[] initialProductos = new Producto[6];

        initialProductos[0] = new Producto("1", "CUPO ROTATIVO", "Producto cupo rotativo Cofrem", CofremUtilsDates.todayNow(), "1");
        initialProductos[1] = new Producto("2", "BONO DE BIENESTAR", "Producto bono de bienestar Cofrem", CofremUtilsDates.todayNow(), "1");
        initialProductos[2] = new Producto("3", "STRIPE REGALO", "Producto tarjeta regalo Cofrem", CofremUtilsDates.todayNow(), "1");
        initialProductos[3] = new Producto("4", "BONO ALIMENTARIO FOSFEC", "Producto bono alimentario fosfec Cofrem", CofremUtilsDates.todayNow(), "1");
        initialProductos[4] = new Producto("5", "CUOTA MONETARIA FOSFEC", "Producto cuota monetaria fosfec Cofrem", CofremUtilsDates.todayNow(), "1");
        initialProductos[5] = new Producto("6", "CUOTA MONETARIA", "Producto cuota monetaria Cofrem", CofremUtilsDates.todayNow(), "1");

        return initialProductos;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
