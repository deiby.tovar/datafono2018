package com.cofrem.transacciones.models.modelsWS;

/**
 * Created by luisp on 13/10/2017.
 */

public class ApiWS {

    public static final int CODIGO_CAMBIO_CLAVE = 0;
    public static final int CODIGO_TARJETA_INACTIVA = 1;
    public static final int CODIGO_TARJETA_INVALIDA = 2;
    public static final int CODIGO_TERMINAL_NO_EXISTE = 3;
    public static final int CODIGO_PASSWORD_INCORECTA = 4;
    public static final int CODIGO_DOCUMENTO_INCORRECTO = 5;
    public static final int CODIGO_PASSWORD_FORMATO_INCORRECTO = 6;
    public static final int CODIGO_ERROR_EJECUCION = 7;
    public static final int CODIGO_TERMINAL_INACTIVA = 8;
    public static final int CODIGO_TRANSACCION_INSUFICIENTE = 9;
    public static final int CODIGO_NUMERO_TRANSACCION_INVALIDO = 10;
    public static final int CODIGO_NUMERO_TRANSACCION_NO_CORRESPONDE = 11;
    public static final int CODIGO_FECHA_INVALIDA = 12;

}
