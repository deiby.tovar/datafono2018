package com.cofrem.transacciones.models.pojo;

public class Consumo {

    private String estado;
    private String numero_transaccion;
    private String fecha;
    private String hora;
    private String codigoTerminal;
    private String numeroTarjeta;
    private String tipoTransaccion;
    private String valor;
    private String detalleServicio;
    private String nombres;
    private String cedula;

    public Consumo() {
    }

    public Consumo(String estado,
                   String numero_transaccion, String fecha,
                   String hora,
                   String codigoTerminal,
                   String numeroTarjeta,
                   String tipoTransaccion,
                   String valor,
                   String detalleServicio,
                   String nombres,
                   String cedula) {
        this.estado = estado;
        this.numero_transaccion = numero_transaccion;
        this.fecha = fecha;
        this.hora = hora;
        this.codigoTerminal = codigoTerminal;
        this.numeroTarjeta = numeroTarjeta;
        this.tipoTransaccion = tipoTransaccion;
        this.valor = valor;
        this.detalleServicio = detalleServicio;
        this.nombres = nombres;
        this.cedula = cedula;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNumero_transaccion() {
        return numero_transaccion;
    }

    public void setNumero_transaccion(String numero_transaccion) {
        this.numero_transaccion = numero_transaccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getCodigoTerminal() {
        return codigoTerminal;
    }

    public void setCodigoTerminal(String codigoTerminal) {
        this.codigoTerminal = codigoTerminal;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDetalleServicio() {
        return detalleServicio;
    }

    public void setDetalleServicio(String detalleServicio) {
        this.detalleServicio = detalleServicio;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
}
