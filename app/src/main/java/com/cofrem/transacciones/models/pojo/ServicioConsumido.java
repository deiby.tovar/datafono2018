package com.cofrem.transacciones.models.pojo;

/**
 * Created by luispineda on 12/07/17.
 */

public class ServicioConsumido {

    private String servicio;
    private String nombreServicio;
    private String valorConsumido;

    public ServicioConsumido() {
    }

    public ServicioConsumido(String servicio, String nombreServicio, String valorConsumido) {
        this.servicio = servicio;
        this.nombreServicio = nombreServicio;
        this.valorConsumido = valorConsumido;
    }

    public String getCodigo_servicio() {
        return servicio;
    }

    public void setCodigo_servicio(String servicio) {
        this.servicio = servicio;
    }

    public String getDescripcion() {
        return nombreServicio;
    }

    public void setDescripcion(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public String getSaldo() {
        return valorConsumido;
    }

    public void setSaldo(String valorConsumido) {
        this.valorConsumido = valorConsumido;
    }
}
