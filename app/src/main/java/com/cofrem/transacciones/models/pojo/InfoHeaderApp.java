package com.cofrem.transacciones.models.pojo;

public class InfoHeaderApp {

    private static InfoHeaderApp instance = null;
    /**
     * Modelo para el registro del InfoHeaderApp
     */
    private String codigoDispositivo = "NO REGISTRA";
    private String idSucursal = "NO REGISTRA";
    private String razonSocial = "NO REGISTRA";
    private String nombreSucursal = "NO REGISTRA";

    protected InfoHeaderApp() {
    }

    public static InfoHeaderApp getInstance() {
        if (instance == null) {
            instance = new InfoHeaderApp();
        }
        return instance;
    }

    public String getCodigoDispositivo() {
        return codigoDispositivo;
    }

    public void setCodigoDispositivo(String codigoDispositivo) {
        this.codigoDispositivo = codigoDispositivo;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }
}
