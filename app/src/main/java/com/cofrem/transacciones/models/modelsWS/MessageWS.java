package com.cofrem.transacciones.models.modelsWS;

import org.ksoap2.serialization.SoapObject;

public class MessageWS {


    //Modelo usado en la respuesta del WS para la respuestas

    //Modelado de Properties y Keys
    public static final String PROPERTY_MESSAGE = "mensaje";
    public static final String PROPERTY_TRANSAC_LISTS = "transacListResult";
    public static final String KEY_CODIGO_MESSAGE = "codigoMensaje";
    public static final String KEY_DETALLE_MESSAGE = "detalleMensaje";

    /**
     * STATUS METHOD TERMINAL
     */
    /**
     * ## TABLA ENTREGADA POR COFREM ########
     * ## Version 2017/07/27 ################
     * Valores posibles que puede tomar el atributo tipo encriptacion
     * 00	TRANSACCIÓN EXITOSA
     * 01	TARJETAHABIENTE NO EXISTE
     * 02	CLAVE ERRARA
     * 03	TARJETAHABIENTE INACTIVO
     * 04	TARJETAHABIENTE EN MORA
     * 05	TARJETAHABIENTE SIN CUPO DISPONIBLE
     * 06	EMPRESA NO EXISTE
     * 07	EMPRESA INACTIVA
     * 08	EMPRESA EN MORA
     * 09	TRANSACCIÓN NO ENCONTRADA
     * 10	CEDULA Y STRIPE NO EXISTE
     * 11	CONSULTA EXITOSA
     * 12	TERMINAL NO EXISTE
     * 13	TERMINAL YA EXISTE
     * 14	PUNTO NO EXISTE
     * 15	…
     * 16	LA TERMINAL YA TIENE UN UUID ASIGNADO
     * 17	UUID YA EXISTE EN OTRA TERMINAL
     * 18	UUID NO VALIDO
     * 19	LA TERMINAL YA TIENE UNA MAC ASIGNADA
     * 20	LA MAC YA EXISTE EN OTRA TERMINAL
     * 21	MAC NO VALIDO
     * 22	NO SE PUDO ACTUALIZAR LA INFORMACIÓN
     * 30	STRIPE NO PERMITIDA EN ESTA TERMINAL
     * 98	NO HAY CONEXIÓN CON LA BASE DE DATOS
     * 99	ERROR INTERSECTADO: ERROR EXCEPCIÓN
     */
    public static final int statusTransaccionExitosa = 0;
    public static final int statusTarjetaHabienteNoExiste = 1;
    public static final int statusClaveErrada = 2;
    public static final int statusTarjetHabienteInactivo = 3;
    public static final int statusTarjetaHabienteMora = 4;
    public static final int statusTarjetaHabienteSinCupoDisponible = 5;
    public static final int statusEmpresaNoExiste = 6;
    public static final int statusEmpresaInactiva = 7;
    public static final int statusEmpresaMora = 8;
    public static final int statusTransaccionNoEncontrada = 9;
    public static final int statusCedulaTarjetaNoExiste = 10;
    public static final int statusConsultaExitosa = 11;
    public static final int statusTerminalNoExiste = 12;
    public static final int statusTerminalExiste = 13;
    public static final int statusPuntoNoExiste = 14;
    public static final int statusEmpty = 15;
    public static final int statusTerminalUUIDYaAsignado = 16;
    public static final int statusTerminalUUIDExistente = 17;
    public static final int statusTerminalUUIDNoValido = 18;
    public static final int statusTerminalMACYaAsignada = 19;
    public static final int statusTerminalMACExistente = 20;
    public static final int statusTerminalMACNoValida = 21;
    public static final int statusActualizacionNoRealizada = 22;
    public static final int statusTarjetaNoPermitidaEnTerminal = 30;
    public static final int statusTransaccionYaAnulada = 30;
    public static final int statusTransaccionNoFechaActual = 32;
    public static final int statusTransaccionEstasdoDiferente = 36;
    public static final int statusErrorDatabase = 98;
    public static final int statusTerminalErrorException = 99;

    //Informacion del detalleMensaje
    private int codigoMensaje;
    private String detalleMensaje;

    /**
     * Constructor de la clase que recibe un Objeto tipo SoapObject
     *
     * @param soap
     */
    public MessageWS(SoapObject soap) {

        this.codigoMensaje = soap.getPropertyAsString(KEY_CODIGO_MESSAGE).equals("anyType{}") ? statusTransaccionExitosa : Integer.parseInt(soap.getPropertyAsString(KEY_CODIGO_MESSAGE));

        this.detalleMensaje = soap.getPropertyAsString(KEY_DETALLE_MESSAGE).equals("anyType{}") ? "" : soap.getPropertyAsString(KEY_DETALLE_MESSAGE);

    }

    public int getCodigoMensaje() {
        return codigoMensaje;
    }

    public void setCodigoMensaje(int codigoMensaje) {
        this.codigoMensaje = codigoMensaje;
    }

    public String getDetalleMensaje() {
        return detalleMensaje;
    }

    public void setDetalleMensaje(String detalleMensaje) {
        this.detalleMensaje = detalleMensaje;
    }
}