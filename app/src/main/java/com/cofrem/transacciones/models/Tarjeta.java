package com.cofrem.transacciones.models;

/**
 * Created by luisp on 11/10/2017.
 */

public class Tarjeta {

    private String numDocumento;
    private String numTarjeta;
    private String passActual;
    private String passNueva;

    public Tarjeta() {
    }

    public Tarjeta(String numDocumento,
                   String numTarjeta,
                   String passActual,
                   String passNueva
    ) {
        this.numDocumento = numDocumento;
        this.numTarjeta = numTarjeta;
        this.passActual = passActual;
        this.passNueva = passNueva;
    }

    public String getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }

    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getPassActual() {
        return passActual;
    }

    public void setPassActual(String passActual) {
        this.passActual = passActual;
    }

    public String getPassNueva() {
        return passNueva;
    }

    public void setPassNueva(String passNueva) {
        this.passNueva = passNueva;
    }
}
