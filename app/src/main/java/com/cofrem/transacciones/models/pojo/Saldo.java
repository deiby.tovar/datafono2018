package com.cofrem.transacciones.models.pojo;

import android.graphics.drawable.Drawable;

public class Saldo {

    private String codigo_servicio;
    private String servicio;
    private String saldo;
    private Drawable imagen;

    public Saldo() {
    }

    public Saldo(String codigo_servicio, String servicio, String saldo) {
        this.codigo_servicio = codigo_servicio;
        this.servicio = servicio;
        this.saldo = saldo;
    }

    public Saldo(String codigo_servicio, String servicio, String saldo, Drawable imagen) {
        this.codigo_servicio = codigo_servicio;
        this.servicio = servicio;
        this.saldo = saldo;
        this.imagen = imagen;
    }

    public String getCodigo_servicio() {
        return codigo_servicio;
    }

    public void setCodigo_servicio(String codigo_servicio) {
        this.codigo_servicio = codigo_servicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public Drawable getImagen() {
        return imagen;
    }

    public void setImagen(Drawable imagen) {
        this.imagen = imagen;
    }
}
