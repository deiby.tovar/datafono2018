package com.cofrem.transacciones.models.pojo;

public class InfoTransaccion {

    private String numero_transaccion;
    private String fecha;
    private String hora;
    private String codigoTerminal;
    private String numeroTarjeta;
    private String tipoTransaccion;
    private String valor;
    private String nombres;
    private String cedula;
    private ServicioConsumido[] serviciosConsumidos;

    public InfoTransaccion() {
    }

    public InfoTransaccion(String numero_transaccion,
                           String fecha,
                           String hora,
                           String codigoTerminal,
                           String numeroTarjeta,
                           String tipoTransaccion,
                           String valor,
                           String nombres,
                           String cedula,
                           ServicioConsumido[] serviciosConsumidos) {
        this.numero_transaccion = numero_transaccion;
        this.fecha = fecha;
        this.hora = hora;
        this.codigoTerminal = codigoTerminal;
        this.numeroTarjeta = numeroTarjeta;
        this.tipoTransaccion = tipoTransaccion;
        this.valor = valor;
        this.nombres = nombres;
        this.cedula = cedula;
        this.serviciosConsumidos = serviciosConsumidos;
    }

    public String getNumero_transaccion() {
        return numero_transaccion;
    }

    public void setNumero_transaccion(String numero_transaccion) {
        this.numero_transaccion = numero_transaccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getCodigoTerminal() {
        return codigoTerminal;
    }

    public void setCodigoTerminal(String codigoTerminal) {
        this.codigoTerminal = codigoTerminal;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public ServicioConsumido[] getServiciosConsumidos() {
        return serviciosConsumidos;
    }

    public void setServiciosConsumidos(ServicioConsumido[] serviciosConsumidos) {
        this.serviciosConsumidos = serviciosConsumidos;
    }
}
