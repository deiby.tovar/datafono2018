package com.cofrem.transacciones.models.pojo;

public class GrpSaldo {

    private Saldo[] saldos;

    public GrpSaldo() {
    }

    public GrpSaldo(Saldo[] saldos) {
        this.saldos = saldos;
    }

    public Saldo[] getSaldos() {
        return saldos;
    }

    public void setSaldos(Saldo[] saldos) {
        this.saldos = saldos;
    }

    public int getCantidadSaldos() {

        if (saldos != null) {
            return saldos.length;
        } else {
            return 0;
        }

    }

}
