package com.cofrem.transacciones.models.pojo;

public class GrpServicio {

    private String numero_de_registros;
    private Servicio[] servicio;

    public GrpServicio() {
    }

    public GrpServicio(String numero_de_registros, Servicio[] servicio) {
        this.numero_de_registros = numero_de_registros;
        this.servicio = servicio;
    }

    public String getNumero_de_registros() {
        return numero_de_registros;
    }

    public void setNumero_de_registros(String numero_de_registros) {
        this.numero_de_registros = numero_de_registros;
    }

    public Servicio[] getServicio() {
        return servicio;
    }

    public void setServicio(Servicio[] servicio) {
        this.servicio = servicio;
    }
}
