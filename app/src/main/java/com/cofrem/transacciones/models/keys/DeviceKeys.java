package com.cofrem.transacciones.models.keys;

public class DeviceKeys {

    public class Get {

        public static final String KEY_DEVICE_CODIGO = "codigo";
        public static final String KEY_DEVICE_PASSWORD = "password";

    }

}
