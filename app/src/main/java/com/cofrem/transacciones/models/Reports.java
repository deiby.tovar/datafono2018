package com.cofrem.transacciones.models;

public class Reports {

    /**
     * Llave del Bundle para la selección de reporte
     */
    public static final String keyReport = "reporte";

    /**
     * Reportes disponibles
     */
    public enum TYPE_REPORT {
        ULTIMO_RECIBO,
        NUM_CARGO,
        DETALLE,
        GENERAl,
        CIERRE_LOTE,
    }

}