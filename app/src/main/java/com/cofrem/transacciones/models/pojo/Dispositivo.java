package com.cofrem.transacciones.models.pojo;

public class Dispositivo {

    private String host;
    private String dispositivo;
    private String registro;
    private String estado;

    public Dispositivo() {
    }

    public Dispositivo(String host, String dispositivo, String registro, String estado) {
        this.host = host;
        this.dispositivo = dispositivo;
        this.registro = registro;
        this.estado = estado;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getCodigo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
