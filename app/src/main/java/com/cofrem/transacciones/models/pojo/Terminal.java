package com.cofrem.transacciones.models.pojo;

/**
 * Created by luispineda on 12/07/17.
 */

public class Terminal {

    private String nit;
    private String razon_social;
    private String sucursal_id;
    private String nombre_sucursal;
    private String ciudad;
    private String direccion;
    private String estado_sucursal;
    private String estado_terminal;
    private String codigo_terminal;
    private String ip1;

    public Terminal() {
    }

    public Terminal(String nit,
                    String razon_social,
                    String sucursal_id,
                    String nombre_sucursal,
                    String ciudad,
                    String direccion,
                    String estado_sucursal,
                    String estado_terminal,
                    String codigo_terminal,
                    String ip1) {
        this.nit = nit;
        this.razon_social = razon_social;
        this.sucursal_id = sucursal_id;
        this.nombre_sucursal = nombre_sucursal;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.estado_sucursal = estado_sucursal;
        this.estado_terminal = estado_terminal;
        this.codigo_terminal = codigo_terminal;
        this.ip1 = ip1;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getSucursal_id() {
        return sucursal_id;
    }

    public void setSucursal_id(String sucursal_id) {
        this.sucursal_id = sucursal_id;
    }

    public String getNombre_sucursal() {
        return nombre_sucursal;
    }

    public void setNombre_sucursal(String nombre_sucursal) {
        this.nombre_sucursal = nombre_sucursal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstado_sucursal() {
        return estado_sucursal;
    }

    public void setEstado_sucursal(String estado_sucursal) {
        this.estado_sucursal = estado_sucursal;
    }

    public String getEstado_terminal() {
        return estado_terminal;
    }

    public void setEstado_terminal(String estado_terminal) {
        this.estado_terminal = estado_terminal;
    }

    public String getCodigo_terminal() {
        return codigo_terminal;
    }

    public void setCodigo_terminal(String codigo_terminal) {
        this.codigo_terminal = codigo_terminal;
    }

    public String getIp1() {
        return ip1;
    }

    public void setIp1(String ip1) {
        this.ip1 = ip1;
    }
}
