package com.cofrem.transacciones.models.pojo;

/**
 * Created by luispineda on 12/07/17.
 */

public class Servicio {

    private String codigo_servicio;
    private String descripcion;
    private String saldo;

    public Servicio() {
    }

    public Servicio(String codigo_servicio, String descripcion, String saldo) {
        this.codigo_servicio = codigo_servicio;
        this.descripcion = descripcion;
        this.saldo = saldo;
    }

    public String getCodigo_servicio() {
        return codigo_servicio;
    }

    public void setCodigo_servicio(String codigo_servicio) {
        this.codigo_servicio = codigo_servicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}
