package com.cofrem.transacciones.database;

/**
 * Clase que maneja:
 * - Scripts de la base de datos
 * - Atributos de la base de datos
 * - DDL de la base de datos
 */

class DatabaseManager {

    // Informacion de los tipos de los campos
    private static final String STRING_TYPE = "TEXT";
    private static final String INT_TYPE = "INTEGER";
    private static final String TIMESTAMP_TYPE = "TIMESTAMP";

    //Informacion de las  caracteristicas de los campos
    private static final String PRIMARY_KEY = "PRIMARY KEY";
    private static final String AUTOINCREMENT = "AUTOINCREMENT";
    private static final String ATTR_NULL = "NULL";
    private static final String ATTR_NOT_NULL = "NOT NULL";

    /**
     * Informacion de la base de datos
     */
    static class DatabaseApp {

        static final String DATABASE_NAME = "cofrem_transactions.db";
        static final int DATABASE_VERSION = 5;

    }

    /**
     * #############################################################################################
     * TABLE_PRODUCTO
     * #############################################################################################
     */
    static class TableProducto {

        /**
         * Nombre de la tabla
         */
        static final String TABLE_PRODUCTO = "producto";

        /**
         * Columnas de la tabla
         */
        static final String COL_ID = "id";
        static final String COL_NOMBRE = "nombre";
        static final String COL_DESCRIPCION = "descripcion";
        static final String COL_REGISTRO = "registro";
        static final String COL_ESTADO = "estado";

        /**
         * Comando CREATE
         */
        static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_PRODUCTO + "(" +
                        COL_ID + " " + INT_TYPE + " " + PRIMARY_KEY + "," +
                        COL_NOMBRE + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_DESCRIPCION + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_REGISTRO + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_ESTADO + " " + INT_TYPE + " " + ATTR_NOT_NULL + ")";

        /**
         * Comando DROP
         */
        static final String DROP_TABLE =
                "DROP TABLE IF EXISTS '" + TABLE_PRODUCTO + "'";
    }

    /**
     * #############################################################################################
     * Tabla Transacciones:
     * - Modelado de la tabla producto
     * - Scripts de la tabla producto
     * #############################################################################################
     */
    static class TableTransacciones {

        /**
         * Nombre de la tabla
         */
        static final String TABLE_TRANSACCIONES = "transacciones";

        /**
         * Columnas de la tabla
         */
        static final String COL_ID = "id";
        static final String COL_SERVICIOS = "servicios";
        static final String COL_NUMERO_CARGO = "numero_cargo";
        static final String COL_NUMERO_TARJETA = "numero_tarjeta";
        static final String COL_VALOR = "valor";
        static final String COL_TIPO_TRANSACCION = "tipo_transaccion";
        static final String COL_CEDULA_USUARIO = "cedula_usuario";
        static final String COL_NOMBRE_USUARIO = "nombre_usuario";
        static final String COL_FECHA_SERVER = "fecha_server";
        static final String COL_HORA_SERVER = "hora_server";
        static final String COL_REGISTRO = "registro";
        static final String COL_ESTADO = "estado";

        /**
         * Comando CREATE
         */
        static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_TRANSACCIONES + "(" +
                        COL_ID + " " + INT_TYPE + " " + PRIMARY_KEY + " " + AUTOINCREMENT + "," +
                        COL_SERVICIOS + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_NUMERO_CARGO + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_NUMERO_TARJETA + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_VALOR + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_TIPO_TRANSACCION + " " + INT_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_CEDULA_USUARIO + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_NOMBRE_USUARIO + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_FECHA_SERVER + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_HORA_SERVER + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_REGISTRO + " " + TIMESTAMP_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_ESTADO + " " + INT_TYPE + " " + ATTR_NOT_NULL + ")";

        /**
         * Comando DROP
         */
        static final String DROP_TABLE =
                "DROP TABLE IF EXISTS '" + TABLE_TRANSACCIONES + "'";
    }

    static class TableTerminal {

        /**
         * Nombre de la tabla
         */
        static final String TABLE_TERMINAL = "terminal";

        /**
         * Columnas de la tabla
         */
        static final String COL_NIT = "nit";
        static final String COL_RAZON_SOCIAL = "razon_social";
        static final String COL_SUCURSAL_ID = "sucursal_id";
        static final String COL_NOMBRE_SUCURSAL = "nombre_sucursal";
        static final String COL_CIUDAD = "ciudad";
        static final String COL_DIRECCION = "direccion";
        static final String COL_ESTADO_SUCURSAL = "estado_sucursal";
        static final String COL_ESTADO_TERMINAL = "estado_terminal";
        static final String COL_CODIGO_TERMINAL = "codigo_terminal";
        static final String COL_IP = "ip1";


        /**
         * Comando CREATE
         */
        static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_TERMINAL + "(" +
                        COL_NIT + " " + STRING_TYPE + " " + ATTR_NULL + "," +
                        COL_RAZON_SOCIAL + " " + STRING_TYPE + " " + ATTR_NULL + "," +
                        COL_SUCURSAL_ID + " " + STRING_TYPE + " " + ATTR_NULL + "," +
                        COL_NOMBRE_SUCURSAL + " " + STRING_TYPE + " " + ATTR_NULL + "," +
                        COL_CIUDAD + " " + STRING_TYPE + " " + ATTR_NULL + "," +
                        COL_DIRECCION + " " + STRING_TYPE + " " + ATTR_NULL + "," +
                        COL_ESTADO_SUCURSAL + " " + TIMESTAMP_TYPE + " " + ATTR_NULL + "," +
                        COL_ESTADO_TERMINAL + " " + TIMESTAMP_TYPE + " " + ATTR_NULL + "," +
                        COL_CODIGO_TERMINAL + " " + TIMESTAMP_TYPE + " " + ATTR_NULL + "," +
                        COL_IP + " " + INT_TYPE + " " + ATTR_NULL + ")";

        /**
         * Comando DROP
         */
        static final String DROP_TABLE =
                "DROP TABLE IF EXISTS '" + TABLE_TERMINAL + "'";
    }

    /**
     * #############################################################################################
     * Tabla ConfiguraciónConexión:
     * - Modelado de la tabla producto
     * - Scripts de la tabla producto
     * #############################################################################################
     */
    static class TableConfiguracion {

        /**
         * Nombre de la tabla
         */
        static final String TABLE_CONFIGURACION = "configuracion";

        /**
         * Columnas de la tabla
         */
        static final String COL_HOST = "host";
        static final String COL_CODIGO = "dispositivo";
        static final String COL_PORT = "port";

        /**
         * Comando CREATE
         */
        static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_CONFIGURACION + "(" +
                        COL_HOST + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_CODIGO + " " + STRING_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_PORT + " " + STRING_TYPE + " " + ATTR_NOT_NULL + ")";

        /**
         * Comando DROP
         */
        static final String DROP_TABLE =
                "DROP TABLE IF EXISTS '" + TABLE_CONFIGURACION + "'";
    }

    /**
     * #############################################################################################
     * TABLE_PRINTER
     * #############################################################################################
     */
    static class TablePrinter {

        /**
         * Nombre de la tabla
         */
        static final String TABLE_PRINTER = "printer";

        /**
         * Columnas de la tabla
         */
        static final String COL_FONT_SIZE = "font_size";
        static final String COL_GRAY_LEVEL = "gray_level";


        /**
         * Comando CREATE
         */
        static final String CREATE_TABLE =
                "CREATE TABLE " + TABLE_PRINTER + "(" +
                        COL_FONT_SIZE + " " + INT_TYPE + " " + ATTR_NOT_NULL + "," +
                        COL_GRAY_LEVEL + " " + INT_TYPE + " " + ATTR_NOT_NULL + ")";

        /**
         * Comando DROP
         */
        static final String DROP_TABLE =
                "DROP TABLE IF EXISTS '" + TABLE_PRINTER + "'";
    }

}
