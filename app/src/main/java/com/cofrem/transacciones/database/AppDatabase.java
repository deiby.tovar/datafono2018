package com.cofrem.transacciones.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;
import com.cofrem.transacciones.models.pojo.InfoHeaderApp;
import com.cofrem.transacciones.models.modelsWS.modelEstablecimiento.ConexionEstablecimiento;
import com.cofrem.transacciones.models.pojo.Configuracion;
import com.cofrem.transacciones.models.pojo.Consumo;
import com.cofrem.transacciones.models.pojo.Producto;
import com.cofrem.transacciones.models.pojo.Terminal;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.anulacion.event.AnulacionEvent;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Clase que maneja:
 * - DML de la base de datos
 * - instancia general de acceso a datos
 */
public final class AppDatabase extends SQLiteOpenHelper {

    //Instancia singleton
    private static AppDatabase singleton;

    /**
     * Constructor de la clase
     * Crea la base de datos si no existe
     *
     * @param context instancia desde la que se llaman los metodos
     */
    private AppDatabase(Context context) {
        super(context,
                DatabaseManager.DatabaseApp.DATABASE_NAME,
                null,
                DatabaseManager.DatabaseApp.DATABASE_VERSION);
    }

    /**
     * Retorna la instancia unica del singleton
     *
     * @param context contexto donde se ejecutarán las peticiones
     * @return Instancia
     */
    public static synchronized AppDatabase getInstance(Context context) {

        if (singleton == null) {
            singleton = new AppDatabase(context.getApplicationContext());
        }
        return singleton;
    }

    /**
     * Metodo ejecutado en el evento de la instalacion de la aplicacion
     * Crea las tablas necesarias para el funcionamiento de la aplicacion
     *
     * @param db Database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DatabaseManager.TableProducto.CREATE_TABLE);
        db.execSQL(DatabaseManager.TableTransacciones.CREATE_TABLE);
        db.execSQL(DatabaseManager.TableTerminal.CREATE_TABLE);
        db.execSQL(DatabaseManager.TableConfiguracion.CREATE_TABLE);
        db.execSQL(DatabaseManager.TablePrinter.CREATE_TABLE);

    }

    /**
     * Metodo ejecutado en la actualizacion de la aplicacion
     *
     * @param db         Database
     * @param oldVersion Version Anterior
     * @param newVersion Version Actual
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        // Drop todas las tablas 
        db.execSQL(DatabaseManager.TableProducto.DROP_TABLE);
        db.execSQL(DatabaseManager.TableTransacciones.DROP_TABLE);
        db.execSQL(DatabaseManager.TableTerminal.DROP_TABLE);
        db.execSQL(DatabaseManager.TableConfiguracion.DROP_TABLE);
        db.execSQL(DatabaseManager.TablePrinter.DROP_TABLE);

        onCreate(db);
    }

    /*
    #############################################################################################
    AREA INSERCIONES
    #############################################################################################
    */

    /**
     * Metodo para insertar registro inicial en la Base de Datos de la configuracion para la impresora
     *
     * @return Boolean estado de la transaccion para el registro inicial de la configuracion de la impresora
     */
    public boolean insertInicialPrinter() {

        return insertConfigurationPrinter(
                new ConfigurationPrinter(
                        ConfigurationPrinter.default_gray_level,
                        ConfigurationPrinter.default_font_size
                )
        );

    }

    /**
     * Metodo para insertar registro de la configuracion de la Impresora
     *
     * @param configurationPrinter ConfigurationPrinter informacion de la configuracion
     * @return Boolean estado de la transaccion
     */
    public boolean insertConfigurationPrinter(ConfigurationPrinter configurationPrinter) {

        truncateTable(DatabaseManager.TablePrinter.class.getName());

        boolean transaction = false;

        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseManager.TablePrinter.COL_FONT_SIZE, configurationPrinter.getFont_size());
        contentValues.put(DatabaseManager.TablePrinter.COL_GRAY_LEVEL, configurationPrinter.getGray_level());

        if (insertData(contentValues, DatabaseManager.TablePrinter.TABLE_PRINTER)) {
            transaction = true;
        }

        return transaction;

    }

    /**
     * Metodo para manejar la informacion que se insertara en el registro inicial de productos en la Base de Datos
     *
     * @return Boolean estado del registro de los productos iniciales
     */
    public boolean insertInicialProducto() {

        truncateTable(DatabaseManager.TableProducto.class.getName());

        for (Producto producto : Producto.initialProductos())

            insertProducto(producto);

        return true;

    }

    /**
     * Metodo para registro productos en la Base de Datos
     *
     * @param producto
     * @return
     */
    private boolean insertProducto(Producto producto) {

        boolean transaction = false;

        // Inicializacion de la variable de contenidos del registro
        ContentValues contentValues = new ContentValues();

        // Almacena los valores a insertar
        contentValues.put(DatabaseManager.TableProducto.COL_ID, producto.getId());
        contentValues.put(DatabaseManager.TableProducto.COL_NOMBRE, producto.getNombre());
        contentValues.put(DatabaseManager.TableProducto.COL_DESCRIPCION, producto.getDescripcion());
        contentValues.put(DatabaseManager.TableProducto.COL_REGISTRO, producto.getRegistro());
        contentValues.put(DatabaseManager.TableProducto.COL_ESTADO, producto.getEstado());

        if (insertData(contentValues, DatabaseManager.TableProducto.TABLE_PRODUCTO))

            transaction = true;


        return transaction;
    }


    /**
     * Metodo para manejar la informacion que se insertara en el registro inicial de productos en la Base de Datos
     *
     * @return Boolean estado del registro de los productos iniciales
     */
    public boolean insertConfiguracion(Configuracion configuracion) {

        truncateTable(DatabaseManager.TableConfiguracion.class.getName());

        boolean transaction = false;

        // Inicializacion de la variable de contenidos del registro
        ContentValues contentValues = new ContentValues();

        // Almacena los valores a insertar
        contentValues.put(DatabaseManager.TableConfiguracion.COL_HOST, configuracion.getHost());
        contentValues.put(DatabaseManager.TableConfiguracion.COL_CODIGO, configuracion.getCodigo());
        contentValues.put(DatabaseManager.TableConfiguracion.COL_PORT, configuracion.getPort());

        if (insertData(contentValues, DatabaseManager.TableConfiguracion.TABLE_CONFIGURACION))

            transaction = true;


        return transaction;

    }

    /**
     * @param terminal
     * @return
     */
    public boolean insertTerminal(Terminal terminal) {

        truncateTable(DatabaseManager.TableTerminal.class.getName());

        boolean transaction = false;

        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseManager.TableTerminal.COL_NIT, terminal.getNit());
        contentValues.put(DatabaseManager.TableTerminal.COL_RAZON_SOCIAL, terminal.getRazon_social());
        contentValues.put(DatabaseManager.TableTerminal.COL_SUCURSAL_ID, terminal.getSucursal_id());
        contentValues.put(DatabaseManager.TableTerminal.COL_NOMBRE_SUCURSAL, terminal.getNombre_sucursal());
        contentValues.put(DatabaseManager.TableTerminal.COL_CIUDAD, terminal.getCiudad());
        contentValues.put(DatabaseManager.TableTerminal.COL_DIRECCION, terminal.getDireccion());
        contentValues.put(DatabaseManager.TableTerminal.COL_ESTADO_SUCURSAL, terminal.getEstado_sucursal());
        contentValues.put(DatabaseManager.TableTerminal.COL_ESTADO_TERMINAL, terminal.getEstado_terminal());
        contentValues.put(DatabaseManager.TableTerminal.COL_CODIGO_TERMINAL, terminal.getCodigo_terminal());
        contentValues.put(DatabaseManager.TableTerminal.COL_IP, terminal.getIp1());

        if (insertData(contentValues, DatabaseManager.TableTerminal.TABLE_TERMINAL))

            transaction = true;


        return transaction;

    }

    /**
     * Metodo para insertar registro de transacciones en la Base de Datos
     *
     * @param consumo Informacion de la transaccion realizada
     * @return Boolean estado del registro de la transaccion
     */
    public boolean insertRegistroTransaction(Consumo consumo, int tipoTransaccion) {

        boolean transaction = false;

        ContentValues contentValues = new ContentValues();

        // Almacena los valores a insertar
        contentValues.put(DatabaseManager.TableTransacciones.COL_SERVICIOS, consumo.getDetalleServicio());
        contentValues.put(DatabaseManager.TableTransacciones.COL_NUMERO_CARGO, consumo.getNumero_transaccion());
        contentValues.put(DatabaseManager.TableTransacciones.COL_NUMERO_TARJETA, consumo.getNumeroTarjeta());
        contentValues.put(DatabaseManager.TableTransacciones.COL_VALOR, consumo.getValor());
        contentValues.put(DatabaseManager.TableTransacciones.COL_TIPO_TRANSACCION, tipoTransaccion);
        contentValues.put(DatabaseManager.TableTransacciones.COL_CEDULA_USUARIO, consumo.getCedula());
        contentValues.put(DatabaseManager.TableTransacciones.COL_NOMBRE_USUARIO, consumo.getNombres());
        contentValues.put(DatabaseManager.TableTransacciones.COL_FECHA_SERVER, consumo.getFecha());
        contentValues.put(DatabaseManager.TableTransacciones.COL_HORA_SERVER, consumo.getHora());
        contentValues.put(DatabaseManager.TableTransacciones.COL_REGISTRO, getDateTime());
        contentValues.put(DatabaseManager.TableTransacciones.COL_ESTADO, 1);

        if (insertData(contentValues, DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES))

            transaction = true;


        return transaction;

    }


    /*
    #############################################################################################
    AREA CONSULTAS
    #############################################################################################
    */

    /**
     * Metodo para Obtener el conteo de los registros de la tabla ConfiguracionConexion
     * Usado en modulos:
     * - Configuracion
     *
     * @return int conteo de los registros
     */
    public int conteoConfiguracion() {

        int count = 0;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT COUNT(1) FROM " + DatabaseManager.TableConfiguracion.TABLE_CONFIGURACION,
                null
        );

        if (cursor.moveToFirst())

            count = cursor.getInt(0);

        cursor.close();

        return count;
    }

    /**
     * Metodo para validar si existe registro inicial en la Base de Datos para la configuracion inical de la impresora
     * Usado en modulos:
     * - Configuracion
     *
     * @return int conteo de los registros de la configuracion de la impresora
     */
    public int conteoPrinter() {

        int count = 0;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT COUNT(1) FROM " + DatabaseManager.TablePrinter.TABLE_PRINTER,
                null
        );

        if (cursor.moveToFirst())

            count = cursor.getInt(0);

        cursor.close();

        return count;

    }

    /**
     * Metodo para Obtener la informacion del dispositivo
     *
     * @return
     */
    public Configuracion getDataDispositivo() {

        Configuracion configuracion = null;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * FROM " + DatabaseManager.TableConfiguracion.TABLE_CONFIGURACION
                , null
        );

        if (cursor.moveToFirst())

            configuracion = new Configuracion(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableConfiguracion.COL_CODIGO)),
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableConfiguracion.COL_HOST)),
                    cursor.getInt(cursor.getColumnIndex(DatabaseManager.TableConfiguracion.COL_PORT))
            );

        cursor.close();

        return configuracion;
    }


    /**
     * Metodo para Obtener la configuracion de la impresora
     *
     * @return ConfigurationPrinter configuracion de la impresora registrada
     */
    public ConfigurationPrinter getConfigurationPrinter() {

        ConfigurationPrinter configurationPrinter = null;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * FROM " + DatabaseManager.TablePrinter.TABLE_PRINTER,
                null
        );

        if (cursor.moveToFirst())

            configurationPrinter = new ConfigurationPrinter(
                    cursor.getInt(cursor.getColumnIndex(DatabaseManager.TablePrinter.COL_GRAY_LEVEL)),
                    cursor.getInt(cursor.getColumnIndex(DatabaseManager.TablePrinter.COL_FONT_SIZE))
            );

        cursor.close();

        return configurationPrinter;
    }


    /**
     * Metodo para Obtener ultima  transaccion
     *
     * @return boolean informacion del header
     */
    public boolean getDataHeader() {

        boolean transaction = false;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT *  FROM " + DatabaseManager.TableTerminal.TABLE_TERMINAL,
                null
        );

        if (cursor.moveToFirst()) {

            InfoHeaderApp.getInstance().setCodigoDispositivo(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_CODIGO_TERMINAL))
            );

            InfoHeaderApp.getInstance().setIdSucursal(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_SUCURSAL_ID))
            );

            InfoHeaderApp.getInstance().setRazonSocial(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_RAZON_SOCIAL))
            );
            InfoHeaderApp.getInstance().setNombreSucursal(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_NOMBRE_SUCURSAL))
            );

            transaction = true;
        }

        cursor.close();

        return transaction;
    }

    /**
     * Metodo para Obtener la informacion de la terminal
     *
     * @return
     */
    public Terminal getTerminal() {

        Terminal terminal = new Terminal();

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * FROM " + DatabaseManager.TableTerminal.TABLE_TERMINAL, null
        );

        if (cursor.moveToFirst()) {

            terminal.setCodigo_terminal(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_CODIGO_TERMINAL))
            );

            terminal.setNombre_sucursal(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_NOMBRE_SUCURSAL))
            );

            terminal.setDireccion(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_DIRECCION))
            );

            terminal.setCiudad(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_CIUDAD))

            );

            terminal.setNit(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_NIT))
            );

            terminal.setRazon_social(
                    cursor.getString(cursor.getColumnIndex(DatabaseManager.TableTerminal.COL_RAZON_SOCIAL))
            );

        }

        cursor.close();

        return terminal;
    }

    /*#############################################################################################################################################################
        DE AQUI PARA ABAJO NO SE HA REVISADO
    /*#############################################################################################################################################################
    


    /**
     * Metodo para Obtener el id del producto segun su nombre extraido del web service
     *
     * @param nombreProducto String nombre del producto a consultar
     * @return int id del producto consultado por nombre
     */
    private int obtenerProductoIdByNombre(String nombreProducto) {

        int producto_id;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT " + DatabaseManager.TableProducto.COL_ID + " AS ID " +
                        " FROM " + DatabaseManager.TableProducto.TABLE_PRODUCTO +
                        " WHERE " + DatabaseManager.TableProducto.COL_NOMBRE + " = '" + nombreProducto + "'" +
                        " LIMIT 1", null
        );

        cursor.moveToFirst();

        producto_id = cursor.getInt(0);

        cursor.close();

        return producto_id;
    }


    /**
     * Metodo para Obtener valor de la transaccion por el numero de cargo
     *
     * @param numeroCargo
     * @return
     */
    public int obtenerValorTransaccion(String numeroCargo) {

        Log.i("AppDatabase cargo", numeroCargo);

        int valorTransaccion = AnulacionEvent.VALOR_TRANSACCION_NO_VALIDO;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT  " +
                        DatabaseManager.TableTransacciones.COL_VALOR +
                        " FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES +
                        " WHERE " +
                        DatabaseManager.TableTransacciones.COL_NUMERO_CARGO + " = '" + numeroCargo + "' AND " +
                        DatabaseManager.TableTransacciones.COL_TIPO_TRANSACCION + " = " + Transaccion.TIPO_TRANSACCION_CONSUMO
                , null
        );
        if (cursor.moveToFirst()) {

            valorTransaccion = cursor.getInt(0);

        }

        cursor.close();

        Log.i("AppDatabase valor", String.valueOf(valorTransaccion));

        return valorTransaccion;
    }


    /**
     * Metodo para validar si existe registro inicial en la Base de Datos para la configuracion inical de la impresora
     * Usado en modulos:
     * - Configuracion
     *
     * @return int conteo de los registros de la configuracion de la impresora
     */
    public int conteoDataTerminal() {

        int count;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT COUNT(1) FROM " +
                        DatabaseManager.TableTerminal.TABLE_TERMINAL,
                null
        );

        cursor.moveToFirst();

        count = cursor.getInt(0);

        cursor.close();

        return count;

    }


    /**
     * Metodo para insertar registro de la configuracion de la conexion
     *
     * @param conexionEstablecimiento información de la configuración
     * @return Boolean estado de la transaccion
     */
    public boolean insertConfiguracionConexion(ConexionEstablecimiento conexionEstablecimiento) {

        boolean transaction = false;

        long conteo = 0;

        try {
            getWritableDatabase().beginTransaction();

            // Eliminacion de registros anteriores en la base de datos
            getWritableDatabase().delete(
                    DatabaseManager.TableConfiguracion.TABLE_CONFIGURACION,
                    "",
                    null
            );

            // Inicializacion de la variable de contenidos del registro
            ContentValues contentValuesInsert = new ContentValues();

            // Almacena los valores a insertar
            contentValuesInsert.put(DatabaseManager.TableConfiguracion.COL_HOST, conexionEstablecimiento.getIp1());
            contentValuesInsert.put(DatabaseManager.TableConfiguracion.COL_CODIGO, conexionEstablecimiento.getCodigoTerminal());
            contentValuesInsert.put(DatabaseManager.TableConfiguracion.COL_PORT, getDateTime());

            getWritableDatabase().insert(
                    DatabaseManager.TableConfiguracion.TABLE_CONFIGURACION,
                    null,
                    contentValuesInsert
            );

            transaction = true;

            getWritableDatabase().setTransactionSuccessful();

        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            getWritableDatabase().endTransaction();

        }
        return transaction;
    }

    /**
     * Metodo para borrar la configuracion de conexion en cas de error
     */
    public void deleteConfiguracionConexion() {

        // Eliminacion de registros anteriores en la base de datos
        getWritableDatabase().delete(
                DatabaseManager.TableConfiguracion.TABLE_CONFIGURACION,
                "",
                null
        );
    }

    /**
     * Metodo para obtener el registro de la configuracion de la conexion
     *
     * @return String URL de la configuracion de la conexion
     */
    public String obtenerURLConfiguracionConexion() {

        String urlTransacciones = "";

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT " + DatabaseManager.TableConfiguracion.COL_HOST +
                        " FROM " + DatabaseManager.TableConfiguracion.TABLE_CONFIGURACION +
                        " LIMIT 1", null
        );

        if (cursor.moveToFirst()) {
            urlTransacciones = cursor.getString(0);
        }

        cursor.close();

        return urlTransacciones;

    }


    /*
      #############################################################################################
      AREA MODULE REPORTES
      #############################################################################################
     */


    public String obtenerFechaTransaccionNumCargo(String cargo) {
        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT " + DatabaseManager.TableTransacciones.COL_FECHA_SERVER + " , " +
                        DatabaseManager.TableTransacciones.COL_HORA_SERVER +
                        " FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES +
                        " WHERE " + DatabaseManager.TableTransacciones.COL_NUMERO_CARGO + " = '" + cargo + "'" +
                        " AND " + DatabaseManager.TableTransacciones.COL_TIPO_TRANSACCION + " = " + Transaccion.TIPO_TRANSACCION_CONSUMO +
                        " ORDER BY " + DatabaseManager.TableTransacciones.COL_REGISTRO + " ASC " +
                        " LIMIT 1", null
        );

        cursor.moveToFirst();

        return cursor.getString(0) + "  " + cursor.getString(1);
    }


    public Transaccion obtenerUltimaTransaccionAnulada() {
        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * " +
                        " FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES +
                        " WHERE " + DatabaseManager.TableTransacciones.COL_TIPO_TRANSACCION + " = " + Transaccion.TIPO_TRANSACCION_ANULACION +
                        " ORDER BY " + DatabaseManager.TableTransacciones.COL_REGISTRO + " DESC " +
                        " LIMIT 1", null
        );

        cursor.moveToFirst();

        Transaccion modelTransaccionOLD = getTransaccionDeCursor(cursor);

        cursor.close();

        return modelTransaccionOLD;

    }


    /**
     * Metodo para Obtener ultima  transaccion
     *
     * @return Transaccion ultima transaccion realizada
     */
    public ArrayList<Transaccion> obtenerUltimaTransaccion() {

        String numero_cargo = "";

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * " +
                        " FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES +
                        " ORDER BY " + DatabaseManager.TableTransacciones.COL_REGISTRO + " DESC " +
                        " LIMIT 1", null
        );

        if (cursor.moveToFirst()) {
            numero_cargo = cursor.getString(2);
        }

        cursor.close();

        return obtenerTransaccionByNumeroCargo(numero_cargo);
    }

    /**
     * Metodo para Obtener una  transaccion segun el numero de cargo
     *
     * @param numCargo String numero de cargo para la consulta de la transaccion
     * @return Transaccion obtiene la transaccion segun el numero de cargo
     */
    public ArrayList<Transaccion> obtenerTransaccionByNumeroCargo(String numCargo) {

        ArrayList<Transaccion> lista = new ArrayList<>();

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * " +
                        " FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES +
                        " WHERE " + DatabaseManager.TableTransacciones.COL_NUMERO_CARGO + " = '" + numCargo + "'"
                , null
        );

        while (cursor.moveToNext()) {

            lista.add(getTransaccionDeCursor(cursor));

        }

        cursor.close();

        return lista;
    }

    /**
     * Metodo para Obtener una  transaccion segun el numero de cargo
     *
     * @return Arraylist destalles de la transaccion
     */
    public ArrayList<Transaccion> obtenerDetallesTransaccion() {

        ArrayList<Transaccion> lista = new ArrayList<>();

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES +
                        " WHERE " + DatabaseManager.TableTransacciones.COL_FECHA_SERVER + " = '" + getDateTime().split(" ")[0] + "'"
                , null
        );

        while (cursor.moveToNext()) {

            lista.add(getTransaccionDeCursor(cursor));

        }

        cursor.close();

        return lista;
    }

    /**
     * Metodo para Obtener una  transaccion segun el numero de cargo
     *
     * @return Arraylist destalles de la transaccion
     */
    public ArrayList<Transaccion> obtenerTransaccionesCierreLote() {

        ArrayList<Transaccion> lista = new ArrayList<>();

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT " + DatabaseManager.TableTransacciones.COL_NUMERO_CARGO + " , " +
                        DatabaseManager.TableTransacciones.COL_CEDULA_USUARIO + " , " +
                        DatabaseManager.TableTransacciones.COL_VALOR + " , " +
                        DatabaseManager.TableTransacciones.COL_TIPO_TRANSACCION
                        + " FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES

                , null
        );

        while (cursor.moveToNext()) {

            Transaccion modelTransaccionOLD = new Transaccion();

            modelTransaccionOLD.setNumero_transaccion(cursor.getString(0));
            modelTransaccionOLD.setNumero_documento(cursor.getString(1));
            modelTransaccionOLD.setValor(cursor.getInt(2));
            modelTransaccionOLD.setTipo_transaccion(cursor.getInt(3));

            lista.add(modelTransaccionOLD);

        }

        cursor.close();

        return lista;
    }
    //DatabaseManager.TableTransacciones.COL_NUMERO_CARGO + " = " + num

    public void dropTransactions(String num) {
        getWritableDatabase().delete(
                DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES,
                DatabaseManager.TableTransacciones.COL_NUMERO_CARGO + " = '" + num + "'",
                null
        );
    }

    /**
     * Metodo para Obtener una  transaccion segun el numero de cargo
     *
     * @return Arraylist destalles de la transaccion
     */
    public ArrayList<Transaccion> obtenerGeneralTransaccion() {

        ArrayList<Transaccion> lista = new ArrayList<>();

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT * FROM " + DatabaseManager.TableTransacciones.TABLE_TRANSACCIONES
                , null
        );

        while (cursor.moveToNext()) {

            lista.add(getTransaccionDeCursor(cursor));

        }

        cursor.close();

        return lista;
    }


    /**
     * Metodo para Obtener el conteo de los registros de la tabla base_financiera
     *
     * @return conteo de los registros
     */
    public int conteoProducto() {

        int count;

        Cursor cursor;

        cursor = getWritableDatabase().rawQuery(
                "SELECT COUNT(1) FROM " +
                        DatabaseManager.TableProducto.TABLE_PRODUCTO,
                null
        );

        cursor.moveToFirst();

        count = cursor.getInt(0);

        cursor.close();

        return count;
    }

    /*
      #############################################################################################
      Metodos privados auxiliares
      #############################################################################################
     */

    /**
     * Metodo para Obtener el String de fecha y hora
     *
     * @return String fecha
     */
    private String getDateTime() {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());

        Date date = new Date();

        return dateFormat.format(date);
    }

    /**
     * Metodo que obtiene los cambios registrados en la ultima sesion de lla base de datos
     *
     * @return long conteo de filas afectadas en la ultima transaccion
     */
    private long obtenerConteoCambios() {
        SQLiteStatement statement = getWritableDatabase().compileStatement("SELECT changes()");
        return statement.simpleQueryForLong();
    }


    private Transaccion getTransaccionDeCursor(Cursor cursor) {

        Transaccion modelTransaccionOLD = new Transaccion();

        modelTransaccionOLD.setId(cursor.getInt(0));
//        modelTransaccionOLD.setTipo_servicio(cursor.getInt(1));
        modelTransaccionOLD.setNumero_transaccion(cursor.getString(2));
        modelTransaccionOLD.setNumero_tarjeta(cursor.getString(3));
        modelTransaccionOLD.setValor(cursor.getInt(4));
        modelTransaccionOLD.setTipo_transaccion(cursor.getInt(5));
        modelTransaccionOLD.setNumero_documento(cursor.getString(6));
        modelTransaccionOLD.setNombre_usuario(cursor.getString(7));
        modelTransaccionOLD.setFecha_server(cursor.getString(8));
        modelTransaccionOLD.setHora_server(cursor.getString(9));
        modelTransaccionOLD.setRegistro(cursor.getString(10));
        modelTransaccionOLD.setEstado(cursor.getInt(11));

        return modelTransaccionOLD;

    }


    /*#############################################################################################
    AREA CONSULTAS GENERALES
    #############################################################################################*/

    /**
     * @param contentValues
     * @param tableName
     * @return boolean
     */
    private boolean insertData(ContentValues contentValues, String tableName) {

        boolean transaction = false;

        long conteo = 0;

        try {

            getWritableDatabase().insert(
                    tableName,
                    null,
                    contentValues
            );

            conteo = obtenerConteoCambios();

        } catch (SQLException e) {

            e.printStackTrace();

        }

        if (conteo == 1) {

            transaction = true;

        }

        return transaction;

    }

    /**
     * @return
     */
    public boolean cleanDatabase() {

        boolean transaction = true;

        try {

            DatabaseManager.class.getClasses();

            truncateTable(DatabaseManager.TableConfiguracion.class.getName());

            truncateTable(DatabaseManager.TablePrinter.class.getName());

            truncateTable(DatabaseManager.TableProducto.class.getName());

            truncateTable(DatabaseManager.TableTerminal.class.getName());

            truncateTable(DatabaseManager.TableTransacciones.class.getName());

        } catch (Exception e) {

            Log.e("Exception", e.getMessage());

            transaction = false;

        }

        return transaction;

    }

    /**
     * @param nameClass
     */
    private void truncateTable(String nameClass) {

        dropTable(nameClass);

        createTable(nameClass);

    }

    /**
     * @param nameClass
     */
    private synchronized void dropTable(String nameClass) {

        try {

            Class<?> typeClass = Class.forName(nameClass);

            Field fieldDrop = typeClass.getDeclaredField("DROP_TABLE");

            fieldDrop.setAccessible(true);

            Object valueDrop = fieldDrop.get(typeClass);

            getWritableDatabase().execSQL(valueDrop.toString());

        } catch (SQLException | ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {

            e.printStackTrace();

        }

    }

    /**
     * @param nameClass
     */
    private synchronized void createTable(String nameClass) {

        try {

            Class<?> typeClass = Class.forName(nameClass);

            Field fieldCreate = typeClass.getDeclaredField("CREATE_TABLE");

            fieldCreate.setAccessible(true);

            Object valueCreate = fieldCreate.get(typeClass);

            getWritableDatabase().execSQL(valueCreate.toString());

        } catch (SQLException | ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {

            e.printStackTrace();

        }

    }

}
