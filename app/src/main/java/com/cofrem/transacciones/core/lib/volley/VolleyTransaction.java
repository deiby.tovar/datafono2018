package com.cofrem.transacciones.core.lib.volley;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class VolleyTransaction {

    private static final String TAG_ERROR_JSON = "Error Json: ";

    private static final String TAG_ERROR_VOLLEY = "Error Volley: ";

    /**
     * Metodo que hace la TransaccionWS de datos por el Metodo POST
     *
     * @param context
     * @param moduleTransaccion
     * @param parameters
     * @param callback
     */
    public void getData(Context context,
                        HashMap<String, String> parameters,
                        String moduleTransaccion,
                        final VolleyCallback callback) {

        // Añadir petición GSON a la cola
        VolleySingleton.getInstance(context).addToRequestQueue(

                //Llamado al constructor de la clase GsonRequest
                new GsonRequest<>(

                        //@param int method
                        Request.Method.POST,

                        //@param PARAM_URL
                        moduleTransaccion,

                        //@param Class<T> clazz Clase o modelo en el que se formatean los datos
                        JsonObject.class,

                        //@param headers
                        null,

                        //@param parameters
                        parameters,

                        //@param Listener
                        new Response.Listener<JsonObject>() {

                            @Override
                            public void onResponse(JsonObject response) {

                                try {

                                    callback.onSuccess(response);

                                } catch (JsonParseException e) {

                                    Log.e(TAG_ERROR_JSON, e.getLocalizedMessage());

                                    callback.onErrorConnection();

                                }
                            }
                        },
                        //@param errorListener
                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error.networkResponse != null) {

                                    try {

                                        JSONObject jsonObj = new JSONObject(new String(error.networkResponse.data, "UTF-8"));

                                        callback.onError(jsonObj);

                                    } catch (UnsupportedEncodingException | JSONException e) {

                                        e.printStackTrace();

                                        Log.e(TAG_ERROR_VOLLEY, e.toString());

                                        callback.onErrorConnection();

                                    }

                                } else {

                                    callback.onErrorConnection();

                                }

                            }

                        }

                )

        );

    }

    /**
     * Metodo que hace la TransaccionWS de datos por el Metodo GET
     *
     * @param context
     * @param moduleTransaccion
     * @param callback
     */
    public void getData(Context context,
                        String moduleTransaccion,
                        final VolleyCallback callback) {

        // Añadir petición GSON a la cola
        VolleySingleton.getInstance(context).addToRequestQueue(

                //Llamado al constructor de la clase GsonRequest
                new GsonRequest<>(

                        //@param int method
                        Request.Method.GET,

                        //@param PARAM_URL
                        moduleTransaccion,

                        //@param Class<T> clazz Clase o modelo en el que se formatean los datos
                        JsonObject.class,

                        //@param parameters
                        null,

                        null,

                        //@param Listener
                        new Response.Listener<JsonObject>() {

                            @Override
                            public void onResponse(JsonObject response) {

                                try {

                                    callback.onSuccess(response);


                                } catch (JsonParseException e) {

                                    Log.e(TAG_ERROR_JSON, e.getLocalizedMessage());

                                    callback.onErrorConnection();

                                }

                            }

                        },
                        //@param errorListener
                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error.networkResponse != null) {

                                    try {

                                        JSONObject jsonObj = new JSONObject(new String(error.networkResponse.data, "UTF-8"));

                                        callback.onError(jsonObj);

                                    } catch (UnsupportedEncodingException | JSONException e) {

                                        e.printStackTrace();

                                        Log.e(TAG_ERROR_VOLLEY, e.toString());

                                        callback.onErrorConnection();

                                    }

                                } else {

                                    callback.onErrorConnection();

                                }

                            }

                        }

                )

        );

    }

    /**
     * Interface de Callback de ValuesTransaction Volley
     */
    public interface VolleyCallback {
        /**
         * Retorno en peticion correcta
         *
         * @param data
         */
        void onSuccess(JsonObject data);

        /**
         * Retorno en error de peticion
         *
         * @param errorMessage
         */
        void onError(JSONObject errorMessage);

        /**
         * Retorno en error de conexion
         */
        void onErrorConnection();
    }

}
