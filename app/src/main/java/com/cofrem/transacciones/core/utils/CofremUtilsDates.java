package com.cofrem.transacciones.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CofremUtilsDates {

    /**
     * @return
     */
    public static String todayNow() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());

        Date date = new Date();

        return dateFormat.format(date);

    }

    /**
     * @return
     */
    public static String todayDate() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        Date date = new Date();

        return dateFormat.format(date);

    }

    /**
     * @return
     */
    public static String nowTime() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

        Date date = new Date();

        return dateFormat.format(date);

    }


}
