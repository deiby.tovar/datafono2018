package com.cofrem.transacciones.core.lib.crypt;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by luisp on 11/10/2017.
 */

public class AESCrypt {

    private static final String CHARSET_UTF = "UTF-8";

    public static final String SEED_16_CHARACTER = "$CREDICOFREM@2018";
    // Definición del tipo de algoritmo a utilizar (AES, DES, RSA)
    private static final String alg = "AES";
    // Definición del modo de cifrado a utilizar
    private static final String cI = "AES/GCM/NoPadding";
    private Cipher cipher1;
    private SecretKeySpec key1;
    private AlgorithmParameterSpec spec;

    public AESCrypt() {
    }

    public AESCrypt(String x) throws Exception {
        // hash password with SHA-256 and crop the output to 128-bit for key
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(SEED_16_CHARACTER.getBytes(CHARSET_UTF));
        byte[] keyBytes = new byte[32];
        System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);

        cipher1 = Cipher.getInstance("AES/CBC/PKCS7Padding");
        key1 = new SecretKeySpec(keyBytes, "AES");
        spec = getIV();
    }

    public static String encrypt(String cleartext) throws Exception {
        String key = "92AE31A79FEEB2A3";
        String iv = "0123456789ABCDEF";
        Cipher cipher = Cipher.getInstance(cI);
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(cleartext.getBytes());
        return new String(Base64.encode(encrypted, Base64.DEFAULT), CHARSET_UTF);
    }


    public AlgorithmParameterSpec getIV() {
        byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,};
//        byte[] iv = { 1,4,2,5,6,3};
        IvParameterSpec ivParameterSpec;
        ivParameterSpec = new IvParameterSpec(iv);

        return ivParameterSpec;
    }

    public String encrypt2(String plainText) throws Exception {
        cipher1.init(Cipher.ENCRYPT_MODE, key1, spec);
        byte[] encrypted = cipher1.doFinal(plainText.getBytes(CHARSET_UTF));
        String encryptedText = new String(Base64.encode(encrypted, Base64.DEFAULT), CHARSET_UTF);

        return encryptedText;
    }

    public String decrypt(String cryptedText) throws Exception {
        cipher1.init(Cipher.DECRYPT_MODE, key1, spec);
        byte[] bytes = Base64.decode(cryptedText, Base64.DEFAULT);
        byte[] decrypted = cipher1.doFinal(bytes);

        return new String(decrypted, CHARSET_UTF);
    }


}
