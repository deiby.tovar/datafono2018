package com.cofrem.transacciones.core.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Locale;

public class AutoMapper {

    /**
     * Constantes
     */
    //Tag para muestreo de errores
    private static final String TAG = "Automapper";

    //Address models
    private final String ADDR_BASE = "models.pojo";

    //Namespace models
    private String PACKAGE_MODELS;

    //Nombre del paquete
    private String PACKAGE;

    /**
     * Propiedades
     */

    //Aliases
    private HashMap<String, String> alias = new HashMap<>();

    //Nombre de la clase
    private String className;

    //Ubicacion de los modelos alterna
    private String addrModels;

    /**
     * Constructor de la clase
     */
    public AutoMapper(Context context) {

        this.PACKAGE = context.getPackageName();

        this.PACKAGE_MODELS = this.PACKAGE + "." + this.ADDR_BASE + ".";

    }

    /**
     * Metodo que registra un atributo en una instancia de una clase
     *
     * @param object     Objeto instancia de la clase
     * @param fieldName  Nombre del campo
     * @param fieldValue Valor a registrar
     */
    private static void setValue(Object object, String fieldName, Object fieldValue) {

        final String SECTION = "SetValue";

        Class<?> clazz = object.getClass();

        if (clazz != null) {

            try {

                Field field = clazz.getDeclaredField(fieldName);

                field.setAccessible(true);

                field.set(object, fieldValue);

            } catch (NoSuchFieldException e) {

                Log.e(TAG + " " + SECTION, fieldName + " / NoSuchFieldException: " + e);

            } catch (IllegalArgumentException e) {

                Log.e(TAG + " " + SECTION, fieldName + " / IllegalArgumentException: " + e);

            } catch (IllegalStateException e) {

                Log.e(TAG + " " + SECTION, fieldName + " / IllegalStateException: " + e);

            } catch (IllegalAccessException e) {

                Log.e(TAG + " " + SECTION, fieldName + " / IllegalAccessException: " + e);

            }

        } else {

            Log.e(TAG + " " + SECTION, object.getClass().getName() + " / Clase no existe ");

        }

    }

    /**
     * Getters & Setters
     */
    public HashMap<String, String> getAlias() {
        return alias;
    }

    public void setAlias(HashMap<String, String> alias) {
        this.alias = alias;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAddrModels() {

        return this.addrModels == null ? ADDR_BASE : addrModels;

    }

    public void setAddrModels(String addrModels) {

        this.addrModels = addrModels;

        this.PACKAGE_MODELS = this.PACKAGE + "." + this.addrModels + ".";

    }

    /**
     * Metodo sobrecargardo del mapping que recibe un unico objeto
     *
     * @param dataTransaction Data de la transaccion
     * @return Regresa el objeto mapeado
     */
    public Object mapping(Object dataTransaction) {

        if (this.className != null)

            return mapping(this.className, dataTransaction);

        else {

            Log.e(TAG, "Debes registrar una clase para hacer el mapping");

            return null;
        }
    }

    /**
     * Metodo que mapea los atributos de la clase con los parametros de la transacciÃ³n
     *
     * @param dataTransaction Data de la transaccion
     * @return Regresa el objeto mapeado
     */
    private Object mapping(String className, Object dataTransaction) {

        String logTag = TAG + " mapping";

        Class<?> clazz;

        Object object = null;

        try {

            //Se crea la clase dinamica desde el nombre enviado
            clazz = Class.forName(formatClassName(className));

            if (dataTransaction instanceof JsonArray) { //Si el objeto recibido es un JsonArray

                JsonArray jsonArray = ((JsonArray) dataTransaction).getAsJsonArray();

                int size = jsonArray.size();

                Object objectArray = Array.newInstance(clazz, size);

                for (int position = 0; position < size; position++)

                    Array.set(
                            objectArray,
                            position,
                            mapping(
                                    className,
                                    jsonArray.get(position).getAsJsonObject()
                            )
                    );

                return objectArray;

            } else if (dataTransaction instanceof JsonObject) { //Si el objeto recibido es un JsonObject

                Field[] fields = getFieldsClass(className);

                if (fields != null && fields.length > 0) { //Evalua si la clase tiene campos

                    try {

                        object = clazz.newInstance(); // crea la instancia de la clase

                        String fieldNameDataTransaction;

                        String fieldNameObject;

                        for (Field field : fields) { //Recorre el arreglo de campos

                            if (//Valida la existencia del campo en la transaccion y en el objeto
                                    ((JsonObject) dataTransaction).has(field.getName()) || //El nombre del campo esta contenido en el JsonObject
                                            alias.containsKey(capitalCase(field.getName())) //El nombre del campo esta contenido en el map de alias
                            ) {

                                fieldNameDataTransaction = alias.containsKey(capitalCase(field.getName())) ? alias.get(capitalCase(field.getName())) : field.getName();

                                fieldNameObject = field.getName();

                                if (((JsonObject) dataTransaction).get(fieldNameDataTransaction).isJsonObject()) {

                                    setValue(
                                            object,
                                            fieldNameObject,
                                            mapping(
                                                    fieldNameObject,
                                                    ((JsonObject) dataTransaction).get(fieldNameDataTransaction).getAsJsonObject()
                                            )
                                    );

                                } else if (((JsonObject) dataTransaction).get(fieldNameDataTransaction).isJsonArray()) {

                                    JsonArray jsonArray = ((JsonObject) dataTransaction).get(fieldNameDataTransaction).getAsJsonArray();

                                    int size = jsonArray.size();

                                    Class clazzArray = Class.forName(formatClassName(fieldNameObject));

                                    Object instanceArray = Array.newInstance(clazzArray, size);

                                    for (int position = 0; position < size; position++)
                                        Array.set(
                                                instanceArray,
                                                position,
                                                mapping(
                                                        fieldNameObject,
                                                        jsonArray.get(position).getAsJsonObject()
                                                )
                                        );

                                    setValue(
                                            object,
                                            fieldNameObject,
                                            instanceArray
                                    );

                                } else {

                                    setValue(object,
                                            fieldNameObject,
                                            validateDataContent(((JsonObject) dataTransaction).getAsJsonObject().get(fieldNameDataTransaction)));

                                }

                            }

                        }

                        return object;

                    } catch (IllegalAccessException e) {

                        Log.e(logTag, "IllegalAccessException: " + e);

                        return null;

                    } catch (InstantiationException e) {

                        Log.e(logTag, "InstantiationException: " + e);

                        return null;

                    }

                } else {

                    Log.e(logTag, className + ": NO contiene campos");

                    return null;
                }
            } else {

                return validateDataContent((JsonElement) dataTransaction);

            }

        } catch (ClassNotFoundException e) {

            Log.e(logTag, "ClassNotFoundException: " + e);

            return null;

        }

    }

    /**
     * Metodo que obtiene los campos de la clase
     *
     * @param className Nombre de la clase
     * @return Retorna un arreglo de campos
     */
    private Field[] getFieldsClass(String className) {

        Field[] fieldDeclaredArrayClass = null;

        try {

            Class<?> clazz = Class.forName(formatClassName(className));

            fieldDeclaredArrayClass = clazz.getDeclaredFields();

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

            Log.e(TAG + " fields", className + " / ClassNotFoundException: " + e);

        }

        return fieldDeclaredArrayClass;
    }

    /**
     * Metodo que formatea el nombre de la clase con el nombre del paquete
     *
     * @param className Nombre de la clase
     * @return Retorna un nombre de paquete
     */
    private String formatClassName(String className) {

        return PACKAGE_MODELS + capitalCase(className);

    }

    /**
     * Metodo que formatea el nombre de la clase en CapitalCase
     *
     * @param string Nombre de la clase
     * @return Retorna un nombre de clase
     */
    private String capitalCase(String string) {

        return string.substring(0, 1).toUpperCase(Locale.getDefault()) + string.substring(1);

    }

    /**
     * Metodo que valida un String de un JsonElement
     *
     * @param jsonElement Dato a validar
     * @return String segun el dato
     */
    private String validateDataContent(JsonElement jsonElement) {

        return jsonElement.isJsonNull() ? "" : jsonElement.getAsString();

    }

}