package com.cofrem.transacciones.core.global;

public class InfoGlobalSettings {

    //Dispositivos compatibles
    public static final String[] supportedDevices = {"TPS390"};

    public static final String INITIAL_PASS = "8717";

    public static final String COLOR_EDITEXT_FOCUS = "#464647";

    public static final String COLOR_EDITEXT = "#000000";

}
