package com.cofrem.transacciones.core.utils;

import com.cofrem.transacciones.core.global.KeyResponse;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.pojo.Saldo;

import org.json.JSONException;
import org.json.JSONObject;

public class CofremUtils {

    private static final String TAG_KEYBOARD_CLASS = "Exception Keyboard";

    public static String errorMessage(JSONObject errorMessage) {

        String message;

        try {

            message = errorMessage.getString(KeyResponse.KEY_STATUS) + ": " + errorMessage.getString(KeyResponse.KEY_MESSAGE);

        } catch (JSONException e) {

            message = e.getMessage();

        }

        return message;

    }

    /**
     * Entrega el numero de la tarjeta sin caracteres especiales
     *
     * @param readMagnetic
     * @return
     */
    public static String cleanNumberCard(String[] readMagnetic) {

        //Limpia el formato de la lectura
        return readMagnetic[1]
                .replace(";", "")
                .replace("!", "")
                .replace("#", "")
                .replace("$", "")
                .replace("&", "")
                .replace("/", "")
                .replace("|", "")
                .replace("(", "")
                .replace(")", "")
                .replace("=", "")
                .replace("?", "")
                .replace("¿", "")
                .replace("¿", "")
                .replace("¡", "")
                .replace("*", "")
                .replace("{", "")
                .replace("}", "")
                .replace("[", "")
                .replace("]", "")
                .replace(",", "")
                .replace(".", "")
                .replace("-", "")
                .replace("_", "")
                .replace("%", "");
    }

    /**
     * Entrega el numero de la tarjeta sin caracteres especiales
     *
     * @param valor
     * @return
     */
    public static int cleanValor(String valor) {

        valor = valor
                .trim()
                .replace(".", "")
                .replace(",", "")
                .replace("$", "");

        return valor.length() > 0 ? Integer.parseInt(valor) : 0;

    }

    public static String getTotalSaldo(Saldo[] saldos) {

        int totalSaldo = 0;

        for (Saldo saldo : saldos) {
            totalSaldo = totalSaldo + Integer.parseInt(saldo.getSaldo());
        }

        return PrintRow.numberFormat(totalSaldo);

    }
}