package com.cofrem.transacciones.core.global;

public class InfoGlobalValues {

    /***********************************************************************************************
     * LLAVES GENERALES ****************************************************************************
     **********************************************************************************************/
    public static final String KEY_CONFIGURATION = "KEY_CONFIGURATION";

    public static final String KEY_PASO_PASS = "KEY_PASO_PASS";

    /***********************************************************************************************
     * VALORES GENERALES **************************************************************************
     **********************************************************************************************/




    /***********************************************************************************************
     * VALORES GENERALES **************************************************************************
     **********************************************************************************************/
    public static final int configuracionRegistrarConfigInicial = 0;

    public static final int configuracionPassLocal = 1;

    public static final int configuracionPassServer = 2;

    public static final int configuracionRegistrar = 3;

    public static final int configuracionProbar = 4;
}
