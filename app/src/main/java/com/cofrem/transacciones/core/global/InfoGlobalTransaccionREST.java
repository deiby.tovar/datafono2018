package com.cofrem.transacciones.core.global;

public class InfoGlobalTransaccionREST {

    InfoGlobalTransaccionREST() { /*...*/}

    public static final String HTTP = "http://Redcofrem.ceindetec.org.co";
    public static final String WEB_SERVICE_URI = "/api/";
    public static final String FEED_KEY = "statusTransaction";

    // PARAM_URL de InfoGlobalTransaccionREST
    public static final String METHOD_COMUNICACION = "comunicacion";
    public static final String METHOD_TERMINAL = "terminal";
    public static final String METHOD_CONSULTAR_CLAVE_TARJETA = "consultarclavetarjeta";
    public static final String METHOD_ACTUALIZAR_CLAVE_TARJETA = "actulizarclavetarjeta";
    public static final String METHOD_CLAVE_SUCURSAL = "clavesucursal";
    public static final String METHOD_CLAVE_TERMINAL = "claveterminal";
    public static final String METHOD_ASIGNA_ID = "asignaID";
    public static final String METHOD_GET_SERVICIOS = "getservicios";
    public static final String METHOD_CONSUMO = "consumo";
    public static final String METHOD_ANULACION = "anulacion";
    public static final String METHOD_SALDO = "saldotarjeta";
    public static final String METHOD_INFO_TRANSACCION = "infotransaccion";

}
