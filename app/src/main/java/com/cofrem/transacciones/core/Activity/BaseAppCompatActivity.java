package com.cofrem.transacciones.core.Activity;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.global.InfoGlobalSettings;
import com.cofrem.transacciones.core.global.InfoGlobalSettingsBlockButtons;
import com.cofrem.transacciones.core.utils.KeyBoard;
import com.cofrem.transacciones.models.pojo.InfoHeaderApp;
import com.cofrem.transacciones.modules.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.KeyEvent.KEYCODE_BACK;
import static android.view.KeyEvent.KEYCODE_ENTER;

public abstract class BaseAppCompatActivity
        extends AppCompatActivity {

    protected enum TIPO_ACT {
        PRINCIPAL,
        MODULO,
    }

    private static final String TAG = BaseAppCompatActivity.class.getName();

    //Elementos del header
    @Nullable
    @BindView(R.id.txvHeaderCodigoDispositivo)
    TextView txvHeaderCodigoDispositivo;

    @Nullable
    @BindView(R.id.txvHeaderIdSucursal)
    TextView txvHeaderIdSucursal;

    @Nullable
    @BindView(R.id.txvHeaderRazonSocial)
    TextView txvHeaderRazonSocial;

    @Nullable
    @BindView(R.id.txvHeaderNombreSucursal)
    TextView txvHeaderNombreSucursal;

    //Elementos del header
    @Nullable
    @BindView(R.id.txvExitoTitle)
    TextView txvExitoTitle;

    @Nullable
    @BindView(R.id.txvErrorTitle)
    TextView txvErrorTitle;

    @Nullable
    @BindView(R.id.txvFailCardTitle)
    TextView txvFailCardTitle;

    @Nullable
    @BindView(R.id.txvStripeTitle)
    TextView txvStripeTitle;

    @Nullable
    @BindView(R.id.txvPassTitle)
    TextView txvPassTitle;

    @Nullable
    @BindView(R.id.txvPassAdminTitle)
    TextView txvPassAdminTitle;

    @Nullable
    @BindView(R.id.txvNumCargoTitle)
    TextView txvNumCargoTitle;

    @Nullable
    @BindView(R.id.txvNumDocTitle)
    TextView txvNumDocTitle;

    @Nullable
    @BindView(R.id.txvServicioTitle)
    TextView txvServicioTitle;

    @Nullable
    @BindView(R.id.txvVerificacionTitle)
    TextView txvVerificacionTitle;

    //Variables
    protected boolean setupInfoHeader = true;

    protected View[] bodyProceso;

    protected EditText[] edtProceso;

    private ProgressDialog progressDialog;

    protected CoordinatorLayout coordinatorBase;

    protected TIPO_ACT typeActivity = TIPO_ACT.PRINCIPAL;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Compatibilidad de vectores en versiones antiguas de Android
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //Colocacion de la orientacion de la app
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Previene la apertura del Status Bar
        //getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);

        //Inicializa la barra de progreso
        setupProgressBar();

    }

    /**
     * @param layoutResID
     */
    @Override
    public void setContentView(int layoutResID) {

        super.setContentView(layoutResID);

        ButterKnife.bind(this);

        if (setupInfoHeader) {
            setupHeader();
        }

    }

    /**
     *
     */
    @Override
    protected void onPause() {

        super.onPause();

        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);

        assert activityManager != null;

        activityManager.moveTaskToFront(getTaskId(), 0);

    }

    /**
     * Metodo sobrecargado de la vista cuando pierde el foco
     *
     * @param hasFocus regresa el parametro del foco del sistema
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus) {
            //Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);

            sendBroadcast(closeDialog);
        }
    }

    /**
     * Metodo que intercepta las pulsaciones de las teclas del teclado fisico
     *
     * @param keyCode Tecla del evento
     * @param event   Evento
     * @return retorno de Boolean
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        /**
         * Keycodes disponibles
         * 4: Back
         * 66: Enter
         * 67: Delete
         */
        switch (keyCode) {
            case KEYCODE_ENTER:
                enterPressed();
                break;
            case KEYCODE_BACK:
                onBackPressed();
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    /**
     * Metodo que interfiere la presion del boton "Enter"
     */
    public void enterPressed() {
        //No Action
    }

    /**
     * Metodo que interfiere la presion del boton "Back"
     */
    @Override
    public void onBackPressed() {
        switch (typeActivity) {
            case PRINCIPAL:
                String mensajeRegresar = getString(R.string.general_message_press_back) + " " + getString(R.string.general_text_button_regresar);
                showMessage(mensajeRegresar);
                break;
            case MODULO:
                cleanEditText();
                break;
            default:
                break;
        }
    }

    /**
     * Metodo sobrecargado de la vista para la presion de las teclas de volumen
     *
     * @param event evento de la presion de una tecla
     * @return regresa el rechazo de la presion
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return InfoGlobalSettingsBlockButtons.blockedKeys.contains(event.getKeyCode()) || super.dispatchKeyEvent(event);
    }

    /**
     * #############################################################################################
     * Metodos propios de la clase
     * #############################################################################################
     */

    /**
     *
     */
    private void setupProgressBar() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Procesando");
        progressDialog.setMessage("Procesando su petición...");
    }

    /**
     *
     */
    protected void showProgress() {
        progressDialog.show();
    }

    /**
     *
     */
    protected void hideProgress() {
        progressDialog.dismiss();
    }

    /**
     * @param errorMessage
     */
    protected void showMessageError(String errorMessage) {
        showMessage("Ocurrio un error: " + errorMessage);
    }

    /**
     * @param message
     */
    protected void showMessage(String message) {

        if (coordinatorBase != null) {
            Snackbar.make(coordinatorBase, message, Snackbar.LENGTH_SHORT).show();
        }

        Log.e(TAG, "Instancia el CoordinatorLayout desde la actividad hija");
    }

    /**
     * Coloca el encabezado
     */
    private void setupHeader() {
        if (txvHeaderCodigoDispositivo != null) {
            txvHeaderCodigoDispositivo.setText(
                    String.format(
                            getString(R.string.header_text_codigo_dispositivo)
                            , InfoHeaderApp.getInstance().getCodigoDispositivo()
                    ));
        }
        if (txvHeaderIdSucursal != null) {
            txvHeaderIdSucursal.setText(
                    String.format(
                            getString(R.string.header_text_id_sucursal)
                            , InfoHeaderApp.getInstance().getIdSucursal()
                    ));
        }
        if (txvHeaderRazonSocial != null) {
            txvHeaderRazonSocial.setText(
                    String.format(
                            getString(R.string.header_text_nombre_razon_social)
                            , InfoHeaderApp.getInstance().getRazonSocial()
                    ));
        }
        if (txvHeaderNombreSucursal != null) {
            txvHeaderNombreSucursal.setText(
                    String.format(
                            getString(R.string.header_text_nombre_sucursal)
                            , InfoHeaderApp.getInstance().getNombreSucursal()
                    ));
        }
    }

    /**
     * Coloca los titulos en vistas genericas que no poseen titulo propio
     *
     * @param titleView
     */
    public void setupTitles(String titleView) {

        TextView[] txvProceso = {
                txvExitoTitle,
                txvErrorTitle,
                txvFailCardTitle,
                txvStripeTitle,
                txvStripeTitle,
                txvStripeTitle,
                txvPassTitle,
                txvPassAdminTitle,
                txvNumCargoTitle,
                txvNumDocTitle,
                txvServicioTitle,
                txvVerificacionTitle,
        };

        for (TextView txv : txvProceso) {
            if (txv != null) {
                txv.setText(titleView);
            }
        }
    }

    /**
     * Navegador a ventana principal
     *
     * @param context
     */
    public void navigateToMain(Context context) {
        navigateTo(context, MainActivity.class);
    }

    /**
     * Manejador de navegacion de vistas
     *
     * @param context
     * @param clazz
     */
    public void navigateTo(Context context, Class clazz) {
        navigateTo(context, clazz, null);
    }

    /**
     * Manejador de navegacion de vistas
     *
     * @param context
     * @param clazz
     */
    public void navigateTo(Context context, Class clazz, Bundle bundle) {

        Intent intent = new Intent(context, clazz);

        //Agregadas banderas para no retorno
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        startActivity(intent);
    }

    /**
     * #############################################################################################
     * Metodo de manejos de vistas
     * #############################################################################################
     */

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    public void hideKeyBoard(View v) {
        //Oculta el teclado
        KeyBoard.hide(this);
        initColorEditText();
        putFocusEditText(v);
    }

    /**
     * Oculta todas las vistas del proceso
     */
    public void hideBody() {
        for (View v : bodyProceso) {
            v.setVisibility(View.GONE);
        }
    }

    /**
     * Inicializa el color de los edittext del proceso
     */
    private void initColorEditText() {
        for (EditText edt : edtProceso) {
            edt.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT));
        }
    }

    /**
     * Pone el foco en un edittext especifico del proceso
     *
     * @param v
     */
    private void putFocusEditText(View v) {
        for (EditText edt : edtProceso) {
            if (edt.getId() == v.getId()) {
                edt.requestFocus();
                edt.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT_FOCUS));
            }
        }
    }

    /**
     * Vacia el contenido de los edittext del proceso
     */
    public void cleanEditText() {
        for (EditText edt : edtProceso) {
            edt.setText("");
        }
    }
}
