package com.cofrem.transacciones.core.global;

public class KeyResponse {

    /***********************************************************************************************
     * Llaves de transacciones Web Services ********************************************************
     **********************************************************************************************/
    public static final String KEY_STATUS = "status";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_DATA = "data";

    /***********************************************************************************************
     * Llaves de results Web Services **************************************************************
     **********************************************************************************************/
    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_ERROR = "error";

}
