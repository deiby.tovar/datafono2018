package com.cofrem.transacciones.core.global;

public class InfoGlobalSettingsPrint {

    //estados de la impresora

    public static final int PRINTER_DISCONNECT = -1004;
    public static final int PRINTER_OUT_OF_PAPER = -1001;
    public static final int PRINTER_OVER_FLOW = -1003;
    public static final int PRINTER_OVER_HEAT = -1002;
    public static final int PRINTER_ERROR = -9999;
    public static final int PRINTER_OK = 0;

    //0 - 255
    public static final int LEFT_DISTANCE = 1;

    //0 - 255
    public static final int LINE_DISTANCE = 1;

    //1 - 4
    public static final int FONT_SIZE = 2;
    public static final int FONT_SIZE_1 = 1;
    public static final int FONT_SIZE_2 = 2;
    public static final int FONT_SIZE_3 = 3;
    public static final int FONT_SIZE_4 = 4;


    //0 - 12
    public static final int GRAY_LEVEL = 12;

    /**
     * Alguna especie de codigos???
     */

    public static final int CODE_PRINTIT = 1;
    public static final int CODE_ENABLE_BUTTON = 2;
    public static final int CODE_NOPAPER = 3;
    public static final int CODE_LOWBATTERY = 4;
    public static final int CODE_PRINTVERSION = 5;
    public static final int CODE_PRINTBARCODE = 6;
    public static final int CODE_PRINTQRCODE = 7;
    public static final int CODE_PRINTPAPERWALK = 8;
    public static final int CODE_PRINTCONTENT = 9;
    public static final int CODE_CANCELPROMPT = 10;
    public static final int CODE_PRINTERR = 11;
    public static final int CODE_OVERHEAT = 12;
    public static final int CODE_MAKER = 13;
    public static final int CODE_PRINTPICTURE = 14;
    public static final int CODE_EXECUTECOMMAND = 15;
    public static final int CODE_PRINTCERIBO = 16;

    public static final int ALGIN_LEFT = 0;
    public static final int ALGIN_MIDDLE = 1;
    public static final int ALGIN_RIGHT = 2;


}
