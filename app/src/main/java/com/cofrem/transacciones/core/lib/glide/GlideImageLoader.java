package com.cofrem.transacciones.core.lib.glide;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

/**
 * Clase que hace uso de la libreria GlideImage
 */
public class GlideImageLoader implements ImageLoader {

    Context context;
    private RequestManager requestManager;
    private RequestOptions requestOptions;

    public GlideImageLoader(Context context) {

        this.context = context;

        this.requestManager = Glide.with(this.context);

        requestOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .optionalCenterCrop()
                .override(600, 400);

    }

    /**
     * @param imageView
     * @param url
     */
    @Override
    public void loadImage(ImageView imageView, String url) {

        requestManager
                .load(url)
                .apply(requestOptions)
                .into(imageView);

    }

    /**
     * @param imageView
     * @param drawable
     */
    @Override
    public void loadImage(ImageView imageView, int drawable) {

        requestManager
                .load(this.context.getResources().getDrawable(drawable))
                .apply(requestOptions)
                .into(imageView);
    }

}
