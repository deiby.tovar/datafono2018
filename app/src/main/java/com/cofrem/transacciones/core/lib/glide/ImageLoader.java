package com.cofrem.transacciones.core.lib.glide;

import android.widget.ImageView;

public interface ImageLoader {

    /**
     * @param imageView
     * @param url
     */
    void loadImage(ImageView imageView, String url);

    /**
     * @param imageView
     * @param drawable
     */
    void loadImage(ImageView imageView, int drawable);

}
