package com.cofrem.transacciones.modules.configuration.comm.event;

public class CommEvent {

    public enum Type {
        onVerifySuccess,
        onCommTestSuccess,
        onCommTestError,
        onTransaccionWSConexionError,
    }

    private Type type;

    private String errorMessage;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
