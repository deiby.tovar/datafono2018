package com.cofrem.transacciones.modules.transaction.saldo.event;

import com.cofrem.transacciones.models.modelsWS.modelTransaccion.InformacionSaldo;
import com.cofrem.transacciones.models.pojo.Saldo;

public class SaldoEvent {

    public enum Type {
        onConsultarSaldoSuccess,
        onConsultarSaldoError,
        onErrorConexion,
        onImprecionReciboSuccess,
        onImprecionReciboError,
    }

    private Type type;

    private String errorMessage;

    private InformacionSaldo informacionSaldo;

    private Saldo[] saldos;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public InformacionSaldo getInformacionSaldo() {
        return informacionSaldo;
    }

    public void setInformacionSaldo(InformacionSaldo informacionSaldo) {
        this.informacionSaldo = informacionSaldo;
    }

    public Saldo[] getSaldos() {
        return saldos;
    }

    public void setSaldos(Saldo[] saldos) {
        this.saldos = saldos;
    }
}
