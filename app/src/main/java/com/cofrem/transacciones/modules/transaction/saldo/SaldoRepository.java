package com.cofrem.transacciones.modules.transaction.saldo;

import android.content.Context;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.global.InfoGlobalSettingsPrint;
import com.cofrem.transacciones.core.global.InfoGlobalTransaccionREST;
import com.cofrem.transacciones.core.global.KeyResponse;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.printer.PrinterHandler;
import com.cofrem.transacciones.core.lib.printer.StyleConfig;
import com.cofrem.transacciones.core.lib.volley.VolleyTransaction;
import com.cofrem.transacciones.core.utils.AutoMapper;
import com.cofrem.transacciones.core.utils.CofremUtils;
import com.cofrem.transacciones.database.AppDatabase;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.modelsWS.modelTransaccion.InformacionSaldo;
import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;
import com.cofrem.transacciones.models.pojo.Saldo;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.models.transactions.TransaccionWS;
import com.cofrem.transacciones.modules.transaction.saldo.event.SaldoEvent;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SaldoRepository implements SaldoContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public SaldoRepository(Context context) {
        this.context = context;
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */

    public static String toTextCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    @Override
    public void consultarSaldo(Transaccion transaccion) {

        final HashMap<String, String> parameters = new HashMap<>();

        final String codigo = AppDatabase.getInstance(context).getDataDispositivo().getCodigo();

        parameters.put("codigo", codigo);

        parameters.put("numero_tarjeta", transaccion.getNumero_tarjeta());

        parameters.put("password", String.valueOf(transaccion.getClave()));

        VolleyTransaction volleyTransaction = new VolleyTransaction();

        volleyTransaction.getData(
                context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_SALDO,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        TransaccionWS transaccionWS = new TransaccionWS(data);
                        AutoMapper autoMapper = new AutoMapper(context);
                        autoMapper.setClassName(Saldo.class.getSimpleName());
                        Saldo[] saldos = (Saldo[]) autoMapper.mapping(transaccionWS.getDataTransaction().getAsJsonArray("Saldos"));
                        if (saldos.length > 0)
                            postEvent(SaldoEvent.Type.onConsultarSaldoSuccess, saldos);
                        else
                            postEvent(SaldoEvent.Type.onConsultarSaldoError, "No se encontraron servicios asociados");
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        try {
                            postEvent(SaldoEvent.Type.onErrorConexion, errorMessage.getString(KeyResponse.KEY_STATUS) + ": " + errorMessage.getString(KeyResponse.KEY_MESSAGE));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(SaldoEvent.Type.onErrorConexion);
                    }
                });
    }

    @Override
    public void imprimirRecibo(Transaccion transaccion, Saldo[] saldos) {

        ConfigurationPrinter configurationPrinter = AppDatabase.getInstance(context).getConfigurationPrinter();

        int gray = configurationPrinter.getGray_level();

        // creamos el ArrayList se que encarga de almacenar los rows del recibo
        ArrayList<PrintRow> printRows = new ArrayList<PrintRow>();

        //Se agrega el logo al primer renglon del recibo y se coloca en el centro

        printRows.add(PrintRow.printLogo(context, gray));

        PrintRow.printCofrem(context, printRows, gray, 10);

        printRows.add(
                new PrintRow(
                        context.getResources().getString(R.string.recibo_title_consulta_saldo),
                        new StyleConfig(StyleConfig.Align.CENTER, gray, 20)
                )
        );

        printRows.add(
                new PrintRow(context.getResources().getString(
                        R.string.recibo_separador_operador),
                        new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1)
                )
        );

        PrintRow.printOperador(context, printRows, gray, 10);

        printRows.add(
                new PrintRow(
                        context.getResources().getString(R.string.recibo_numero_documento),
                        transaccion.getNumero_documento(),
                        new StyleConfig(StyleConfig.Align.LEFT, gray)
                )
        );

        printRows.add(
                new PrintRow(
                        context.getResources().getString(R.string.recibo_numero_tarjeta),
                        PrinterHandler.getFormatNumTarjeta(transaccion.getNumero_tarjeta()),
                        new StyleConfig(StyleConfig.Align.LEFT, gray, 20)
                )
        );

        for (Saldo saldo : saldos) {

            printRows.add(
                    new PrintRow(
                            saldo.getServicio(),
                            PrintRow.numberFormat(saldo.getSaldo()),
                            new StyleConfig(StyleConfig.Align.LEFT, gray)
                    )
            );
        }

        printRows.add(
                new PrintRow(
                        context.getResources().getString(R.string.recibo_separador_linea),
                        new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1, 10)
                )
        );

        printRows.add(
                new PrintRow(
                        context.getResources().getString(R.string.recibo_total),
                        CofremUtils.getTotalSaldo(saldos),
                        new StyleConfig(StyleConfig.Align.LEFT, gray)
                )
        );

        printRows.add(
                new PrintRow(
                        context.getResources().getString(R.string.recibo_separador_linea),
                        new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1, 50)
                )
        );

        printRows.add(
                new PrintRow(
                        ".",
                        new StyleConfig(StyleConfig.Align.LEFT, 1)
                )
        );

        int status = new PrinterHandler().imprimerTexto(printRows);

        if (status == InfoGlobalSettingsPrint.PRINTER_OK) {
            postEvent(SaldoEvent.Type.onImprecionReciboSuccess);
        } else {
            postEvent(SaldoEvent.Type.onImprecionReciboError, PrinterHandler.stringErrorPrinter(status, context));
        }
    }

    private void postEvent(SaldoEvent.Type type, String errorMessage,
                           InformacionSaldo informacionSaldo,
                           Saldo[] saldos) {
        SaldoEvent saldoEvent = new SaldoEvent();
        saldoEvent.setType(type);
        if (errorMessage != null) {
            saldoEvent.setErrorMessage(errorMessage);
        }
        if (informacionSaldo != null) {
            saldoEvent.setInformacionSaldo(informacionSaldo);
        }
        if (saldos != null) {
            saldoEvent.setSaldos(saldos);
        }
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(saldoEvent);
    }

    private void postEvent(SaldoEvent.Type type) {
        postEvent(type,
                null,
                null,
                null);
    }

    private void postEvent(SaldoEvent.Type type, String errorMessage) {
        postEvent(type, errorMessage, null, null);
    }

    private void postEvent(SaldoEvent.Type type, InformacionSaldo informacionSaldo) {
        postEvent(type, null, informacionSaldo, null);
    }

    private void postEvent(SaldoEvent.Type type, Saldo[] saldo) {
        postEvent(type, null, null, saldo);
    }
}
