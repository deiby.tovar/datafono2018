package com.cofrem.transacciones.modules.configuration.register;

import com.cofrem.transacciones.models.pojo.Configuracion;
import com.cofrem.transacciones.models.pojo.Terminal;
import com.cofrem.transacciones.modules.configuration.register.event.RegisterEvent;

public interface RegisterContract {

    interface view {
        void onVerifyPassLocalValida();

        void onVerifyPassLocalNoValida();

        void onVerifyTerminalSuccess(Terminal terminal);

        void onVerifyTerminalError(String messageError);

        void onRegisterDataServerSuccess();

        void onRegisterDataServerError(String messageError);

        void onRegisterDataDBError(String messageError);

        void onVerifyPassServerValida();

        void onVerifyPassServerNoValida();

        void onGetDataHeaderSuccess();

        void onGetDataHeaderError();

        void onErrorConexion();
    }

    interface presenter {
        void onCreate();

        void onDestroy();

        void verifyPassLocal(String pass);

        void verifyPassServer(String pass);

        void verifyTerminal(Configuracion configuracion);

        void registerData(Configuracion configuracion, Terminal terminal);

        void onEventMainThread(RegisterEvent registerEvent);
    }

    interface repository {
        void verifyPassLocal(String passAdmin);

        void verifyPassServer(String passAdmin);

        void verifyTerminal(Configuracion configuracion);

        void registerData(Configuracion configuracion, Terminal terminal);
    }

}
