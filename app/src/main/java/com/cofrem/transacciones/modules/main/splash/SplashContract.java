package com.cofrem.transacciones.modules.main.splash;

import com.cofrem.transacciones.modules.main.splash.event.SplashEvent;

interface SplashContract {
    interface view {
        void onVerifyHardwareSuccess();

        void onVerifyHardwareError();

        void onVerifyRegistrosInicialesError();

        void onMagneticReaderDeviceError();

        void onNFCDeviceError();

        void onPrinterDeviceError();

        void onInternetConnectionError();

        void onDeviceError();

        void onVerifyConfiguracionExiste();

        void onVerifyConfiguracionNoExiste();

        void onVerifyConfiguracionNoValida();

        void onGetDataHeaderSuccess();

        void onGetDataHeaderError();
    }

    interface presenter {
        void onCreate();

        void onDestroy();

        void verifyHardware();

        void verifyConfiguracion();

        void getDataHeader();

        void onEventMainThread(SplashEvent splashEvent);
    }

    interface repository {
        void verifyConfiguracion();

        void verifyHardware();

        void getDataHeader();
    }
}
