package com.cofrem.transacciones.modules.transaction.credito;

import com.cofrem.transacciones.models.pojo.Servicio;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.credito.event.CreditoEvent;

public interface CreditoContract {
    interface view {
        void onVerifyServiciosSuccess(Servicio[] paramServicio);

        void onVerifyServiciosErrorNoServicios();

        void onVerifyServiciosErrorDocumentoIncorrecto();

        void onVerifyServiciosErrorCambioClave();

        void onRegistrarConsumoSuccess();

        void onRegistrarConsumoTerminalInactiva();

        void onErrorSaldoInsuficiente();

        void onRegistrarConsumoError();

        void onRegistrarConsumoDBError();

        void onImprimirReciboSuccess();

        void onImprimirReciboError(String errorMessage);

        void onErrorTarjetaInactiva();

        void onErrorTarjetaInvalida();

        void onErrorConexion();
    }

    interface presenter {
        /**
         * Metodo encargado de consultar los servicios activos asociados a una tarjeta
         *
         * @param transaccionOLD
         */
        void verifyServicios(Transaccion transaccionOLD);

        /**
         * Metodo encargado de hacer la transaccionOLD
         */
        void registrarConsumo(Transaccion transaccionOLD);

        /**
         * Metodo que imprime el recibo de la transaccionOLD
         */
        void imprimirRecibo(String stringCopia);

        /**
         * Metodo para la creacion del presentador
         */
        void onCreate();

        /**
         * Metodo para la destruccion del presentador
         */
        void onDestroy();

        /**
         * Metodo para recibir los eventos generados
         *
         * @param creditoEvent
         */
        void onEventMainThread(CreditoEvent creditoEvent);
    }

    interface repository {
        /**
         * Metodo encargado de consultar los servicios activos asociados a una tarjeta
         *
         * @param transaccionOLD
         */
        void verifyServicios(Transaccion transaccionOLD);

        /**
         * Metodo encargado de hacer la transaccionOLD
         *
         * @param transaccionOLD
         */
        void registrarConsumo(Transaccion transaccionOLD);

        /**
         * Metodo que imprime el recibo de la transaccionOLD
         */
        void imprimirRecibo(String stringCopia);
    }
}
