package com.cofrem.transacciones.modules.transaction.saldo;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.lib.magneticcard.MagneticHandler;
import com.cofrem.transacciones.core.utils.CofremUtils;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.pojo.Saldo;
import com.cofrem.transacciones.models.pojo.Transaccion;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;

public class SaldoActivity
        extends BaseAppCompatActivity
        implements SaldoContract.view {

    enum PASO_PROCESO {
        DOCUMENTO,
        STRIPE,
        PASS,
        EXITO,
        ERROR,
        FAIL_CARD,
    }

    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */

    @BindView(R.id.clySaldo)
    CoordinatorLayout clySaldo;

    //Elementos del modulo//////////////////////////////////////////////////////////////////////////
    @BindView(R.id.bodySaldoNumDoc)
    View bodySaldoNumDoc;

    @BindView(R.id.bodySaldoStripe)
    View bodySaldoStripe;

    @BindView(R.id.bodySaldoPass)
    View bodySaldoPass;

    @BindView(R.id.bodySaldoExito)
    View bodySaldoExito;

    @BindView(R.id.bodySaldoFailCard)
    View bodySaldoFailCard;

    @BindView(R.id.bodySaldoError)
    View bodySaldoError;

    //Paso DOCUMENTO////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtNumDocValue)
    EditText edtNumDocValue;

    //Paso STRIPE///////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvStripeNumDoc)
    TextView txvStripeNumDoc;

    //Paso PASS/////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtPassValue)
    EditText edtPassValue;

    //Paso EXITO////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.rlyExitoNumTarjetaContainer)
    RelativeLayout rlyExitoNumTarjetaContainer;

    @BindView(R.id.txvExitoNumTarjeta)
    TextView txvExitoNumTarjeta;

    @BindView(R.id.rlyExitoNumDocContainer)
    RelativeLayout rlyExitoNumDocContainer;

    @BindView(R.id.txvExitoNumDoc)
    TextView txvExitoNumDoc;

    @BindView(R.id.rlyExitoSaldoContainer)
    RelativeLayout rlyExitoSaldoContainer;

    @BindView(R.id.txvExitoSaldo)
    TextView txvExitoSaldo;

    @BindView(R.id.btnExitoImprimir)
    Button btnExitoImprimir;

    //Paso ERROR////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvErrorMessage)
    TextView txvErrorMessage;

    Saldo[] saldos;

    //Cambio de paso
    private PASO_PROCESO pasoTransaccion = PASO_PROCESO.DOCUMENTO;

    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    Transaccion transaccion = new Transaccion();

    private SaldoContract.presenter presenter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transaction_module_saldo);

        setupTitles(getString(R.string.transaction_text_title_saldo));

        coordinatorBase = clySaldo;

        typeActivity = TIPO_ACT.MODULO;

        bodyProceso = new View[]{
                bodySaldoNumDoc,
                bodySaldoStripe,
                bodySaldoPass,
                bodySaldoExito,
                bodySaldoFailCard,
                bodySaldoError,
        };

        edtProceso = new EditText[]{
                edtNumDocValue,
                edtPassValue,
        };

        //Instanciamiento e inicializacion del presentador
        presenter = new SaldoPresenter(this);

        //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /**
     * Implementacion de metodo abstracto para manejo de presion de la tecla enter
     */
    public void enterPressed() {
        switch (pasoTransaccion) {
            case DOCUMENTO:
                registerNumDoc();
                break;
            case STRIPE:
                stripe();
                break;
            case PASS:
                registerPass();
                break;
            case EXITO:
            case ERROR:
            case FAIL_CARD:
            default:
                navigateToMain(this);
                break;


        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */

    /**
     * Metodo para registrar el numero de documento
     */
    @OnClick(R.id.btnNumDocAceptar)
    public void registerNumDoc() {

        String numDoc = edtNumDocValue.getText().toString();

        cleanEditText();

        if (numDoc.length() > 0) {

            //Registro de numero de documento
            transaccion.setNumero_documento(numDoc);

            //Numero de documento en Stripe
            txvStripeNumDoc.setText(String.valueOf(transaccion.getNumero_documento()));

            //Cambio de paso
            pasoTransaccion = PASO_PROCESO.STRIPE;

            //Avance de vista
            avanceView();

            //Lectura de tarjeta
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    stripe();
                }
            }, 100);

        } else {

            //Muestra el mensaje de error de formato de la contraseña
            Toast.makeText(this, R.string.transaction_error_numero_documento, Toast.LENGTH_SHORT).show();

        }

    }

    /**
     * Metodo para mostrar la orden de deslizar la tarjeta
     */
    private void stripe() {

        String[] readMagnetic = new MagneticHandler().readMagnetic();

        if (readMagnetic != null) {

            transaccion.setNumero_tarjeta(CofremUtils.cleanNumberCard(readMagnetic));

            readCardSuccess();

        } else {
            readCardError();
        }
    }

    /**
     * Metodo para mostrar la lectura correcta de tarjeta
     */
    private void readCardSuccess() {

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.PASS;

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo para mostrar la lectura erronea de tarjeta
     */
    private void readCardError() {

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.FAIL_CARD;

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo para registrar la contraseña del usuario
     */
    @OnClick(R.id.btnPassAceptar)
    public void registerPass() {

        String pass = edtPassValue.getText().toString();

        cleanEditText();

        if (pass.length() == 4) {

            showProgress();

            transaccion.setClave(Integer.valueOf(pass));

            //Se registra el tipo de encriptacion en el modelo
            transaccion.setTipo_encriptacion(Transaccion.CODIGO_ENCR_NO_ENCRIPTADO);

            presenter.consultarSaldo(transaccion);
        } else {
            showMessage(getResources().getString(R.string.transaction_error_format_clave_usuario));
        }
    }

    /**
     * Metodo para finalizar la transaccion
     */
    @OnClick(R.id.btnExitoImprimir)
    public void printRecibo() {
        presenter.imprimirRecibo(transaccion, saldos);
    }

    /**
     * Metodo para regresar a la ventana de transaccion
     */
    @OnClick({R.id.btnNumDocCancelar,
            R.id.btnPassCancelar,
            R.id.btnServicioCancelar,
            R.id.btnExitoSalir,
            R.id.btnFailCardSalir,
            R.id.btnErrorSalir,})
    public void navigateToMain() {
        super.navigateToMain(this);
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */

    /**
     *
     */
    @Override
    public void onConsultarSaldoSuccess(Saldo[] saldos) {

        hideProgress();

        this.saldos = saldos;

        String totalSaldo = saldos.length > 0 ? CofremUtils.getTotalSaldo(saldos) : PrintRow.numberFormat(0);

        txvExitoNumTarjeta.setText(transaccion.getNumero_tarjeta());

        txvExitoNumDoc.setText(transaccion.getNumero_documento());

        txvExitoSaldo.setText(totalSaldo);

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.EXITO;

        //Avance de vista
        avanceView();

    }

    /**
     *
     */
    @Override
    public void onConsultarSaldoError(String error) {
        handlerErrorView("Error desconocido al consultar saldo, intentelo nuevamente");
    }

    /**
     * Metodo para manejar la orden de imprimir recibo exitosa
     */
    @Override
    public void onImprimirReciboSuccess() {
        hideProgress();
    }

    /**
     * Metodo para manejar la orden de imprimir recibo Error
     */
    @Override
    public void onImprimirReciboError(String errorMessage) {
        hideProgress();
        showMessage(errorMessage);
    }

    @Override
    public void onErrorConexion() {
        handlerErrorView(getString(R.string.configuration_text_estado_conexion));
    }
    /**
     * #############################################################################################
     * Metodo de manejos de vistas
     * #############################################################################################
     */
    /**
     * @param errorMessage
     */
    private void handlerErrorView(String errorMessage) {

        hideProgress();

        txvErrorMessage.setText(errorMessage);

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.ERROR;

        //Avance de vista
        avanceView();
    }

    private void avanceView() {

        cleanEditText();

        hideBody();

        switch (pasoTransaccion) {
            case DOCUMENTO:
                bodySaldoNumDoc.setVisibility(View.VISIBLE);
                break;
            case STRIPE:
                bodySaldoStripe.setVisibility(View.VISIBLE);
                break;
            case PASS:
                bodySaldoPass.setVisibility(View.VISIBLE);
                break;
            case EXITO:

                rlyExitoNumTarjetaContainer.setVisibility(View.VISIBLE);
                txvExitoNumTarjeta.setVisibility(View.VISIBLE);

                rlyExitoNumDocContainer.setVisibility(View.VISIBLE);
                txvExitoNumDoc.setVisibility(View.VISIBLE);

                rlyExitoSaldoContainer.setVisibility(View.VISIBLE);
                txvExitoSaldo.setVisibility(View.VISIBLE);

                btnExitoImprimir.setVisibility(View.VISIBLE);

                bodySaldoExito.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                bodySaldoError.setVisibility(View.VISIBLE);
                break;
            case FAIL_CARD:
                bodySaldoFailCard.setVisibility(View.VISIBLE);
                break;

        }
    }

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    @OnLongClick({R.id.edtNumDocValue,
            R.id.edtPassValue,})
    @OnTouch({R.id.edtNumDocValue,
            R.id.edtPassValue,})
    public boolean hideKeyBoardLong(View v) {
        hideKeyBoard(v);
        return true;
    }

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    @OnClick({R.id.edtNumDocValue,
            R.id.edtPassValue,})
    public void hideKeyBoard(View v) {
        super.hideKeyBoard(v);
    }

}
