package com.cofrem.transacciones.modules.transaction.cambio_clave;

import com.cofrem.transacciones.models.Tarjeta;
import com.cofrem.transacciones.modules.transaction.cambio_clave.event.CambioClaveEvent;

interface CambioClaveContract {
    interface view {
        void onValidarPassActualSuccess();

        void onVerifyPassActualErrorPassIncorrecta();

        void onVerifyPassActualErrorDocumentoIncorrecto();

        void onVerifyPassActualErrorTarjetaInactiva();

        void onVerifyPassActualErrorTarjetaInvalida();

        void onVerifyPassNuevaSuccess();

        void onVerifyPassNuevaErrorPassIncorrecta();

        void onVerifyPassNuevaErrorDocumentoIncorrecto();

        void onVerifyPassNuevaErrorPassFormatoIncorrecto();

        void onVerifyPassNuevaErrorTarjetaInactiva();

        void onVerifyPassNuevaErrorTarjetaInvalida();

        void onErrorConexion();
    }

    interface presenter {
        /**
         * Metodo encargado de validar la informacion de la tarjeta
         *
         * @param tarjeta
         */
        void verifyPassActual(Tarjeta tarjeta);

        /**
         * Metodo encargado de validar la informacion de la tarjeta
         *
         * @param tarjeta
         */
        void verifyPassNueva(Tarjeta tarjeta);

        /**
         * Metodo para la creacion del presentador
         */
        void onCreate();

        /**
         * Metodo para la destruccion del presentador
         */
        void onDestroy();

        /**
         * Metodo para recibir los eventos generados
         *
         * @param cambioClaveEvent
         */
        void onEventMainThread(CambioClaveEvent cambioClaveEvent);
    }

    interface repository {
        /**
         * Metodo encargado de validar la informacion de la tarjeta
         *
         * @param tarjeta
         */
        void verifyPassActual(Tarjeta tarjeta);

        /**
         * Metodo encargado de validar la informacion de la tarjeta
         *
         * @param tarjeta
         */
        void verifyPassNueva(Tarjeta tarjeta);
    }
}
