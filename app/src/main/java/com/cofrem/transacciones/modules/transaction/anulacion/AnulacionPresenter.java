package com.cofrem.transacciones.modules.transaction.anulacion;

import android.content.Context;

import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.anulacion.event.AnulacionEvent;

import org.greenrobot.eventbus.Subscribe;

public class AnulacionPresenter implements AnulacionContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private AnulacionContract.view view;

    private AnulacionContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public AnulacionPresenter(AnulacionContract.view view) {
        this.view = view;
        this.repository = new AnulacionRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    /**
     * @param passAdmin
     */
    @Override
    public void validatePassAdmin(String passAdmin) {
        repository.validatePassAdmin(passAdmin);
    }

    /**
     * @param numCargo
     */
    @Override
    public void getValorTransaccion(String numCargo) {
        repository.getValorTransaccion(numCargo);
    }

    /**
     * @param transaccion
     */
    @Override
    public void registerAnulacion(Transaccion transaccion) {
        repository.registerAnulacion(transaccion);
    }

    @Override
    public void imprimirRecibo(String stringCopia) {
        repository.imprimirRecibo(stringCopia);
    }

    @Override
    @Subscribe
    public void onEventMainThread(AnulacionEvent anulacionEvent) {
        switch (anulacionEvent.getType()) {
            case onValidatePassAdminCorrecta:
                onValidatePassAdminCorrecta();
                break;
            case onvalidatePassAdminError:
                onvalidatePassAdminError(anulacionEvent.getErrorMessage());
                break;
            case onGetValorTransaccionSuccess:
                onGetValorTransaccionSuccess(anulacionEvent.getValorInt());
                break;
            case onGetValorTransaccionError:
                onGetValorTransaccionError();
                break;
            case onRegisterAnulacionSuccess:
                onRegisterAnulacionSuccess();
                break;
            case onRegisterAnulacionError:
                onRegisterAnulacionError();
                break;
            case onImprimirReciboSuccess:
                onImpresionReciboSuccess();
                break;
            case onImprimirReciboError:
                onImpresionReciboError(anulacionEvent.getErrorMessage());
                break;
            case onErrorConexion:
                onErrorConexion();
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void onValidatePassAdminCorrecta() {
        if (view != null) {
            view.onValidatePassAdminCorrecta();
        }
    }

    private void onvalidatePassAdminError(String errorMessage) {
        if (view != null) {
            view.onvalidatePassAdminError(errorMessage);
        }
    }

    private void onGetValorTransaccionSuccess(int valor) {
        if (view != null) {
            view.onGetValorTransaccionSuccess(valor);
        }
    }

    private void onGetValorTransaccionError() {
        if (view != null) {
            view.onGetValorTransaccionError();
        }
    }

    private void onRegisterAnulacionSuccess() {
        if (view != null) {
            view.onRegisterAnulacionSuccess();
        }
    }

    private void onRegisterAnulacionError() {
        if (view != null) {
            view.onRegisterAnulacionError();
        }
    }

    private void onImpresionReciboSuccess() {
        if (view != null) {
            view.onImprimirReciboSuccess();
        }
    }

    private void onImpresionReciboError(String errorMessage) {
        if (view != null) {
            view.onImprimirReciboError(errorMessage);
        }
    }

    private void onErrorConexion() {
        if (view != null) {
            view.onErrorConexion();
        }
    }

}
