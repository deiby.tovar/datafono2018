package com.cofrem.transacciones.modules.main.splash;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.modules.main.splash.event.SplashEvent;
import org.greenrobot.eventbus.Subscribe;

public class SplashPresenter implements SplashContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private SplashContract.view view;

    private SplashContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public SplashPresenter(SplashContract.view view) {
        this.view = view;
        this.repository = new SplashRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    /**
     * Metodo que verifica:
     * - Conexion a internet
     * - Existencia datos en DB interna
     * - Coherencia de datos con el servidor
     */
    @Override
    public void verifyHardware() {
        if (view != null) {
            repository.verifyHardware();
        }
    }

    /**
     * Metodo que verifica:
     * - La existencia de la configuración inicial
     * - En caso de no existir mostrará la vista de configuración
     * - En caso de existir validara el acceso
     */
    @Override
    public void verifyConfiguracion() {
        if (view != null) {
            repository.verifyConfiguracion();
        }
    }

    @Override
    public void getDataHeader() {
        if (view != null) {
            repository.getDataHeader();
        }
    }

    @Override
    @Subscribe
    public void onEventMainThread(SplashEvent splashEvent) {
        switch (splashEvent.getType()) {
            case onVerifyHardwareSuccess:
                onVerifyHardwareSuccess();
                break;
            case onVerifyHardwareError:
                onVerifyHardwareError();
                break;
            case onVerifyRegistrosInicialesError:
                onVerifyRegistrosInicialesError();
                break;
            case onMagneticReaderDeviceError:
                onMagneticReaderDeviceError();
                break;
            case onNFCDeviceError:
                onNFCDeviceError();
                break;
            case onPrinterDeviceError:
                onPrinterDeviceError();
                break;
            case onInternetConnectionError:
                onInternetConnectionError();
                break;
            case onDeviceError:
                onDeviceError();
                break;
            case onVerifyConfiguracionExiste:
                onVerifyConfiguracionExiste();
                break;
            case onVerifyConfiguracionNoExiste:
                onVerifyConfiguracionNoExiste();
                break;
            case onVerifyConfiguracionNoValida:
                onVerifyConfiguracionNoValida();
                break;
            case onGetDataHeaderSuccess:
                onGetDataHeaderSuccess();
                break;
            case onGetDataHeaderError:
                onGetDataHeaderError();
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void onVerifyHardwareSuccess() {
        if (view != null) {
            view.onVerifyHardwareSuccess();
        }
    }

    /**
     * Metodo para manejar la verificacion erronea
     */
    private void onVerifyRegistrosInicialesError() {
        if (view != null) {
            view.onVerifyRegistrosInicialesError();
        }
    }

    /**
     * Metodo para manejar la verificacion erronea
     */
    private void onVerifyHardwareError() {
        if (view != null) {
            view.onVerifyHardwareError();
        }
    }

    /**
     * Metodo para manejar la conexion al dispositivo lector de banda magnetica erronea
     */
    private void onMagneticReaderDeviceError() {
        if (view != null) {
            view.onMagneticReaderDeviceError();
        }
    }

    /**
     * Metodo para manejar la conexion al dispositivo NFC erronea
     */
    private void onNFCDeviceError() {
        if (view != null) {
            view.onNFCDeviceError();
        }
    }

    /**
     * Metodo para manejar la conexion al dispositivo de impresion erronea
     */
    private void onPrinterDeviceError() {
        if (view != null) {
            view.onPrinterDeviceError();
        }
    }

    /**
     * Metodo para manejar la conexion a internet erronea
     */
    private void onInternetConnectionError() {
        if (view != null) {
            view.onInternetConnectionError();
        }
    }

    /**
     * Metodo para manejar la conexion al dispositivo de impresion erronea
     */
    private void onDeviceError() {
        if (view != null) {
            view.onDeviceError();
        }
    }

    private void onVerifyConfiguracionExiste() {
        if (view != null) {
            view.onVerifyConfiguracionExiste();
        }
    }

    private void onVerifyConfiguracionNoExiste() {
        if (view != null) {
            view.onVerifyConfiguracionNoExiste();
        }
    }

    /**
     * Metodo para manejar la existencia de la configuracion inicial NO valida
     */
    private void onVerifyConfiguracionNoValida() {
        if (view != null) {
            view.onVerifyConfiguracionNoValida();
        }
    }

    private void onGetDataHeaderSuccess() {
        if (view != null) {
            view.onGetDataHeaderSuccess();
        }
    }

    /**
     * Metodo para manejar la existencia de la configuracion inicial NO valida
     */
    private void onGetDataHeaderError() {
        if (view != null) {
            view.onGetDataHeaderError();
        }
    }
}
