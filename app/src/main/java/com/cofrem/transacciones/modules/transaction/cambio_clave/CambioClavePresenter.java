package com.cofrem.transacciones.modules.transaction.cambio_clave;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.models.Tarjeta;
import com.cofrem.transacciones.modules.transaction.cambio_clave.event.CambioClaveEvent;
import org.greenrobot.eventbus.Subscribe;

public class CambioClavePresenter implements CambioClaveContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private CambioClaveContract.view view;

    private CambioClaveContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public CambioClavePresenter(CambioClaveContract.view view) {
        this.view = view;
        this.repository = new CambioClaveRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    /**
     * @param tarjeta
     */
    @Override
    public void verifyPassActual(Tarjeta tarjeta) {
        repository.verifyPassActual(tarjeta);
    }

    /**
     * @param tarjeta
     */
    @Override
    public void verifyPassNueva(Tarjeta tarjeta) {
        repository.verifyPassNueva(tarjeta);
    }

    @Override
    @Subscribe
    public void onEventMainThread(CambioClaveEvent cambioClaveEvent) {
        switch (cambioClaveEvent.getType()) {
            case onVerifyPassActualSuccess:
                onValidarPassActualSuccess();
                break;
            case onVerifyPassActualErrorPassIncorrecta:
                onVerifyPassActualErrorPassIncorrecta();
                break;
            case onVerifyPassActualErrorDocumentoIncorrecto:
                onVerifyPassActualErrorDocumentoIncorrecto();
                break;
            case onVerifyPassActualErrorTarjetaInactiva:
                onVerifyPassActualErrorTarjetaInactiva();
                break;
            case onVerifyPassActualErrorTarjetaInvalida:
                onVerifyPassActualErrorTarjetaInvalida();
                break;
            case onVerifyPassNuevaSuccess:
                onVerifyPassNuevaSuccess();
                break;
            case onVerifyPassNuevaErrorPassIncorrecta:
                onVerifyPassNuevaErrorPassIncorrecta();
                break;
            case onVerifyPassNuevaErrorDocumentoIncorrecto:
                onVerifyPassNuevaErrorDocumentoIncorrecto();
                break;
            case onVerifyPassNuevaErrorPassFormatoIncorrecto:
                onVerifyPassNuevaErrorPassFormatoIncorrecto();
                break;
            case onVerifyPassNuevaErrorTarjetaInactiva:
                onVerifyPassNuevaErrorTarjetaInactiva();
                break;
            case onVerifyPassNuevaErrorTarjetaInvalida:
                onVerifyPassNuevaErrorTarjetaInvalida();
                break;
            case onErrorConexion:
                onErrorConexion();
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void onValidarPassActualSuccess() {
        if (view != null) {
            view.onValidarPassActualSuccess();
        }
    }

    private void onVerifyPassActualErrorPassIncorrecta() {
        if (view != null) {
            view.onVerifyPassActualErrorPassIncorrecta();
        }
    }

    private void onVerifyPassActualErrorDocumentoIncorrecto() {
        if (view != null) {
            view.onVerifyPassActualErrorDocumentoIncorrecto();
        }
    }

    private void onVerifyPassActualErrorTarjetaInactiva() {
        if (view != null) {
            view.onVerifyPassActualErrorTarjetaInactiva();
        }
    }

    private void onVerifyPassActualErrorTarjetaInvalida() {
        if (view != null) {
            view.onVerifyPassActualErrorTarjetaInvalida();
        }
    }

    private void onVerifyPassNuevaSuccess() {
        if (view != null) {
            view.onVerifyPassNuevaSuccess();
        }
    }

    private void onVerifyPassNuevaErrorPassIncorrecta() {
        if (view != null) {
            view.onVerifyPassNuevaErrorPassIncorrecta();
        }
    }

    private void onVerifyPassNuevaErrorDocumentoIncorrecto() {
        if (view != null) {
            view.onVerifyPassNuevaErrorDocumentoIncorrecto();
        }
    }

    private void onVerifyPassNuevaErrorPassFormatoIncorrecto() {
        if (view != null) {
            view.onVerifyPassNuevaErrorPassFormatoIncorrecto();
        }
    }

    private void onVerifyPassNuevaErrorTarjetaInactiva() {
        if (view != null) {
            view.onVerifyPassNuevaErrorTarjetaInactiva();
        }
    }

    private void onVerifyPassNuevaErrorTarjetaInvalida() {
        if (view != null) {
            view.onVerifyPassNuevaErrorTarjetaInvalida();
        }
    }

    private void onErrorConexion() {
        if (view != null) {
            view.onErrorConexion();
        }
    }
}
