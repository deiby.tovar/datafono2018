package com.cofrem.transacciones.modules.configuration.printer;

import android.content.Context;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.global.InfoGlobalSettingsPrint;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.printer.PrinterHandler;
import com.cofrem.transacciones.core.lib.printer.StyleConfig;
import com.cofrem.transacciones.database.AppDatabase;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;
import com.cofrem.transacciones.modules.configuration.printer.event.PrinterEvent;

import java.util.ArrayList;

public class PrinterRepository implements PrinterContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public PrinterRepository(Context context) {
        this.context = context;
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    @Override
    public void verifyConfigPrinterInitial() {
        ConfigurationPrinter configurationPrinter = AppDatabase.getInstance(context).getConfigurationPrinter();
        if (configurationPrinter != null)
            postEvent(PrinterEvent.Type.onVerifyConfigPrinterInitialSuccess, configurationPrinter);
        else {
            postEvent(PrinterEvent.Type.onVerifyConfigPrinterInitialError);
        }
    }

    /**
     * @param configurationPrinter
     */
    @Override
    public void saveConfigPrinter(ConfigurationPrinter configurationPrinter) {
        if (AppDatabase.getInstance(context).insertConfigurationPrinter(configurationPrinter))
            postEvent(PrinterEvent.Type.onSaveConfigPrinterSuccess);
        else {
            postEvent(PrinterEvent.Type.onSaveConfigPrinterError);
        }
    }

    /**
     * @param gray
     */
    @Override
    public void printTest(int gray) {
    // creamos el ArrayList se que encarga de almacenar los rows del recibo
        ArrayList<PrintRow> printRows = new ArrayList<PrintRow>();
    //Se agrega el logo al primer renglon del recibo y se coloca en el centro
        printRows.add(PrintRow.printLogo(context, gray));
        PrintRow.printCofrem(context, printRows, gray, 10);
    //se siguen agregando cado auno de los String a los renglones (Rows) del recibo para imprimir
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_title_prueba_impresora), new StyleConfig(StyleConfig.Align.CENTER, gray, 50)));
        printRows.add(new PrintRow(".", new StyleConfig(StyleConfig.Align.LEFT, 1)));
        int status = new PrinterHandler().imprimerTexto(printRows);
        if (status == InfoGlobalSettingsPrint.PRINTER_OK) {
            postEvent(PrinterEvent.Type.onPrintTestSuccess);
        } else {
            postEvent(PrinterEvent.Type.onPrintTestError, PrinterHandler.stringErrorPrinter(status, context));
        }
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void postEvent(PrinterEvent.Type type,
                           ConfigurationPrinter configuracion,
                           String errorMessage) {
        PrinterEvent printerEvent = new PrinterEvent();
        printerEvent.setType(type);
        if (errorMessage != null) {
            printerEvent.setErrorMessage(errorMessage);
        }
        if (configuracion != null) {
            printerEvent.setConfigurationPrinter(configuracion);
        }
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(printerEvent);
    }

    private void postEvent(PrinterEvent.Type type) {
        postEvent(type,
                null,
                null
        );
    }

    private void postEvent(PrinterEvent.Type type,
                           ConfigurationPrinter configuration) {
        postEvent(type,
                configuration,
                null);
    }

    private void postEvent(PrinterEvent.Type type,
                           String error) {
        postEvent(type,
                null,
                error);
    }
}
