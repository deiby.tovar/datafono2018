package com.cofrem.transacciones.modules.configuration.comm;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.modules.configuration.comm.event.CommEvent;
import org.greenrobot.eventbus.Subscribe;

public class CommPresenter implements CommContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private CommContract.view view;

    private CommContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public CommPresenter(CommContract.view view) {
        this.view = view;
        this.repository = new CommRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    @Override
    public void commTest() {
        repository.commTest();
    }

    @Override
    @Subscribe
    public void onEventMainThread(CommEvent commEvent) {
        switch (commEvent.getType()) {
            case onCommTestSuccess:
                onTestComunicationSuccess();
                break;
            case onCommTestError:
                onTestComunicationError(commEvent.getErrorMessage());
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void onTestComunicationSuccess() {
        if (view != null) {
            view.onTestComunicationSuccess();
        }
    }

    private void onTestComunicationError(String error) {
        if (view != null) {
            view.onTestComunicationError(error);
        }
    }
}
