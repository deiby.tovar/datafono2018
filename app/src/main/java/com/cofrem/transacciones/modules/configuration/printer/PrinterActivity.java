package com.cofrem.transacciones.modules.configuration.printer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;
import com.cofrem.transacciones.modules.configuration.ConfigurationActivity;

public class PrinterActivity
        extends BaseAppCompatActivity
        implements PrinterContract.view {

    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    /**
     * Declaracion de los Contoles
     */
    @BindView(R.id.clyConfigurationPrinter)
    CoordinatorLayout clyConfigurationPrinter;
    @BindView(R.id.skbPrinterGrayLevel)
    SeekBar skbPrinterGrayLevel;
    @BindView(R.id.skbPrinterFontSize)
    SeekBar skbPrinterFontSize;
    @BindView(R.id.txvPrinterGrayLevel)
    TextView txvPrinterGrayLevel;
    @BindView(R.id.txvPrinterFontSize)
    TextView txvPrinterFontSize;
    @BindView(R.id.btnPrinterAceptar)
    Button btnPrinterAceptar;
    @BindView(R.id.btnPrinterCancelar)
    Button btnPrinterCancelar;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */
    private PrinterContract.presenter presenter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_module_printer);
    //Instanciamiento e inicializacion del presentador
        presenter = new PrinterPresenter(this);
    //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();
    //metodo verifyDatos acceso
        presenter.verifyConfigPrinterInitial();
        skbPrinterGrayLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txvPrinterGrayLevel.setText(getString(R.string.configuration_text_gray_level, progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        skbPrinterFontSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txvPrinterFontSize.setText(getString(R.string.configuration_text_font_size, progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        coordinatorBase = clyConfigurationPrinter;
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados del sistema
     * #############################################################################################
     */
    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    /**
     * @param configurationPrinter
     */
    private void setupConfigPrinter(ConfigurationPrinter configurationPrinter) {
        skbPrinterFontSize.setProgress(configurationPrinter.getFont_size() - 1);
        skbPrinterGrayLevel.setProgress(configurationPrinter.getGray_level() - 1);
        txvPrinterGrayLevel.setText(this.getString(R.string.configuration_text_gray_level, skbPrinterGrayLevel.getProgress() + 1));
        txvPrinterFontSize.setText(this.getString(R.string.configuration_text_font_size, skbPrinterFontSize.getProgress() + 1));
    }

    @OnClick(R.id.btnPrinterTest)
    public void printerTest() {
        presenter.printTest( skbPrinterGrayLevel.getProgress() + 1);
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    /**
     * @param configurationPrinter
     */
    @Override
    public void onVerifyConfigPrinterInitialSuccess(ConfigurationPrinter configurationPrinter) {
        setupConfigPrinter(configurationPrinter);
    }

    @Override
    public void onVerifyConfigPrinterInitialError() {
        ConfigurationPrinter configurationPrinter = new ConfigurationPrinter(
                ConfigurationPrinter.default_gray_level,
                ConfigurationPrinter.default_font_size
        );
        setupConfigPrinter(configurationPrinter);
    }

    @Override
    public void onSaveConfigPrinterSuccess() {
        showMessage(getString(R.string.configuration_text_registro_printer_success));
        navigateToConfigurationScreen();
    }

    @Override
    public void onSaveConfigPrinterError() {
        showMessage(getString(R.string.configuration_text_registro_printer_error));
    }

    @Override
    public void onPrintTestSuccess() {
    }

    /**
     * @param messageError
     */
    @Override
    public void onPrintTestError(String messageError) {
        showMessage(messageError);
    }

    @OnClick(R.id.btnPrinterAceptar)
    void saveConfigurationPrinter() {
        ConfigurationPrinter configuration = new ConfigurationPrinter();
        configuration.setFont_size(skbPrinterFontSize.getProgress() + 1);
        configuration.setGray_level(skbPrinterGrayLevel.getProgress() + 1);
        presenter.saveConfigPrinter( configuration);
    }

    @OnClick(R.id.btnPrinterCancelar)
    void navigateToConfigurationScreen() {
        Intent intent = new Intent(this, ConfigurationActivity.class);
    //Agregadas banderas para no retorno
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}