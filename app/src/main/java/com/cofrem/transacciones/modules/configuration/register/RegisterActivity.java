package com.cofrem.transacciones.modules.configuration.register;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.filters.FilterHost;
import com.cofrem.transacciones.core.global.InfoGlobalSettings;
import com.cofrem.transacciones.core.global.InfoGlobalValues;
import com.cofrem.transacciones.core.utils.KeyBoard;
import com.cofrem.transacciones.models.pojo.Configuracion;
import com.cofrem.transacciones.models.pojo.Terminal;
import com.cofrem.transacciones.modules.configuration.ConfigurationActivity;
import com.cofrem.transacciones.modules.main.splash.SplashActivity;

import static android.view.KeyEvent.KEYCODE_ENTER;

public class RegisterActivity
        extends BaseAppCompatActivity
        implements RegisterContract.view {
    /*
      #############################################################################################
      Declaracion de controles y variables
      #############################################################################################
     */
    private static final int PASO_PASS_TECNICO_LOCAL = InfoGlobalValues.configuracionPassLocal;
    private static final int PASO_PASS_TECNICO_SERVER = InfoGlobalValues.configuracionPassServer;
    private static final int PASO_HOST = 3;
    private static final int PASO_DISPOSITIVO = 4;
    private static final int PASO_EXITO = 5;
    //Vistas del modulo
    @BindView(R.id.clyConfigurationRegister)
    CoordinatorLayout clyConfigurationRegister;
    @BindView(R.id.bodyRegisterPass)
    View bodyConfigRegisterPass;
    @BindView(R.id.bodyRegisterHost)
    View bodyConfigRegisterHost;
    @BindView(R.id.bodyRegisterTerminal)
    View bodyConfigRegisterTerminal;
    @BindView(R.id.bodyRegisterExito)
    View bodyConfigRegisterExito;
    //config_register_pass
    @BindView(R.id.edtPassConfigValue)
    EditText edtConfigRegisterPass;
    @BindView(R.id.btnPassConfigCancelar)
    Button btnConfigRegisterPassCancelar;
    //config_register_pass_host
    @BindView(R.id.edtHostValue)
    EditText edtConfigRegisterHostServer;
    @BindView(R.id.edtHostId)
    EditText edtConfigRegisterHostId;
    @BindView(R.id.btnHostCancelar)
    Button btnConfigRegisterHostCancelar;
    //config_register_info_terminal
    @BindView(R.id.txvTerminalEstablecimiento)
    TextView txvConfigRegisterTerminalEstablecimiento;
    @BindView(R.id.txvTerminalSucursal)
    TextView txvConfigRegisterTerminalSucursal;
    @BindView(R.id.txvTerminalCodigo)
    TextView txvConfigRegisterTerminalCodigo;
    //config_register_exito
    @BindView(R.id.txvExitoMessage)
    TextView txvGeneralExitoMessage;
    //Variables
    private int valueActivityBack;
    private Configuracion configuracion = new Configuracion();
    private Terminal terminal;
    //Pasos definidos
    private int pasoRegisterConfiguration = 0;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private RegisterContract.presenter presenter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_module_register);
        setupTitles(getString(R.string.configuration_text_title_configuracion));
        //Instanciamiento e inicializacion del presentador
        presenter = new RegisterPresenter(this);
        //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();
        setupUI();
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados del sistema
     * #############################################################################################
     */
    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /**
     * Metodo que intercepta las pulsaciones de las teclas del teclado fisico
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        /**
         * Keycodes disponibles
         *
         * 4: Back
         * 66: Enter
         * 67: Delete
         *
         */
        switch (keyCode) {
            case KEYCODE_ENTER:
                switch (pasoRegisterConfiguration) {
                    case PASO_PASS_TECNICO_LOCAL:
                    case PASO_PASS_TECNICO_SERVER:
                        verifyPass();
                        break;
                    case PASO_HOST:
                        verifyTerminal();
                        break;
                    case PASO_DISPOSITIVO:
                        aceptarInformacionTerminal();
                        break;
                }
                break;
            default:
                Log.i("Key Pressed", String.valueOf(keyCode));
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Metodo que interfiere la presion del boton "Back"
     */
    @Override
    public void onBackPressed() {
        switch (pasoRegisterConfiguration) {
            case PASO_PASS_TECNICO_LOCAL:
            case PASO_PASS_TECNICO_SERVER:
                edtConfigRegisterPass.setText("");
                break;
            case PASO_HOST:
                edtConfigRegisterHostServer.setText("");
                edtConfigRegisterHostId.setText("");
                break;
            case PASO_DISPOSITIVO:
                edtConfigRegisterHostServer.setText("");
                edtConfigRegisterHostId.setText("");
                break;
        }
    }

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    @OnLongClick({R.id.edtPassConfigValue,
            R.id.edtHostValue,
            R.id.edtHostId,
    })
    @OnTouch({R.id.edtPassConfigValue,
            R.id.edtHostValue,
            R.id.edtHostId,
    })
    public boolean hideKeyBoardLong(View v) {
        hideKeyBoard(v);
        return true;
    }

    @OnClick({R.id.edtPassConfigValue,
            R.id.edtHostValue,
            R.id.edtHostId,
    })
    public void hideKeyBoard(View v) {
        KeyBoard.hide(this);
        edtConfigRegisterPass.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT));
        edtConfigRegisterHostServer.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT));
        edtConfigRegisterHostId.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT));
        switch (v.getId()) {
            case R.id.edtPassConfigValue:
                edtConfigRegisterPass.requestFocus();
                edtConfigRegisterPass.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT_FOCUS));
                break;
            case R.id.edtHostValue:
                edtConfigRegisterHostServer.requestFocus();
                edtConfigRegisterHostServer.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT_FOCUS));
                break;
            case R.id.edtHostId:
                edtConfigRegisterHostId.requestFocus();
                edtConfigRegisterHostId.setBackgroundColor(Color.parseColor(InfoGlobalSettings.COLOR_EDITEXT_FOCUS));
                break;
        }
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    /**
     * Metodo que envia la contraseña ingresada para su validacion
     */
    @OnClick(R.id.btnPassConfigAceptar)
    public void verifyPass() {
        String paramPassword = edtConfigRegisterPass.getText().toString();
        if (paramPassword.length() == 4) {
            showProgress();
            if (pasoRegisterConfiguration == PASO_PASS_TECNICO_LOCAL)
                presenter.verifyPassLocal(paramPassword);
            else {
                presenter.verifyPassServer( paramPassword);
            }
        } else {
            //Vacia la caja de contraseña
            edtConfigRegisterPass.setText("");
            //Muestra el mensaje de error de formato de la contraseña
            showMessage(getString(R.string.configuration_error_format_clave));
        }
    }

    /**
     * Metodo que registra la configuracion del host
     */
    @OnClick(R.id.btnHostAceptar)
    public void verifyTerminal() {
        String host = edtConfigRegisterHostServer.getText().toString();
        StringBuilder idTerminal = new StringBuilder(edtConfigRegisterHostId.getText().toString());
        if (host.length() > 6 && host.length() < 16) {
            configuracion.setHost(host);
            if (idTerminal.length() >= 1 && idTerminal.length() <= 15) {
                while (idTerminal.length() < 15) idTerminal.insert(0, "0");
                configuracion.setCodigo(idTerminal.toString());
                showProgress();
                presenter.verifyTerminal( configuracion);
            } else {
                showMessage(getString(R.string.configuration_error_format_codigo_establecimiento));
            }
        } else {
            showMessage(getString(R.string.configuration_error_format_host));
        }
    }

    @OnClick(R.id.btnTerminalCancelar)
    public void rechazarInformacionTerminal() {
        pasoRegisterConfiguration = PASO_HOST;
        avanceView();
    }

    @OnClick(R.id.btnTerminalAceptar)
    public void aceptarInformacionTerminal() {
        showProgress();
        presenter.registerData( configuracion, terminal);
    }

    @OnClick(R.id.btnExitoSalir)
    public void navigateBack() {
        Intent intent = new Intent();
        switch (valueActivityBack) {
            case InfoGlobalValues.configuracionRegistrarConfigInicial:
                intent = new Intent(this, SplashActivity.class);
                break;
            case InfoGlobalValues.configuracionRegistrar:
                intent = new Intent(this, ConfigurationActivity.class);
                break;
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick({R.id.btnPassConfigCancelar,
            R.id.btnHostCancelar
    })
    public void navigateToConfigurationScreen() {
        Intent intent = new Intent(this, ConfigurationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    /**
     * Metodo para manejar el valor de acceso valido
     */
    @Override
    public void onVerifyPassLocalValida() {
        pasoRegisterConfiguration = PASO_HOST;
        avanceView();
        hideProgress();
    }

    /**
     * Metodo para manejar el valor de acceso NO valido
     */
    @Override
    public void onVerifyPassLocalNoValida() {
        emptyEditText();
        hideProgress();
        showMessage(getString(R.string.configuration_text_clave_no_valido));
    }

    /**
     * Metodo para manejar el valor de acceso valido
     */
    @Override
    public void onVerifyPassServerValida() {
        pasoRegisterConfiguration = PASO_HOST;
        avanceView();
        hideProgress();
    }

    /**
     * Metodo para manejar el valor de acceso NO valido
     */
    @Override
    public void onVerifyPassServerNoValida() {
        emptyEditText();
        hideProgress();
        showMessage(getString(R.string.configuration_text_clave_no_valido));
    }

    /**
     * @param terminal
     */
    @Override
    public void onVerifyTerminalSuccess(Terminal terminal) {
        this.terminal = terminal;
        txvConfigRegisterTerminalEstablecimiento.setText(terminal.getRazon_social());
        txvConfigRegisterTerminalSucursal.setText(terminal.getNombre_sucursal());
        int codigo = Integer.parseInt(terminal.getCodigo_terminal());
        txvConfigRegisterTerminalCodigo.setText(String.valueOf(codigo));
        pasoRegisterConfiguration = PASO_DISPOSITIVO;
        avanceView();
        hideProgress();
    }

    /**
     * @param messageError
     */
    @Override
    public void onVerifyTerminalError(String messageError) {
        emptyEditText();
        hideProgress();
        showMessageError(messageError);
    }

    @Override
    public void onRegisterDataServerSuccess() {
        txvGeneralExitoMessage.setText(R.string.text_configuration_register_complete);
        pasoRegisterConfiguration = PASO_EXITO;
        avanceView();
        hideProgress();
    }

    @Override
    public void onRegisterDataServerError(String messageError) {
        emptyEditText();
        hideProgress();
        showMessageError(messageError);
    }

    /**
     * @param messageError
     */
    @Override
    public void onRegisterDataDBError(String messageError) {
        emptyEditText();
        hideProgress();
        showMessageError(messageError);
    }

    @Override
    public void onGetDataHeaderSuccess() {
        showMessage(getString(R.string.header_error_disponible));
    }

    @Override
    public void onGetDataHeaderError() {
        showMessageError(getString(R.string.header_error_no_disponible));
    }

    @Override
    public void onErrorConexion() {
        emptyEditText();
        hideProgress();
        showMessageError(getString(R.string.error_general_conexion));
    }

    //#############################################################################################
    // Metodo de manejos de vistas
    // #############################################################################################
    private void avanceView() {
        bodyConfigRegisterPass.setVisibility(View.GONE);
        bodyConfigRegisterHost.setVisibility(View.GONE);
        bodyConfigRegisterTerminal.setVisibility(View.GONE);
        bodyConfigRegisterExito.setVisibility(View.GONE);
        switch (pasoRegisterConfiguration) {
            case PASO_PASS_TECNICO_LOCAL:
            case PASO_PASS_TECNICO_SERVER:
                bodyConfigRegisterPass.setVisibility(View.VISIBLE);
                break;
            case PASO_HOST:
                bodyConfigRegisterHost.setVisibility(View.VISIBLE);
                break;
            case PASO_DISPOSITIVO:
                bodyConfigRegisterTerminal.setVisibility(View.VISIBLE);
                break;
            case PASO_EXITO:
                bodyConfigRegisterExito.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setupUI() {
        Bundle bundleReceived = getIntent().getExtras();
        switch (bundleReceived.getInt(InfoGlobalValues.KEY_PASO_PASS)) {
            case InfoGlobalValues.configuracionPassLocal:
                pasoRegisterConfiguration = PASO_PASS_TECNICO_LOCAL;
                break;
            case InfoGlobalValues.configuracionPassServer:
                pasoRegisterConfiguration = PASO_PASS_TECNICO_SERVER;
                break;
        }
        valueActivityBack = bundleReceived.getInt(InfoGlobalValues.KEY_CONFIGURATION);
        if (valueActivityBack == InfoGlobalValues.configuracionRegistrarConfigInicial) {
            btnConfigRegisterPassCancelar.setVisibility(View.GONE);
            btnConfigRegisterHostCancelar.setVisibility(View.GONE);
        }
        edtConfigRegisterHostServer.setFilters(new InputFilter[]{new FilterHost()});
        coordinatorBase = clyConfigurationRegister;
        emptyEditText();
        avanceView();
    }

    private void emptyEditText() {
        edtConfigRegisterPass.setText("");
        edtConfigRegisterHostServer.setText("");
        edtConfigRegisterHostId.setText("");
    }
}
