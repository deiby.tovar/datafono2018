package com.cofrem.transacciones.modules.transaction.anulacion.event;

import com.cofrem.transacciones.models.modelsWS.modelTransaccion.InformacionTransaccion;

public class AnulacionEvent {

    public enum Type {
        onValidatePassAdminCorrecta,
        onvalidatePassAdminError,
        onGetValorTransaccionSuccess,
        onGetValorTransaccionError,
        onRegisterAnulacionSuccess,
        onRegisterAnulacionError,
        onImprimirReciboSuccess,
        onImprimirReciboError,
        onErrorConexion,
    }

    public static final int VALOR_TRANSACCION_NO_VALIDO = -1;

    private Type type;

    private String errorMessage;
    // Variable que maneja un valor entero a enviar
    private int valorInt;
    private InformacionTransaccion informacionTransaccion;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getValorInt() {
        return valorInt;
    }

    public void setValorInt(int valorInt) {
        this.valorInt = valorInt;
    }

    public InformacionTransaccion getInformacionTransaccion() {
        return informacionTransaccion;
    }

    public void setInformacionTransaccion(InformacionTransaccion informacionTransaccion) {
        this.informacionTransaccion = informacionTransaccion;
    }
}
