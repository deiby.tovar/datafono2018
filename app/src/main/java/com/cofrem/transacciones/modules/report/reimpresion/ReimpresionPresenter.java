package com.cofrem.transacciones.modules.report.reimpresion;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.report.reimpresion.event.ReimpresionEvent;
import org.greenrobot.eventbus.Subscribe;

public class ReimpresionPresenter implements ReimpresionContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private ReimpresionContract.view view;

    private ReimpresionContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public ReimpresionPresenter(ReimpresionContract.view view) {
        this.view = view;
        this.repository = new ReimpresionRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    @Override
    public void validarClaveAdministrador(String clave) {
        repository.validarClaveAdministrador(clave);
    }

    @Override
    public void validarExistenciaUltimoRecibo() {
        repository.validarExistenciaUltimoRecibo();
    }

    @Override
    public void imprimirUltimoRecibo() {
        repository.imprimirUltimoRecibo();
    }

    @Override
    public void validarExistenciaReciboConNumCargo(String numCargo) {
        repository.validarExistenciaReciboConNumCargo(numCargo);
    }

    @Override
    public void imprimirReciboConNumCargo() {
        repository.imprimirReciboConNumCargo();
    }

    @Override
    public void validarExistenciaDetalleRecibos() {
        repository.validarExistenciaDetalleRecibos();
    }

    @Override
    public void imprimirReporteDetalle() {
        repository.imprimirReporteDetalle();
    }

    @Override
    public void imprimirReporteGeneral() {
        repository.imprimirReporteGeneral();
    }

    @Override
    public void cierreDeLote() {
        repository.cierreDeLote();
    }

    @Override
    public void imprimirCierreLote() {
        repository.imprimirCierreLote();
    }

    @Override
    public void VerifySuccess() {
        if (view != null) {
            // repository.validarPasswordTecnico();
        }
    }

    @Override
    @Subscribe
    public void onEventMainThread(ReimpresionEvent reimpresionEvent) {
        switch (reimpresionEvent.getType()) {
            case onVerifyExistenceReciboPorNumCargoSuccess:
                onVerifyExistenceReciboPorNumCargoSuccess(reimpresionEvent.getModelTransaccionOLD());
                break;
            case onVerifyExistenceReciboPorNumCargoError:
                onVerifyExistenceReciboPorNumCargoError();
                break;
            case onVerifyExistenceUltimoReciboSuccess:
                onVerifyExistenceUltimoReciboSuccess(reimpresionEvent.getModelTransaccionOLD());
                break;
            case onVerifyExistenceUltimoReciboError:
                onVerifyExistenceUltimoReciboError();
                break;
            case onVerifyClaveAdministradorSuccess:
                onVerifyClaveAdministradorSuccess();
                break;
            case onVerifyClaveAdministradorError:
                onVerifyClaveAdministradorError();
                break;
            case onVerifyExistenceReporteDetalleSuccess:
                onVerifyExistenceReporteDetalleSuccess();
                break;
            case onVerifyExistenceReporteDetalleError:
                onVerifyExistenceReporteDetalleError();
                break;
            case onImprimirUltimoReciboSuccess:
                onImprimirUltimoReciboSuccess();
                break;
            case onImprimirUltimoReciboError:
                onImprimirUltimoReciboError(reimpresionEvent.getErrorMessage());
                break;
            case onImprimirReciboPorNumCargoSuccess:
                onImprimirReciboPorNumCargoSuccess();
                break;
            case onImprimirReciboPorNumCargoError:
                onImprimirReciboPorNumCargoError(reimpresionEvent.getErrorMessage());
                break;
            case onImprimirReporteDetalleSuccess:
                onImprimirReporteDetalleSuccess();
                break;
            case onImprimirReporteDetalleError:
                onImprimirReporteDetalleError(reimpresionEvent.getErrorMessage());
                break;
            case onImprimirReporteGeneralSuccess:
                onImprimirImprimirReporteGeneralSuccess();
                break;
            case onImprimirReporteGeneralError:
                onImprimirImprimirReporteGeneralError(reimpresionEvent.getErrorMessage());
                break;
            case onCierreLoteSuccess:
                onCierreDeLoteSuccess();
                break;
            case onCierreLoteError:
                onCierreDeLoteError(reimpresionEvent.getErrorMessage());
                break;
            case onTransaccionError:
                onTransaccionError(reimpresionEvent.getErrorMessage());
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void onVerifySuccess() {
        if (view != null) {
            //  view.onConsultarSaldoSuccess();
        }
    }

    public void onVerifyExistenceReciboPorNumCargoSuccess(Transaccion modeltgransaccion) {
        if (view != null) {
            view.onVerifyExistenceReciboPorNumCargoSuccess(modeltgransaccion);
        }
    }

    public void onVerifyExistenceReciboPorNumCargoError() {
        if (view != null) {
            view.onVerifyExistenceReciboPorNumCargoError();
        }
    }

    public void onVerifyExistenceUltimoReciboSuccess(Transaccion modeltgransaccion) {
        if (view != null) {
            if (view != null) {
                view.onVerifyExistenceUltimoReciboSuccess(modeltgransaccion);
            }
        }
    }

    public void onVerifyExistenceUltimoReciboError() {
        if (view != null) {
            view.onVerifyExistenceUltimoReciboError();
        }
    }

    public void onVerifyClaveAdministradorSuccess() {
        if (view != null) {
            view.onVerifyClaveAdministradorSuccess();
        }
    }

    public void onVerifyClaveAdministradorError() {
        if (view != null) {
            view.onVerifyClaveAdministradorError();
        }
    }

    private void onVerifyExistenceReporteDetalleSuccess() {
        if (view != null) {
            view.onVerifyExistenceReporteDetalleSuccess();
        }
    }

    private void onVerifyExistenceReporteDetalleError() {
        if (view != null) {
            view.onVerifyExistenceReporteDetalleError();
        }
    }

    private void onImprimirUltimoReciboSuccess() {
        if (view != null) {
            view.onImprimirUltimoReciboSuccess();
        }
    }

    private void onImprimirUltimoReciboError(String error) {
        if (view != null) {
            view.onImprimirUltimoReciboError(error);
        }
    }

    private void onImprimirReciboPorNumCargoSuccess() {
        if (view != null) {
            view.onImprimirReciboPorNumCargoSuccess();
        }
    }

    private void onImprimirReciboPorNumCargoError(String error) {
        if (view != null) {
            view.onImprimirReciboPorNumCargoError(error);
        }
    }

    private void onImprimirReporteDetalleSuccess() {
        if (view != null) {
            view.onImprimirReporteDetalleSuccess();
        }
    }

    private void onImprimirReporteDetalleError(String error) {
        if (view != null) {
            view.onImprimirReporteDetalleError(error);
        }
    }

    private void onImprimirImprimirReporteGeneralSuccess() {
        if (view != null) {
            view.onImprimirReporteGeneralSuccess();
        }
    }

    private void onImprimirImprimirReporteGeneralError(String error) {
        if (view != null) {
            view.onImprimirReporteGeneralError(error);
        }
    }

    private void onCierreDeLoteSuccess() {
        if (view != null) {
            view.onCierreDeLoteSuccess();
        }
    }

    private void onCierreDeLoteError(String error) {
        if (view != null) {
            view.onCierreDeLoteError(error);
        }
    }

    private void onTransaccionError(String error) {
        if (view != null) {
            view.onTransaccionWSConexionError(error);
        }
    }
}
