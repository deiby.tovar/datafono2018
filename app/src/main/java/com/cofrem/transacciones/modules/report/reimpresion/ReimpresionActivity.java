package com.cofrem.transacciones.modules.report.reimpresion;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.utils.KeyBoard;
import com.cofrem.transacciones.models.Reports;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.report.ReportActivity;

public class ReimpresionActivity
//        extends BaseAppCompatActivity
//        implements ReimpresionContract.view
{
//
//    private static final int PASO_ULTIMO_RECIBO = 1;
//    private static final int PASO_NUMERO_CARGO = 2;
//    private static final int PASO_DETALLE = 3;
//    private static final int PASO_GENERAL = 4;
//
//    /**
//     * #############################################################################################
//     * Declaracion de controles y variables
//     * #############################################################################################
//     */
//
//    @BindView(R.id.clyReimpresion)
//    CoordinatorLayout clyReimpresion;
//
//    //Elementos del modulo
//    @BindView(R.id.bodyReimpresionPassAdmin)
//    View bodyReimpresionPassAdmin;
//
//    @BindView(R.id.bodyReimpresionImprimir)
//    View bodyReimpresionImprimir;
//
//    @BindView(R.id.bodyReimpresionNumCargo)
//    View bodyReimpresionNumCargo;
//
//    @BindView(R.id.bodyReimpresionNumCargoImprimir)
//    View bodyReimpresionNumCargoImprimir;
//
//    @BindView(R.id.bodyReporteDetallesImpresion)
//    View bodyReporteDetallesImpresion;
//
//    @BindView(R.id.bodyReporteGeneralImpresion)
//    View bodyReporteGeneralImpresion;
//
//    @BindView(R.id.bodyCierreLotePassConfig)
//    View bodyCierreLotePassConfig;
//
//    @BindView(R.id.bodyCierreLoteVerificacion)
//    View bodyCierreLoteVerificacion;
//
//    @BindView(R.id.bodyCierreLoteImpresion)
//    View bodyCierreLoteImpresion;
//
//    @BindView(R.id.bodyReimpresionError)
//    View bodyReimpresionError;
//
//    //Paso PASS_ADMIN
//    @BindView(R.id.edtPassAdminValue)
//    EditText edtPassAdminValue;
//
//    //Paso NUM_CARGO
//    @BindView(R.id.edtNumCargoValue)
//    EditText edtNumCargoValue;
//
//    //Paso PASS_CONFIG
//    @BindView(R.id.edtPassConfigValue)
//    EditText edtPassConfigValue;
//
//    //Paso CIERRE_LOTE
//    @BindView(R.id.txvCierreLoteValorVentas)
//    TextView txvCierreLoteValorVentas;
//
//    @BindView(R.id.txvCierreLoteValorAnulaciones)
//    TextView txvCierreLoteValorAnulaciones;
//
//    //Paso ERROR
//    @BindView(R.id.txvErrorMessage)
//    TextView txvErrorMessage;
//
//    Transaccion transaccion;
//    String passwordAdmin = "";
//    String numeroCargo = "";
//    /**
//     * Pasos definidos
//     */
//    private int pasoReporte;
//    /**
//     * #############################################################################################
//     * Instanciamientos de las clases
//     * #############################################################################################
//     */
//
//    private ReimpresionContract.presenter presenter;
//
//    /**
//     * #############################################################################################
//     * Constructor  de  la clase
//     * #############################################################################################
//     */
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_report_module_reimpresion);
//        //Instanciamiento e inicializacion del presentador
//        presenter = new ReimpresionPresenter(this);
//        //Llamada al metodo onCreate del presentador para el registro del bus de datos
//        presenter.onCreate();
//        //Metodo que oculta por defecto los include de la vista
//        inicializarOcultamientoVistas();
//        //metodo verifyDatos acceso
//        presenter.VerifySuccess();
//        Bundle args = getIntent().getExtras();
//        switch ((Reports.TYPE_REPORT) args.getSerializable(Reports.keyReport)) {
//            case ULTIMO_RECIBO:
//                bodyReimpresionRecibo.setVisibility(View.VISIBLE);
//                break;
//            case NUM_CARGO:
//                bodyReimpresionRecibo.setVisibility(View.VISIBLE);
//                break;
//            case DETALLE:
//                pasoReporte = PASO_DETALLE;
//                presenter.validarExistenciaDetalleRecibos();
//                break;
//            case GENERAl:
//                pasoReporte = PASO_GENERAL;
//                bodyReimpresionPassAdmin.setVisibility(View.VISIBLE);
//                break;
//            case CIERRE_LOTE:
//                bodyCierreLotePassConfig.setVisibility(View.VISIBLE);
//                break;
//        }
//    }
//    /**
//     * #############################################################################################
//     * Metodos sobrecargados del sistema
//     * #############################################################################################
//     */
//    /**
//     * Metodo sobrecargado de la vista para la destruccion del activity
//     */
//    @Override
//    public void onDestroy() {
//        presenter.onDestroy();
//        super.onDestroy();
//    }
//    /**
//     * #############################################################################################
//     * Metodos sobrecargados de la interface
//     * #############################################################################################
//     */
//    /**
//     * Metodo para manejar la existencia de un Ultimo recibo para reimprimir
//     */
//    @Override
//    public void onVerifyExistenceUltimoReciboSuccess(Transaccion modelTransaccionOLD) {
//        this.transaccion = modelTransaccionOLD;
//        bodyReimpresionRecibo.setVisibility(View.GONE);
//        bodyReimpresionPassAdmin.setVisibility(View.VISIBLE);
//    }
//
//    /**
//     * Metodo para manejar la NO existencia de un Ultimo recibo para reimprimir
//     */
//    @Override
//    public void onVerifyExistenceUltimoReciboError() {
//// texto quemado hay que pitarlo
//        hideProgress();
//        Toast.makeText(this, "no existen registros", Toast.LENGTH_LONG).show();
//    }
//
//    /**
//     * Metodo para manejar la existencia de un recibo por numero de cargo
//     */
//    @Override
//    public void onVerifyExistenceReciboPorNumCargoSuccess(Transaccion modelTransaccionOLD) {
//        hideProgress();
//        this.transaccion = modelTransaccionOLD;
//        bodyReimpresionNumCargo.setVisibility(View.GONE);
//        bodyReimpresionPassAdmin.setVisibility(View.VISIBLE);
//    }
//
//    /**
//     * Metodo para manejar la NO existencia de un recibo por numero de cargo
//     */
//    @Override
//    public void onVerifyExistenceReciboPorNumCargoError() {
//        hideProgress();
//        edtPassAdminValue.setText("");
//        Toast.makeText(this, this.getString(R.string.report_text_message_No_existen_recibo_num_cargo), Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onVerifyClaveAdministradorSuccess() {
//        hideProgress();
//        bodyReimpresionPassAdmin.setVisibility(View.GONE);
//        switch (pasoReporte) {
////            case PASO_ULTIMO_RECIBO:
////                bodyReimpresionImprimir.setVisibility(View.VISIBLE);
////                break;
////            case PASO_NUMERO_CARGO:
////                bodyReimpresionNumCargoImprimir.setVisibility(View.VISIBLE);
////                txvReportReimprimeonReciboImpresionSaldoCantidad.setText(transaccion.getNumero_transaccion());
////                break;
////            case PASO_DETALLE:
////                bodyReporteDetallesImpresion.setVisibility(View.VISIBLE);
////                break;
////            case PASO_GENERAL:
////                bodyReporteGeneralImpresion.setVisibility(View.VISIBLE);
////                break;
//        }
//        edtPassAdminValue.setText("");
//    }
//
//    @Override
//    public void onVerifyClaveAdministradorError() {
//        hideProgress();
//        edtPassAdminValue.setText("");
//        Toast.makeText(this, this.getString(R.string.report_text_message_clave_admin_incorrecta), Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onVerifyExistenceReporteDetalleSuccess() {
//        hideProgress();
//        bodyReimpresionPassAdmin.setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void onVerifyExistenceReporteDetalleError() {
//        hideProgress();
//        Toast.makeText(this, this.getString(R.string.report_text_message_No_existen_recibos), Toast.LENGTH_LONG).show();
//        regresarDesdeReimpimirRecibo();
//    }
//
//    @Override
//    public void onImprimirUltimoReciboSuccess() {
//        showProgress();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                regresarDesdeReimpimirRecibo();
//            }
//        }, 5000);
//    }
//
//    @Override
//    public void onImprimirUltimoReciboError(String error) {
//        hideProgress();
//        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onImprimirReciboPorNumCargoSuccess() {
//        showProgress();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                regresarDesdeReimpimirRecibo();
//            }
//        }, 5000);
//    }
//
//    @Override
//    public void onImprimirReciboPorNumCargoError(String error) {
//        hideProgress();
//        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onImprimirReporteDetalleSuccess() {
//        showProgress();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                regresarDesdeReimpimirRecibo();
//            }
//        }, 5000);
//    }
//
//    @Override
//    public void onImprimirReporteDetalleError(String error) {
//        hideProgress();
//        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onImprimirReporteGeneralSuccess() {
//        showProgress();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                regresarDesdeReimpimirRecibo();
//            }
//        }, 5000);
//    }
//
//    @Override
//    public void onImprimirReporteGeneralError(String error) {
//        hideProgress();
//        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onCierreDeLoteSuccess() {
//        hideProgress();
//        bodyCierreLotePassConfig.setVisibility(View.GONE);
//        bodyCierreLoteImpresion.setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void onCierreDeLoteError(String error) {
//        hideProgress();
//        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onErrorConexion(String error) {
//        hideProgress();
//        inicializarOcultamientoVistas();
//        txvReporteTansaccionErrorDetalleTexto.setText(error);
//        bodyTransaccionErronea.setVisibility(View.VISIBLE);
//        //        Toast.makeText(this, R.string.configuration_text_informacion_dispositivo_error_conexion, Toast.LENGTH_LONG).show();
//    }
//    /**
//     * #############################################################################################
//     * Metodo propios de la clase
//     * #############################################################################################
//     */
//    /**
//     * Metodo que oculta el teclado al presionar el EditText
//     */
//    @OnLongClick({R.id.edtReportReimprimeonReciboNummeroCargoContenidoClave,
//            R.id.edtReportReimpresionReciboClaveAdministradorContenidoClave
//    })
//    @OnTouch({R.id.edtReportReimprimeonReciboNummeroCargoContenidoClave,
//            R.id.edtReportReimpresionReciboClaveAdministradorContenidoClave
//    })
//    public boolean hideKeyBoardLong() {
//        hideKeyBoard();
//        return true;
//    }
//
//    @OnClick({R.id.edtReportReimprimeonReciboNummeroCargoContenidoClave,
//            R.id.edtReportReimpresionReciboClaveAdministradorContenidoClave
//    })
//    public void hideKeyBoard() {
//        //Oculta el teclado
//        KeyBoard.hide(this);
//    }
//
//    /**
//     * Metodo que oculta por defecto los include de la vista
//     */
//    private void inicializarOcultamientoVistas() {
//        bodyReimpresionRecibo.setVisibility(View.GONE);
//        bodyReimpresionImprimir.setVisibility(View.GONE);
//        bodyReimpresionNumCargo.setVisibility(View.GONE);
//        bodyReimpresionNumCargoImprimir.setVisibility(View.GONE);
//        bodyReporteDetallesImpresion.setVisibility(View.GONE);
//        bodyReporteGeneralImpresion.setVisibility(View.GONE);
//        bodyCierreLotePassConfig.setVisibility(View.GONE);
//        bodyCierreLoteVerificacion.setVisibility(View.GONE);
//        bodyCierreLoteImpresion.setVisibility(View.GONE);
//        bodyReimpresionPassAdmin.setVisibility(View.GONE);
//        bodyTransaccionErronea.setVisibility(View.GONE);
//    }
//
//    /**
//     * Metodo para validar la existencia de un ultimo recibo para Reimprimir
//     */
//    @OnClick(R.id.btnReportUltimoRecibo)
//    public void validarExistenciaUltimoRecibo() {
//        pasoReporte = PASO_ULTIMO_RECIBO;
//        presenter.validarExistenciaUltimoRecibo(this);
//    }
//
//    /**
//     * Metodo que se encargara de validar la clave del administrador
//     */
//    @OnClick(R.id.btnReportReimpresionReciboClaveAdministradorBotonAceptar)
//    public void validarClaveAdministrador() {
//        //Se obtiene el texto de la contraseña
//        passwordAdmin = edtPassAdminValue.getText().toString();
//        //Vacia la caja de contraseña
//        edtPassAdminValue.setText("");
//        //La contraseña debe ser de exactamente 4 caracteres
//        if (passwordAdmin.length() == 4) {
//            //Muestra la barra de progreso
//            showProgress();
//            //Valida la contraseña
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    presenter.validarClaveAdministrador(ReimpresionActivity.this, passwordAdmin);
//                }
//            }, 100);
//        } else {
//            //Muestra el mensaje de error de formato de la contraseña
//            Toast.makeText(this, R.string.transaction_error_format_clave_administrador, Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    /**
//     * Metodo que se encargara de Reimprimir el recibo
//     */
//    @OnClick(R.id.btnReportReimpresionReciboImprimirRecibo)
//    public void imprimirUltimoRecibo() {
//        btnReportReimpresionReciboImprimirRecibo.setEnabled(false);
//        presenter.imprimirUltimoRecibo(this);
//    }
//
//    /**
//     * Metodo para salir de la vista de Reimpresion Ultimo
//     */
//    @OnClick(R.id.btnReportReimpresionReciboSalir)
//    public void salirDelContentReimprimirUltimo() {
//        bodyReimpresionImprimir.setVisibility(View.GONE);
//        bodyReimpresionRecibo.setVisibility(View.VISIBLE);
//    }
//
//    /**
//     * Metodo para iniciar el proceso de Reimprimir recibo por numero de cargo
//     */
//    @OnClick(R.id.btnReportNumCargo)
//    public void navigateToContentReimprimirNumCargo() {
//        bodyReimpresionRecibo.setVisibility(View.GONE);
//        bodyReimpresionNumCargo.setVisibility(View.VISIBLE);
//    }
//
//    /**
//     * Metodo para validad si el nuero de cargo existe para imprimir el recibo
//     */
//    @OnClick(R.id.btnReportReimprimeonReciboNummeroCargoBotonAceptar)
//    public void acceptReimprimirNumCargo() {
//        //Se obtiene el texto de la contraseña
//        numeroCargo = edtReportReimprimeonReciboNummeroCargoContenidoClave.getText().toString();
//        //Vacia la caja de contraseña
//        edtReportReimprimeonReciboNummeroCargoContenidoClave.setText("");
//        //La contraseña debe ser de exactamente 4 caracteres
//        if (numeroCargo.length() == 7) {
//            //Muestra la barra de progreso
//            showProgress();
//            pasoReporte = PASO_NUMERO_CARGO;
//            //Valida la contraseña
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    presenter.validarExistenciaReciboConNumCargo(ReimpresionActivity.this, numeroCargo);
//                }
//            }, 100);
//        } else {
//            //Muestra el mensaje de error de formato del numero de cargo
//            Toast.makeText(this, R.string.transaction_error_format_numero_cargo, Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    @OnClick(R.id.btnReportReimprimeonReciboImpresionBotonImprimir)
//    public void imprimirReimprimirNumCargo() {
//        presenter.imprimirReciboConNumCargo(this);
//        edtReportReimprimeonReciboNummeroCargoContenidoClave.setText("");
//    }
//
//    @OnClick(R.id.btnReportReimprimeonReciboImpresionBotonSalir)
//    public void cancelReimprimirNumCargo() {
//        bodyReimpresionNumCargoImprimir.setVisibility(View.GONE);
//        bodyReimpresionRecibo.setVisibility(View.VISIBLE);
//    }
//
//    @OnClick(R.id.btnReportReporteDetallesImpresionBotonImprimir)
//    public void imprimirReporteDetalle() {
//        presenter.imprimirReporteDetalle(this);
//    }
//
//    @OnClick(R.id.btnReportReporteGeneralImpresionBotonImprimir)
//    public void imprimirReporteGeneral() {
//        presenter.imprimirReporteGeneral(this);
//    }
//
//    @OnClick(R.id.btnReportCierreLoteClaveDispositivoBotonAceptar)
//    public void cierreDeLote() {
//        showProgress();
//        presenter.cierreDeLote(this);
//    }
//
//    @OnClick(R.id.btnReportCierreLoteImpresionImprimir)
//    public void imprimirCierreLote() {
//        presenter.imprimirCierreLote(this);
//    }
//
//    @OnClick({R.id.btnTransBack
//            , R.id.btnReportReporteDetallesImpresionBotonSalir
//            , R.id.btnReportReporteGeneralImpresionBotonSalir
//            , R.id.btnReportCierreLoteClaveDispCancelar
//            , R.id.btnReportCierreLoteImpresionSalir
//            , R.id.btnReportReimprimeonReciboNummeroCargoBotonCancelar
//            , R.id.btnReportReimpresionReciboClaveAdministradorBotonCancelar
//            , R.id.btnReporteTansaccionErrorSalir
//    })
//    public void regresarDesdeReimpimirRecibo() {
//        Intent intent = new Intent(this, ReportActivity.class);
//        startActivity(intent);
//    }
//
//    public void regresarDesdeReimpimirRecibo(int timer) {
//        final Handler Handler = new Handler();
//        Handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                regresarDesdeReimpimirRecibo();
//            }
//        }, timer);
//    }
}
