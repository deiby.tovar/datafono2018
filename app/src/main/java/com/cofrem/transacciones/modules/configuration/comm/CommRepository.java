package com.cofrem.transacciones.modules.configuration.comm;

import android.content.Context;
import com.cofrem.transacciones.core.global.InfoGlobalTransaccionREST;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.volley.VolleyTransaction;
import com.cofrem.transacciones.core.utils.CofremUtils;
import com.cofrem.transacciones.modules.configuration.comm.event.CommEvent;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import java.util.HashMap;

public class CommRepository implements CommContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public CommRepository(Context context) {
        this.context = context;
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    @Override
    public void commTest() {
        final HashMap<String, String> parameters = new HashMap<>();
        parameters.put("codigo", "02");
        VolleyTransaction volleyTransaction = new VolleyTransaction();
        volleyTransaction.getData(
                context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_COMUNICACION,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        postEvent(CommEvent.Type.onCommTestSuccess);
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        postEvent(CommEvent.Type.onCommTestError, CofremUtils.errorMessage(errorMessage));
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(CommEvent.Type.onCommTestError);
                    }
                });
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void postEvent(CommEvent.Type type,
                           String errorMessage) {

        CommEvent commEvent = new CommEvent();

        commEvent.setType(type);

        if (errorMessage != null) {
            commEvent.setErrorMessage(errorMessage);
        }

        EventBus eventBus = GreenRobotEventBus.getInstance();

        eventBus.post(commEvent);
    }

    private void postEvent(CommEvent.Type type) {
        postEvent(type,
                null);
    }
}
