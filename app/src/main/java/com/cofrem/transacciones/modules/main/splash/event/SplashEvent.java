package com.cofrem.transacciones.modules.main.splash.event;

public class SplashEvent {

    public enum Type {
        onVerifyHardwareSuccess,
        onVerifyHardwareError,
        onVerifyRegistrosInicialesError,
        onMagneticReaderDeviceError,
        onNFCDeviceError,
        onPrinterDeviceError,
        onInternetConnectionError,
        onDeviceError,
        onVerifyConfiguracionExiste,
        onVerifyConfiguracionNoExiste,
        onVerifyConfiguracionNoValida, onGetDataHeaderSuccess,
        onGetDataHeaderError,
    }

    private Type type;

    private String errorMessage;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
