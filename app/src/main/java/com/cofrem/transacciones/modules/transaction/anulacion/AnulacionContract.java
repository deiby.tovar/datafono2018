package com.cofrem.transacciones.modules.transaction.anulacion;

import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.anulacion.event.AnulacionEvent;

public interface AnulacionContract {
    interface view {
        void onValidatePassAdminCorrecta();

        void onvalidatePassAdminError(String errorMessage);

        void onGetValorTransaccionSuccess(int valor);

        void onGetValorTransaccionError();

        void onRegisterAnulacionSuccess();

        void onRegisterAnulacionError();

        void onImprimirReciboSuccess();

        void onImprimirReciboError(String errorMessage);

        void onErrorConexion();
    }

    interface presenter {

        void onCreate();

        void onDestroy();

        void validatePassAdmin(String passAdmin);

        void getValorTransaccion(String numCargo);

        void registerAnulacion(Transaccion transaccion);

        void imprimirRecibo(String stringCopia);

        void onEventMainThread(AnulacionEvent anulacionEvent);
    }

    interface repository {
        void validatePassAdmin(String passAdmin);

        void getValorTransaccion(String numCargo);

        void registerAnulacion(Transaccion transaccionOLD);

        void imprimirRecibo(String stringCopia);
    }
}
