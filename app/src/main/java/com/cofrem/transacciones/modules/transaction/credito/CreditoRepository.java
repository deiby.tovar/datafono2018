package com.cofrem.transacciones.modules.transaction.credito;

import android.content.Context;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.global.InfoGlobalSettingsPrint;
import com.cofrem.transacciones.core.global.InfoGlobalTransaccionREST;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.printer.PrinterHandler;
import com.cofrem.transacciones.core.lib.printer.StyleConfig;
import com.cofrem.transacciones.core.lib.volley.VolleyTransaction;
import com.cofrem.transacciones.core.utils.AutoMapper;
import com.cofrem.transacciones.database.AppDatabase;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.modelsWS.ApiWS;
import com.cofrem.transacciones.models.pojo.*;
import com.cofrem.transacciones.models.transactions.TransaccionWS;
import com.cofrem.transacciones.modules.transaction.credito.event.CreditoEvent;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CreditoRepository implements CreditoContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    private Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public CreditoRepository(Context context) {
        this.context = context;
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    /**
     * Metodo encargado de Pasar de minusculas a mayuscula la primera letra de cada palabra
     *
     * @param string
     * @return
     */
    public static String toTextCase(String string) {
        String[] strings = string.split(" ");
        StringBuffer stringBuffer = new StringBuffer();
        for (String anArr : strings)
            stringBuffer.append(Character.toUpperCase(anArr.charAt(0))).append(anArr.substring(1)).append(" ");
        return stringBuffer.toString().trim();
    }

    /**
     * @param transaccion
     */
    @Override
    public void verifyServicios(final Transaccion transaccion) {
        final HashMap<String, String> parameters = new HashMap<>();
        final String codigo = AppDatabase.getInstance(context).getDataDispositivo().getCodigo();
        parameters.put("codigo", codigo);
        parameters.put("numero_tarjeta", transaccion.getNumero_tarjeta());
        parameters.put("identificacion", transaccion.getNumero_documento());
        VolleyTransaction volleyTransaction = new VolleyTransaction();
        volleyTransaction.getData(context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_GET_SERVICIOS
                ,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        TransaccionWS transaccionWS = new TransaccionWS(data);
                        HashMap<String, String> aliases = new HashMap<>();
                        aliases.put(Servicio.class.getSimpleName(), "servicios");
                        AutoMapper autoMapper = new AutoMapper(context);
                        autoMapper.setClassName(GrpServicio.class.getSimpleName());
                        autoMapper.setAlias(aliases);
                        GrpServicio grpServicio = (GrpServicio) autoMapper.mapping(transaccionWS.getDataTransaction());
                        if (Integer.valueOf(grpServicio.getNumero_de_registros()) > 0) {
                            Servicio[] servicio = grpServicio.getServicio();
                            int saldoTotal = 0;
                            for (Servicio infoServicio : servicio) {
                                saldoTotal = saldoTotal + Integer.valueOf(
                                        infoServicio.getSaldo()
                                                .replace(".", "")
                                                .replace(",", "")
                                );
                            }
                            if (saldoTotal >= transaccion.getValor())
                                postEvent(CreditoEvent.Type.onVerifyServiciosSuccess, grpServicio.getServicio());
                            else {
                                postEvent(CreditoEvent.Type.onErrorSaldoInsuficiente);
                            }
                        } else {
                            postEvent(CreditoEvent.Type.onVerifyServiciosErrorNoServicios);
                        }
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        TransaccionWS transaccionWS = new TransaccionWS((JsonObject) new JsonParser().parse(errorMessage.toString()));
                        int codigo = transaccionWS.getDataTransaction().get("codigo").getAsInt();
                        switch (codigo) {
                            case ApiWS.CODIGO_DOCUMENTO_INCORRECTO:
                                postEvent(CreditoEvent.Type.onVerifyServiciosErrorDocumentoIncorrecto);
                                break;
                            case ApiWS.CODIGO_CAMBIO_CLAVE:
                                postEvent(CreditoEvent.Type.onVerifyServiciosErrorCambioClave);
                                break;
                            case ApiWS.CODIGO_TARJETA_INACTIVA:
                                postEvent(CreditoEvent.Type.onErrorTarjetaInactiva);
                                break;
                            case ApiWS.CODIGO_TARJETA_INVALIDA:
                                postEvent(CreditoEvent.Type.onErrorTarjetaInvalida);
                                break;
                        }
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(CreditoEvent.Type.onErrorConexion);
                    }
                });
    }

    /**
     * @param transaccion
     */
    @Override
    public void registrarConsumo(Transaccion transaccion) {
        final HashMap<String, String> parameters = new HashMap<>();
        final String codigo = AppDatabase.getInstance(context).getDataDispositivo().getCodigo();
        parameters.put("codigo", codigo);
        parameters.put("numero_tarjeta", transaccion.getNumero_tarjeta());
        parameters.put("valor", String.valueOf(transaccion.getValor()));
        parameters.put("servicios", transaccion.getServicios());
        parameters.put("password", String.valueOf(transaccion.getClave()));
        VolleyTransaction volleyTransaction = new VolleyTransaction();
        volleyTransaction.getData(context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_CONSUMO
                ,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        TransaccionWS transaccionWS = new TransaccionWS(data);
                        AutoMapper autoMapper = new AutoMapper(context);
                        autoMapper.setClassName(Consumo.class.getSimpleName());
                        Consumo consumo = (Consumo) autoMapper.mapping(transaccionWS.getDataTransaction());
                        if (AppDatabase.getInstance(context).insertRegistroTransaction(consumo, Transaccion.CODIGO_TIPO_TRANSACCION_CONSUMO)) {
                            postEvent(CreditoEvent.Type.onRegistrarConsumoSuccess);
                            imprimirRecibo(context.getResources().getString(R.string.recibo_copia_comercio));
                        } else {
                            postEvent(CreditoEvent.Type.onRegistrarConsumoDBError);
                        }
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        TransaccionWS transaccionWS = new TransaccionWS((JsonObject) new JsonParser().parse(errorMessage.toString()));
                        int codigo = transaccionWS.getDataTransaction().get("codigo").getAsInt();
                        switch (codigo) {
                            case ApiWS.CODIGO_TERMINAL_INACTIVA:
                                postEvent(CreditoEvent.Type.onRegistrarConsumoTerminalInactiva);
                                break;
                            case ApiWS.CODIGO_TRANSACCION_INSUFICIENTE:
                                postEvent(CreditoEvent.Type.onErrorSaldoInsuficiente);
                                break;
                            case ApiWS.CODIGO_TARJETA_INACTIVA:
                                postEvent(CreditoEvent.Type.onErrorTarjetaInactiva);
                                break;
                            case ApiWS.CODIGO_TARJETA_INVALIDA:
                                postEvent(CreditoEvent.Type.onErrorTarjetaInvalida);
                                break;
                            default:
                                postEvent(CreditoEvent.Type.onRegistrarConsumoError);
                        }
                       }

                    @Override
                    public void onErrorConnection() {
                        postEvent(CreditoEvent.Type.onErrorConexion);
                    }
                });
    }

    /**
     * Metodo que imprime el recibo de la transaccion
     */
    @Override
    public void imprimirRecibo(String stringCopia) {
        ConfigurationPrinter configurationPrinter = AppDatabase.getInstance(context).getConfigurationPrinter();
        int gray = configurationPrinter.getGray_level();
        ArrayList<Transaccion> transaccionArrayList = AppDatabase.getInstance(context).obtenerUltimaTransaccion();
        Transaccion transaccion = transaccionArrayList.get(0);
        // creamos el ArrayList se que encarga de almacenar los rows del recibo
        ArrayList<PrintRow> printRows = new ArrayList<>();
        //Se agrega el logo al primer renglon del recibo y se coloca en el centro
        printRows.add(PrintRow.printLogo(context, gray));
        PrintRow.printCofrem(context, printRows, gray, 10);
        //se siguen agregando cado auno de los String a los renglones (Rows) del recibo para imprimir
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_separador_operador),
                new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1)
        ));
        PrintRow.printOperador(context, printRows, gray, 10);
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_numero_transaccion),
                transaccion.getNumero_transaccion(), new StyleConfig(StyleConfig.Align.LEFT, gray)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_fecha),
                transaccion.getRegistro(), new StyleConfig(StyleConfig.Align.LEFT, gray, 20)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(
                        R.string.recibo_separador_afiliado),
                new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_numero_documento),
                transaccion.getNumero_documento(),
                new StyleConfig(StyleConfig.Align.LEFT, gray)
        ));
        printRows.add(new PrintRow(
                transaccion.getNombre_usuario(),
                new StyleConfig(StyleConfig.Align.LEFT, gray)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_numero_tarjeta),
                PrinterHandler.getFormatNumTarjeta(transaccion.getNumero_tarjeta()), new StyleConfig(StyleConfig.Align.LEFT, gray, 20)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_separador_detalle),
                new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_valor),
                PrintRow.numberFormat(transaccion.getValor()), new StyleConfig(StyleConfig.Align.LEFT, gray)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_separador_linea),
                new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1, 10)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_total),
                PrintRow.numberFormat(transaccion.getValor()), new StyleConfig(StyleConfig.Align.LEFT, gray)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_separador_linea),
                new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1, 30)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_mensaje_final),
                new StyleConfig(StyleConfig.Align.CENTER, gray, StyleConfig.FontSize.F3, 30)
        ));
        PrintRow.printFirma(context, printRows, gray);
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_entidad),
                new StyleConfig(StyleConfig.Align.CENTER, gray, StyleConfig.FontSize.F3)
        ));
        printRows.add(new PrintRow(
                context.getResources().getString(R.string.recibo_vigilado),
                new StyleConfig(StyleConfig.Align.CENTER, gray, StyleConfig.FontSize.F3, 20)
        ));
        printRows.add(new PrintRow(
                stringCopia,
                new StyleConfig(StyleConfig.Align.CENTER, gray, StyleConfig.FontSize.F3, 60)
        ));
        printRows.add(new PrintRow(
                ".",
                new StyleConfig(StyleConfig.Align.LEFT, 1)
        ));
        //retornamos el estado de la impresora tras enviar los rows para imprimir
        int status = new PrinterHandler().imprimerTexto(printRows);
        if (status == InfoGlobalSettingsPrint.PRINTER_OK) postEvent(CreditoEvent.Type.onImprimirReciboSuccess);
        else {
            postEvent(CreditoEvent.Type.onImprimirReciboError, PrinterHandler.stringErrorPrinter(status, context));
        }
    }
    /**
     * #############################################################################################
     *Metodo propios de la clase
     * #############################################################################################
     */
    private void postEvent(CreditoEvent.Type type,
                           String errorMessage,
                           Servicio[] dataServicio
    ) {
        CreditoEvent creditoEvent = new CreditoEvent();
        creditoEvent.setType(type);
        if (errorMessage != null) {
            creditoEvent.setErrorMessage(errorMessage);
        }
        if (dataServicio != null) {
            creditoEvent.setServicio(dataServicio);
        }
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(creditoEvent);
    }
    private void postEvent(CreditoEvent.Type type) {
        postEvent(type,
                null,
                null
        );
    }

    /**
     * @param type
     * @param error
     */
    private void postEvent(CreditoEvent.Type type,
                           String error) {
        postEvent(type,
                error,
                null
        );
    }

    /**
     * @param type
     * @param dataServicio
     */
    private void postEvent(CreditoEvent.Type type,
                           Servicio[] dataServicio) {
        postEvent(type,
                null,
                dataServicio
        );
    }
}