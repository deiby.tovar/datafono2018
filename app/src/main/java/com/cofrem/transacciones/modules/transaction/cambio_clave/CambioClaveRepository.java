package com.cofrem.transacciones.modules.transaction.cambio_clave;

import android.content.Context;
import com.cofrem.transacciones.core.global.InfoGlobalTransaccionREST;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.volley.VolleyTransaction;
import com.cofrem.transacciones.models.Tarjeta;
import com.cofrem.transacciones.models.modelsWS.ApiWS;
import com.cofrem.transacciones.models.transactions.TransaccionWS;
import com.cofrem.transacciones.modules.transaction.cambio_clave.event.CambioClaveEvent;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONObject;

import java.util.HashMap;

public class CambioClaveRepository implements CambioClaveContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public CambioClaveRepository(Context context) {
        this.context = context;
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    /**
     * @param tarjeta
     */
    @Override
    public void verifyPassActual(Tarjeta tarjeta) {
        final HashMap<String, String> parameters = new HashMap<>();
        parameters.put("numero_tarjeta", tarjeta.getNumTarjeta());
        parameters.put("identificacion", tarjeta.getNumDocumento());
        parameters.put("password", tarjeta.getPassActual());
        VolleyTransaction volleyTransaction = new VolleyTransaction();
        volleyTransaction.getData(context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_CONSULTAR_CLAVE_TARJETA
                ,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        postEvent(CambioClaveEvent.Type.onVerifyPassActualSuccess);
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        TransaccionWS transaccionWS = new TransaccionWS((JsonObject) new JsonParser().parse(errorMessage.toString()));
                        int codigo = transaccionWS.getDataTransaction().get("codigo").getAsInt();
                        switch (codigo) {
                            case ApiWS.CODIGO_PASSWORD_INCORECTA:
                                postEvent(CambioClaveEvent.Type.onVerifyPassActualErrorPassIncorrecta);
                                break;
                            case ApiWS.CODIGO_DOCUMENTO_INCORRECTO:
                                postEvent(CambioClaveEvent.Type.onVerifyPassActualErrorDocumentoIncorrecto);
                                break;
                            case ApiWS.CODIGO_TARJETA_INACTIVA:
                                postEvent(CambioClaveEvent.Type.onVerifyPassActualErrorTarjetaInactiva);
                                break;
                            case ApiWS.CODIGO_TARJETA_INVALIDA:
                                postEvent(CambioClaveEvent.Type.onVerifyPassActualErrorTarjetaInvalida);
                                break;
                        }
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(CambioClaveEvent.Type.onErrorConexion);
                    }
                });
    }

    /**
     * @param tarjeta
     */
    @Override
    public void verifyPassNueva(Tarjeta tarjeta) {
        final HashMap<String, String> parameters = new HashMap<>();
        parameters.put("numero_tarjeta", tarjeta.getNumTarjeta());
        parameters.put("identificacion", tarjeta.getNumDocumento());
        parameters.put("password", tarjeta.getPassActual());
        parameters.put("nuevo_password", tarjeta.getPassNueva());
        VolleyTransaction volleyTransaction = new VolleyTransaction();
        volleyTransaction.getData(context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_ACTUALIZAR_CLAVE_TARJETA
                ,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        postEvent(CambioClaveEvent.Type.onVerifyPassNuevaSuccess);
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        TransaccionWS transaccionWS = new TransaccionWS((JsonObject) new JsonParser().parse(errorMessage.toString()));
                        int codigo = transaccionWS.getDataTransaction().get("codigo").getAsInt();
                        switch (codigo) {
                            case ApiWS.CODIGO_PASSWORD_INCORECTA:
                                postEvent(CambioClaveEvent.Type.onVerifyPassNuevaErrorPassIncorrecta);
                                break;
                            case ApiWS.CODIGO_DOCUMENTO_INCORRECTO:
                                postEvent(CambioClaveEvent.Type.onVerifyPassNuevaErrorDocumentoIncorrecto);
                                break;
                            case ApiWS.CODIGO_PASSWORD_FORMATO_INCORRECTO:
                                postEvent(CambioClaveEvent.Type.onVerifyPassNuevaErrorPassFormatoIncorrecto);
                                break;
                            case ApiWS.CODIGO_TARJETA_INACTIVA:
                                postEvent(CambioClaveEvent.Type.onVerifyPassNuevaErrorTarjetaInactiva);
                                break;
                            case ApiWS.CODIGO_TARJETA_INVALIDA:
                                postEvent(CambioClaveEvent.Type.onVerifyPassNuevaErrorTarjetaInvalida);
                                break;
                        }
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(CambioClaveEvent.Type.onErrorConexion);
                    }
                });
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void postEvent(CambioClaveEvent.Type type, String errorMessage) {
        CambioClaveEvent cambioClaveEvent = new CambioClaveEvent();
        cambioClaveEvent.setType(type);
        if (errorMessage != null) {
            cambioClaveEvent.setErrorMessage(errorMessage);
        }
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(cambioClaveEvent);
    }
    private void postEvent(CambioClaveEvent.Type type) {
        postEvent(type, null);
    }
}
