package com.cofrem.transacciones.modules.main.splash;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.global.InfoGlobalValues;
import com.cofrem.transacciones.modules.configuration.register.RegisterActivity;

public class SplashActivity
        extends BaseAppCompatActivity
        implements SplashContract.view {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     *///Contoles
    @BindView(R.id.txvSplashInfo)
    TextView txvSplashInfo;
    @BindView(R.id.pgbSplashLoading)
    ProgressBar pgbSplashLoading;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private SplashContract.presenter presenter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Previene inicializacion del header
        setupInfoHeader = false;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        //Instanciamiento e inicializacion del presentador
        presenter = new SplashPresenter(this);

        //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();

        //Metodo para validar la configuracion inicial
        presenter.verifyHardware();
    }
    /*
    #############################################################################################
    Metodos sobrecargados del sistema
    #############################################################################################
    */

    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
    /*
    #############################################################################################
    Metodo sobrecargados del activity base
    #############################################################################################
    */

    /**
     * Metodo para mostrar la barra de progreso
     */
    @Override
    protected void showProgress() {
        pgbSplashLoading.setVisibility(View.VISIBLE);
    }

    /**
     * Metodo para ocultar la barra de progreso
     */
    @Override
    protected void hideProgress() {
        pgbSplashLoading.setVisibility(View.GONE);
    }

    /**
     * @param message
     */
    @Override
    protected void showMessage(String message) {
        txvSplashInfo.setText(new StringBuilder(txvSplashInfo.getText()).append("\n").append(message));
    }
    /*
    #############################################################################################
    Metodos sobrecargados de la interface
    #############################################################################################
    */

    @Override
    public void onVerifyHardwareSuccess() {
        showMessage(getString(R.string.general_message_verify_success));
        verifyConfiguracion();
    }

    /**
     * Metodo para manejar la verificacion erronea
     */
    @Override
    public void onVerifyHardwareError() {
        hideProgress();
        showMessage(getString(R.string.general_message_verify_error));
    }

    @Override
    public void onVerifyRegistrosInicialesError() {
        hideProgress();
        showMessage(getString(R.string.general_message_verify_registros_iniciales_error));
    }

    /**
     * Metodo para manejar la conexion al dispositivo lector de banda magnetica erronea
     */
    @Override
    public void onMagneticReaderDeviceError() {
        showMessage(getString(R.string.general_message_magnetic_device_error));
    }

    /**
     * Metodo para manejar la conexion al dispositivo NFC erronea
     */
    @Override
    public void onNFCDeviceError() {
        showMessage(getString(R.string.general_message_nfc_device_error));
    }

    /**
     * Metodo para manejar la conexion al dispositivo de impresion erronea
     */
    @Override
    public void onPrinterDeviceError() {
        showMessage(getString(R.string.general_message_printer_device_error));
    }

    /**
     * Metodo para manejar la conexion a internet erronea
     */
    @Override
    public void onInternetConnectionError() {
        showMessage(getString(R.string.general_message_internet_error));
    }

    /**
     * Metodo para manejar la conexion al dispositivo de impresion erronea
     */
    @Override
    public void onDeviceError() {
        showMessage(getString(R.string.general_message_device_error));
    }

    /**
     * Metodo para manejar la existencia de la configuracion inicial
     */
    @Override
    public void onVerifyConfiguracionExiste() {
        hideProgress();
        showMessage(getString(R.string.general_message_verify_configuration_initial_existe));
        getDataHeader();
    }

    /**
     * Metodo para manejar la NO existencia de la configuracion inicial
     */
    @Override
    public void onVerifyConfiguracionNoExiste() {
        hideProgress();
        showMessage(getString(R.string.general_message_verify_configuration_initial_no_existe));
        navigateToRegister();
    }

    /**
     * Metodo para manejar la existencia de la configuracion inicial NO valida
     */
    @Override
    public void onVerifyConfiguracionNoValida() {
        hideProgress();
        showMessage(getString(R.string.general_message_verify_configuration_initial_no_valida));
        navigateToRegister();
    }

    @Override
    public void onGetDataHeaderSuccess() {
        hideProgress();
        navigateToMain();
    }

    @Override
    public void onGetDataHeaderError() {
        hideProgress();
        navigateToMain();
    }
    /*
    #############################################################################################
    Metodo propios de la clase
    #############################################################################################
    */

    private void verifyConfiguracion() {
        showProgress();
        presenter.verifyConfiguracion();
    }

    private void getDataHeader() {
        showProgress();
        presenter.getDataHeader();
    }

    /**
     * Metodo para navegar a la ventana inicial
     */
    private void navigateToMain() {
        navigateToMain(this);
    }

    /**
     * Metodo para navegar a la ventana de registro de configuracion
     */
    private void navigateToRegister() {
        Bundle bundle = new Bundle();
        bundle.putInt(InfoGlobalValues.KEY_CONFIGURATION, InfoGlobalValues.configuracionRegistrarConfigInicial);
        bundle.putInt(InfoGlobalValues.KEY_PASO_PASS, InfoGlobalValues.configuracionPassLocal);
        navigateTo(this, RegisterActivity.class, bundle);
    }
}