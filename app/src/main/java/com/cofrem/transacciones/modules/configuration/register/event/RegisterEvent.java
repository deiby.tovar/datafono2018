package com.cofrem.transacciones.modules.configuration.register.event;

import com.cofrem.transacciones.models.pojo.Terminal;

public class RegisterEvent {

    public enum Type {
        onVerifyPassLocalValida,
        onVerifyPassLocalNoValida,
        onVerifyPassServerValida,
        onVerifyPassServerNoValida,
        onVerifyTerminalSuccess,
        onVerifyTerminalError,
        onRegisterDataServerSuccess,
        onRegisterDataServerError,
        onRegisterDataDBError,
        onGetDataHeaderSuccess,
        onGetDataHeaderError,
        onErrorConexion,
    }

    private Type type;

    private String errorMessage;

    private Terminal terminal;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }
}
