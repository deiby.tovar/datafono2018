package com.cofrem.transacciones.modules.configuration.register;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.global.InfoGlobalSettings;
import com.cofrem.transacciones.core.global.InfoGlobalTransaccionREST;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.volley.VolleyTransaction;
import com.cofrem.transacciones.core.utils.AutoMapper;
import com.cofrem.transacciones.core.utils.CofremUtils;
import com.cofrem.transacciones.database.AppDatabase;
import com.cofrem.transacciones.models.keys.DeviceKeys;
import com.cofrem.transacciones.models.pojo.Configuracion;
import com.cofrem.transacciones.models.pojo.Terminal;
import com.cofrem.transacciones.models.transactions.TransaccionWS;
import com.cofrem.transacciones.modules.configuration.register.event.RegisterEvent;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

public class RegisterRepository implements RegisterContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public RegisterRepository(Context context) {
        this.context = context;
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    /**
     * Valida el acceso a la configuracion del dispositivo mediante contraseña
     *
     * @param paramPassword
     */
    @Override
    public void verifyPassLocal(String paramPassword) {
        if (paramPassword.equals(InfoGlobalSettings.INITIAL_PASS)) {
            postEvent(RegisterEvent.Type.onVerifyPassLocalValida);
        } else {
            postEvent(RegisterEvent.Type.onVerifyPassLocalNoValida);
        }
    }

    /**
     * Valida el acceso a la configuracion del dispositivo mediante contraseña
     *
     * @param passAdmin
     */
    @Override
    public void verifyPassServer(String passAdmin) {
        final HashMap<String, String> parameters = new HashMap<>();

        parameters.put(DeviceKeys.Get.KEY_DEVICE_CODIGO, AppDatabase.getInstance(context).getDataDispositivo().getCodigo());

        parameters.put(DeviceKeys.Get.KEY_DEVICE_PASSWORD, passAdmin);

        VolleyTransaction volleyTransaction = new VolleyTransaction();

        volleyTransaction.getData(
                context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion()
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI + InfoGlobalTransaccionREST.METHOD_CLAVE_TERMINAL,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        postEvent(RegisterEvent.Type.onVerifyPassServerValida);
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        postEvent(RegisterEvent.Type.onVerifyPassServerNoValida, CofremUtils.errorMessage(errorMessage));
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(RegisterEvent.Type.onErrorConexion);
                    }
                });
    }

    @Override
    public void verifyTerminal(Configuracion configuracion) {

        HashMap<String, String> parameters = new HashMap<>();

        parameters.put(DeviceKeys.Get.KEY_DEVICE_CODIGO, configuracion.getCodigo());

        VolleyTransaction volleyTransaction = new VolleyTransaction();

        volleyTransaction.getData(
                context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
//                        paramConfiguracion.getHost() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_TERMINAL,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        TransaccionWS transaccionWS = new TransaccionWS(data);
                        AutoMapper autoMapper = new AutoMapper(context);
                        autoMapper.setClassName(Terminal.class.getSimpleName());
                        Terminal terminal = (Terminal) autoMapper.mapping(transaccionWS.getDataTransaction());
                        postEvent(RegisterEvent.Type.onVerifyTerminalSuccess, terminal);
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        postEvent(RegisterEvent.Type.onVerifyTerminalError, CofremUtils.errorMessage(errorMessage));
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(RegisterEvent.Type.onErrorConexion);
                    }
                });
    }

    /**
     * registerData
     * Registra los parametros de conexion del dispositivo
     *
     * @param configuracion
     */
    @Override
    public void registerData(Configuracion configuracion, Terminal terminal) {
        if (AppDatabase.getInstance(context).insertConfiguracion(configuracion) && AppDatabase.getInstance(context).insertTerminal(terminal)) {
            registerDataServer(configuracion);
        } else {
            postEvent(RegisterEvent.Type.onRegisterDataDBError, context.getResources().getString(R.string.error_general_registro_base_datos));
        }
    }

    /**
     * @param paramConfiguracion
     */
    private void registerDataServer(Configuracion paramConfiguracion) {

        final HashMap<String, String> parameters = new HashMap<>();

        parameters.put("codigo", paramConfiguracion.getCodigo());

        parameters.put("uuid", UUID.randomUUID().toString());

        parameters.put("imei", getIMEI(context));

        parameters.put("mac", getMAC(context));

        VolleyTransaction volleyTransaction = new VolleyTransaction();

        volleyTransaction.getData(
                context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
//                        paramConfiguracion.getHost() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_ASIGNA_ID,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        getDataHeader(context);
                        postEvent(RegisterEvent.Type.onRegisterDataServerSuccess);
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        postEvent(RegisterEvent.Type.onRegisterDataServerError, CofremUtils.errorMessage(errorMessage));
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(RegisterEvent.Type.onErrorConexion);
                    }
                }
        );
    }

    /**
     *
     */
    public void getDataHeader(Context context) {
        if (AppDatabase.getInstance(context).getDataHeader()) {
            postEvent(RegisterEvent.Type.onGetDataHeaderSuccess);
        } else {
            postEvent(RegisterEvent.Type.onGetDataHeaderError);
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */

    private String getMAC(Context context) {
        String mac = "";
        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiMan != null) {
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            if (wifiInf != null) mac = wifiInf.getMacAddress();
        }
        return mac;
    }


    @SuppressLint("MissingPermission")
    private String getIMEI(Context context) {

        String imei = "";

        TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (mTelephony.getDeviceId() != null) {
            imei = mTelephony.getDeviceId();
        }
        return imei;
    }

    private void postEvent(RegisterEvent.Type type,
                           String errorMessage,
                           Terminal terminal) {

        RegisterEvent registerEvent = new RegisterEvent();

        registerEvent.setType(type);

        if (errorMessage != null) {
            registerEvent.setErrorMessage(errorMessage);
        }

        if (terminal != null) {
            registerEvent.setTerminal(terminal);
        }

        EventBus eventBus = GreenRobotEventBus.getInstance();

        eventBus.post(registerEvent);
    }

    private void postEvent(RegisterEvent.Type type) {
        postEvent(type,
                null,
                null);
    }

    private void postEvent(RegisterEvent.Type type,
                           String errorMessage) {
        postEvent(type,
                errorMessage,
                null);
    }

    private void postEvent(RegisterEvent.Type type,
                           Terminal terminal) {
        postEvent(type,
                null,
                terminal);
    }
}
