package com.cofrem.transacciones.modules.transaction.anulacion;

import android.content.Context;

import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.global.InfoGlobalSettingsPrint;
import com.cofrem.transacciones.core.global.InfoGlobalTransaccionREST;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.printer.PrinterHandler;
import com.cofrem.transacciones.core.lib.printer.StyleConfig;
import com.cofrem.transacciones.core.lib.volley.VolleyTransaction;
import com.cofrem.transacciones.core.utils.AutoMapper;
import com.cofrem.transacciones.database.AppDatabase;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.modelsWS.modelTransaccion.InformacionTransaccion;
import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;
import com.cofrem.transacciones.models.pojo.InfoTransaccion;
import com.cofrem.transacciones.models.pojo.ServicioConsumido;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.models.transactions.TransaccionWS;
import com.cofrem.transacciones.modules.transaction.anulacion.event.AnulacionEvent;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AnulacionRepository implements AnulacionContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public AnulacionRepository(Context context) {
        this.context = context;
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    /**
     * Valida la clave de administracion
     *
     * @param passAdmin
     */
    @Override
    public void validatePassAdmin(String passAdmin) {

        final HashMap<String, String> parameters = new HashMap<>();

        final String codigo = AppDatabase.getInstance(context).getDataDispositivo().getCodigo();

        parameters.put("codigo", codigo);

        parameters.put("password", passAdmin);

        VolleyTransaction volleyTransaction = new VolleyTransaction();

        volleyTransaction.getData(
                context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_CLAVE_SUCURSAL,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {

                        postEvent(AnulacionEvent.Type.onValidatePassAdminCorrecta);

                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        try {
                            postEvent(AnulacionEvent.Type.onvalidatePassAdminError, errorMessage.get("message").toString());
                        } catch (JSONException e) {
                            postEvent(AnulacionEvent.Type.onvalidatePassAdminError, e.getLocalizedMessage());
                        }
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(AnulacionEvent.Type.onErrorConexion);
                    }
                });
    }

    /**
     * @param numCargo
     */
    @Override
    public void getValorTransaccion(String numCargo) {

        final HashMap<String, String> parameters = new HashMap<>();

        final String codigo = AppDatabase.getInstance(context).getDataDispositivo().getCodigo();

        parameters.put("codigo_terminal", codigo);

        parameters.put("numero_transaccion", numCargo);

        VolleyTransaction volleyTransaction = new VolleyTransaction();

        volleyTransaction.getData(
                context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_INFO_TRANSACCION,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        TransaccionWS transaccionWS = new TransaccionWS(data);

                        HashMap<String, String> aliases = new HashMap<>();

                        aliases.put(ServicioConsumido.class.getSimpleName(), "consumos");

                        AutoMapper autoMapper = new AutoMapper(context);

                        autoMapper.setClassName(InfoTransaccion.class.getSimpleName());

                        autoMapper.setAlias(aliases);

                        InfoTransaccion infoTransaccion = (InfoTransaccion) autoMapper.mapping(transaccionWS.getDataTransaction());

                        postEvent(AnulacionEvent.Type.onGetValorTransaccionSuccess, Integer.parseInt(infoTransaccion.getValor()));

                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        try {
                            postEvent(AnulacionEvent.Type.onGetValorTransaccionError, errorMessage.get("message").toString());
                        } catch (JSONException e) {
                            postEvent(AnulacionEvent.Type.onGetValorTransaccionError, e.getLocalizedMessage());
                        }
                    }

                    @Override
                    public void onErrorConnection() {
                        postEvent(AnulacionEvent.Type.onErrorConexion);
                    }
                });
    }

    /**
     * @param transaccionOLD
     */
    @Override
    public void registerAnulacion(Transaccion transaccionOLD) {
        final HashMap<String, String> parameters = new HashMap<>();
        final String codigo = AppDatabase.getInstance(context).getDataDispositivo().getCodigo();
        parameters.put("codigo", codigo);
        parameters.put("numero_tarjeta", transaccionOLD.getNumero_tarjeta());
        parameters.put("numero_transaccion", transaccionOLD.getNumero_transaccion());
        parameters.put("identificacion", transaccionOLD.getNumero_documento());
        parameters.put("password", transaccionOLD.getClave() + "");
        VolleyTransaction volleyTransaction = new VolleyTransaction();
        //Consulta la clave del administrador para compararla con la ingresada en la vista
        volleyTransaction.getData(context,
                parameters,
                InfoGlobalTransaccionREST.HTTP +
                        // AppDatabase.getInstance(context).obtenerURLConfiguracionConexion() +
                        InfoGlobalTransaccionREST.WEB_SERVICE_URI +
                        InfoGlobalTransaccionREST.METHOD_ANULACION
                ,
                new VolleyTransaction.VolleyCallback() {
                    @Override
                    public void onSuccess(JsonObject data) {
                        if (data.get("estado").getAsBoolean()) {
                            InformacionTransaccion informacionTransaccion = new InformacionTransaccion(data);
                            postEvent(AnulacionEvent.Type.onRegisterAnulacionError, informacionTransaccion);
                        } else {
                            postEvent(AnulacionEvent.Type.onErrorConexion, data.get("mensaje").getAsString());
                        }
                    }

                    @Override
                    public void onError(JSONObject errorMessage) {
                        postEvent(AnulacionEvent.Type.onvalidatePassAdminError);
                    }

                    @Override
                    public void onErrorConnection() {
                    }
                });
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */


    /**
     * Metodo que imprime el recibo de la transaccion
     *
     * @param stringCopia
     */
    @Override
    public void imprimirRecibo(String stringCopia) {
        ConfigurationPrinter configurationPrinter = AppDatabase.getInstance(context).getConfigurationPrinter();
        int gray = configurationPrinter.getGray_level();
        Transaccion transaccion = AppDatabase.getInstance(context).obtenerUltimaTransaccionAnulada();
        //        String fecha_transaccion = AppDatabase.getInstance(context).obtenerFechaTransaccionNumCargo(transaccion.getNumero_cargo());
        // creamos el ArrayList se que encarga de almacenar los rows del recibo
        ArrayList<PrintRow> printRows = new ArrayList<PrintRow>();
        //Se agrega el logo al primer renglon del recibo y se coloca en el centro
        printRows.add(PrintRow.printLogo(context, gray));
        PrintRow.printCofrem(context, printRows, gray, 10);
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_title_anulacion), new StyleConfig(StyleConfig.Align.CENTER, gray, 20)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_separador_operador), new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1)));
        PrintRow.printOperador(context, printRows, gray, 10);
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_numero_transaccion), transaccion.getNumero_transaccion(), new StyleConfig(StyleConfig.Align.LEFT, gray)));
//        printRows.add(new PrintRow(context.getResources().getString(
//                R.string.recibo_fecha),fecha_transaccion, new StyleConfig(StyleConfig.Align.LEFT, gray)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_fecha_anulacion), transaccion.getFullFechaServer(), new StyleConfig(StyleConfig.Align.LEFT, gray, 20)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_separador_afiliado), new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_numero_documento), transaccion.getNumero_documento(), new StyleConfig(StyleConfig.Align.LEFT, gray)));
        printRows.add(new PrintRow(transaccion.getNombre_usuario(), new StyleConfig(StyleConfig.Align.LEFT, gray)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_numero_tarjeta), PrinterHandler.getFormatNumTarjeta(transaccion.getNumero_tarjeta()), new StyleConfig(StyleConfig.Align.LEFT, gray, 20)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_separador_detalle), new StyleConfig(StyleConfig.Align.LEFT, gray, StyleConfig.FontSize.F1)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_valor_anulado), PrintRow.numberFormat(transaccion.getValor()), new StyleConfig(StyleConfig.Align.LEFT, gray)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_entidad), new StyleConfig(StyleConfig.Align.CENTER, gray, StyleConfig.FontSize.F3)));
        printRows.add(new PrintRow(context.getResources().getString(
                R.string.recibo_vigilado), new StyleConfig(StyleConfig.Align.CENTER, gray, StyleConfig.FontSize.F3, 20)));
        printRows.add(new PrintRow(stringCopia, new StyleConfig(StyleConfig.Align.CENTER, gray, StyleConfig.FontSize.F3, 60)));
        printRows.add(new PrintRow(".", new StyleConfig(StyleConfig.Align.LEFT, 1)));
        int status = new PrinterHandler().imprimerTexto(printRows);
        if (status == InfoGlobalSettingsPrint.PRINTER_OK) {
            postEvent(AnulacionEvent.Type.onImprimirReciboSuccess);
        } else {
            postEvent(AnulacionEvent.Type.onImprimirReciboError, PrinterHandler.stringErrorPrinter(status, context));
        }
    }

    private void postEvent(AnulacionEvent.Type type,
                           String errorMessage,
                           int valorInt,
                           InformacionTransaccion informacionTransaccion) {
        AnulacionEvent anulacionEvent = new AnulacionEvent();
        anulacionEvent.setType(type);
        if (errorMessage != null) {
            anulacionEvent.setErrorMessage(errorMessage);
        }
        if (valorInt != AnulacionEvent.VALOR_TRANSACCION_NO_VALIDO) {
            anulacionEvent.setValorInt(valorInt);
        }
        if (informacionTransaccion != null) {
            anulacionEvent.setInformacionTransaccion(informacionTransaccion);
        }
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(anulacionEvent);
    }

    private void postEvent(AnulacionEvent.Type type) {
        postEvent(type,
                null,
                AnulacionEvent.VALOR_TRANSACCION_NO_VALIDO,
                null
        );
    }

    private void postEvent(AnulacionEvent.Type type, int valorInt) {
        postEvent(type,
                null,
                valorInt,
                null
        );
    }

    private void postEvent(AnulacionEvent.Type type,
                           String errorMessage) {
        postEvent(type,
                errorMessage,
                AnulacionEvent.VALOR_TRANSACCION_NO_VALIDO,
                null
        );
    }

    private void postEvent(AnulacionEvent.Type type,
                           InformacionTransaccion informacionTransaccion) {
        postEvent(type,
                null,
                AnulacionEvent.VALOR_TRANSACCION_NO_VALIDO,
                informacionTransaccion
        );
    }
}
