package com.cofrem.transacciones.modules.transaction.credito.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.models.pojo.Servicio;

import java.util.Arrays;
import java.util.List;

/**
 * RecyclerView.Adapter ServicioAdapter
 *
 * @author Christhian Hernando Torres - 2018
 * @version 1.0
 */
public class ServicioAdapter extends RecyclerView.Adapter<ServicioAdapter.ViewHolder> {

    private List<Servicio> items;

    //Variables

    private OnItemClickListener onItemClickListener;

    private Context context;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    public ServicioAdapter(OnItemClickListener onItemClickListener) {

        this.onItemClickListener = onItemClickListener;

    }

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se crea el Recycler View
     *
     * @param viewGroup
     * @param i
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.item_servicio, viewGroup, false);

        return new ViewHolder(view);

    }

    /*
    #############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################
    */

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se enlaza la informacion con el Recycler View
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        Servicio servicio = items.get(position);

        viewHolder.txvServicioDescripcion.setText(servicio.getDescripcion());

        viewHolder.txvServicioSaldo.setText(servicio.getSaldo());

        Drawable imagen;

        switch (servicio.getCodigo_servicio()) {
            case "R":
                imagen = ContextCompat.getDrawable(context, R.drawable.ic_card_giftcard_black);
                break;
            case "A":
                imagen = ContextCompat.getDrawable(context, R.drawable.ic_sync_black);
                break;
            case "B":
                imagen = ContextCompat.getDrawable(context, R.drawable.ic_work_black);
                break;
            default:
                imagen = ContextCompat.getDrawable(context, R.drawable.if_stop);
        }

        viewHolder.imvServicioIcono.setImageDrawable(imagen);

    }

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se solicita el conteo de items
     *
     * @return
     */
    @Override
    public int getItemCount() {

        if (items != null)

            return items.size();

        return 0;

    }

    /**
     * @param dataServicio
     */
    public void swapData(Servicio[] dataServicio) {

        if (dataServicio != null) {

            items = Arrays.asList(dataServicio);

        } else {

            items = null;

        }

        notifyDataSetChanged();
    }

    /**
     * @param adapterPosition
     * @return
     */
    private Servicio obtenerServicio(int adapterPosition) {

        if (items != null) {

            Servicio servicio = items.get(adapterPosition);

            if (servicio != null) {

                return servicio;

            }

        }

        return null;

    }

    /**
     * Escuchador de click
     */
    public interface OnItemClickListener {

        /**
         * @param paramServicio
         * @param paramPosition
         */
        void onClickItemServicio(Servicio paramServicio, int paramPosition);

        /**
         * @param paramServicio
         * @param paramPosition
         */
        void onLongClickItemServicio(Servicio paramServicio, int paramPosition);

    }

    /**
     * Un {@link RecyclerView.ViewHolder} que gestiona la vista formato del Recycler View
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        /**
         * #############################################################################################
         * Definición de variables y controles
         * #############################################################################################
         */


        ImageView imvServicioIcono;

        TextView txvServicioDescripcion;

        TextView txvServicioSaldo;

        RelativeLayout rlyServicios;

        /**
         * @param view
         */
        ViewHolder(View view) {

            super(view);

            context = view.getContext();

            imvServicioIcono = view.findViewById(R.id.imvServicioIcono);

            txvServicioDescripcion = view.findViewById(R.id.txvServicioDescripcion);

            txvServicioSaldo = view.findViewById(R.id.txvServicioSaldo);

            rlyServicios = view.findViewById(R.id.rlyServicios);

            context = view.getContext();

            view.setOnClickListener(this);

            view.setOnLongClickListener(this);

        }

        /**
         * @param view
         */
        @Override
        public void onClick(View view) {

            rlyServicios.setBackgroundColor(view.getContext().getResources().getColor(R.color.appColorPrimaryLight));

            onItemClickListener.onClickItemServicio(
                    obtenerServicio(getAdapterPosition()),
                    getAdapterPosition()
            );

        }

        /**
         * @param view
         */
        @Override
        public boolean onLongClick(View view) {

            rlyServicios.setBackgroundColor(view.getContext().getResources().getColor(R.color.appColorGrayTerciary));

            onItemClickListener.onLongClickItemServicio(
                    obtenerServicio(getAdapterPosition()),
                    getAdapterPosition()
            );


            return true;
        }

    }
}
