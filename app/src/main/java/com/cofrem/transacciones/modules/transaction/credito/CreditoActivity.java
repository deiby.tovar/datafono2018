package com.cofrem.transacciones.modules.transaction.credito;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.lib.magneticcard.MagneticHandler;
import com.cofrem.transacciones.core.utils.CofremUtils;
import com.cofrem.transacciones.models.MoneyTextWatcher;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.pojo.Servicio;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.credito.adapter.ServicioAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;

public class CreditoActivity
        extends BaseAppCompatActivity
        implements CreditoContract.view,
        ServicioAdapter.OnItemClickListener {

    /**
     * Enumerable de pasos  del proceso
     */
    enum PASO_PROCESO {
        VALOR,
        DOCUMENTO,
        VERIFICACION,
        STRIPE,
        SERVICIO,
        PASS,
        EXITO,
        ERROR,
        FAIL_CARD,
    }

    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    @BindView(R.id.clyCredito)
    CoordinatorLayout clyCredito;

    //Elementos del modulo//////////////////////////////////////////////////////////////////////////
    @BindView(R.id.bodyCreditoValor)
    View bodyCreditoValor;

    @BindView(R.id.bodyCreditoNumDoc)
    View bodyCreditoNumDoc;

    @BindView(R.id.bodyCreditoVerificacion)
    View bodyCreditoVerificacion;

    @BindView(R.id.bodyCreditoStripe)
    View bodyCreditoStripe;

    @BindView(R.id.bodyCreditoPass)
    View bodyCreditoPass;

    @BindView(R.id.bodyCreditoExito)
    View bodyCreditoExito;

    @BindView(R.id.bodyCreditoError)
    View bodyCreditoError;

    @BindView(R.id.bodyCreditoServicio)
    View bodyCreditoServicio;

    @BindView(R.id.bodyCreditoFailCard)
    View bodyCreditoFailCard;

    //Paso VALOR////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtValorConsumoValue)
    EditText edtValorConsumoValue;

    //Paso DOCUMENTO////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtNumDocValue)
    EditText edtNumDocValue;

    //Paso VERIFICACION/////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvVerificacionConsumo)
    TextView txvVerificacionConsumo;

    @BindView(R.id.txvVerificacionNumDoc)
    TextView txvVerificacionNumDoc;

    //Paso STRIPE///////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvStripeNumDoc)
    TextView txvStripeNumDoc;

    @BindView(R.id.rlyStripeConsumoContainer)
    RelativeLayout rlyStripeConsumoContainer;

    @BindView(R.id.txvStripeConsumo)
    TextView txvStripeConsumo;

    //Paso PASS/////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtPassValue)
    EditText edtPassValue;

    //Paso SERVICIO/////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.rcvServicio)
    RecyclerView rcvServicio;

    //Paso EXITO////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvExitoMessage)
    TextView txvExitoMessage;

    @BindView(R.id.btnExitoImprimir)
    Button btnExitoImprimir;

    //Paso ERROR////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvErrorMessage)
    TextView txvErrorMessage;

    //Cambio de paso
    private PASO_PROCESO pasoTransaccion = PASO_PROCESO.VALOR;

    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private Transaccion transaccion = new Transaccion();

    private CreditoContract.presenter presenter;

    private ServicioAdapter servicioAdapter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transaction_module_credito);

        setupTitles(getString(R.string.transaction_text_title_credito));

        coordinatorBase = clyCredito;

        typeActivity = TIPO_ACT.MODULO;

        bodyProceso = new View[]{
                bodyCreditoValor,
                bodyCreditoNumDoc,
                bodyCreditoVerificacion,
                bodyCreditoStripe,
                bodyCreditoServicio,
                bodyCreditoPass,
                bodyCreditoExito,
                bodyCreditoError,
                bodyCreditoFailCard
        };

        edtProceso = new EditText[]{
                edtValorConsumoValue,
                edtNumDocValue,
                edtPassValue,
        };

        //Instanciamiento e inicializacion del presentador
        presenter = new CreditoPresenter(this);

        //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();

        edtValorConsumoValue.addTextChangedListener(new MoneyTextWatcher(edtValorConsumoValue));

        //Se registra consumo como el tipo de transaccion
        transaccion.setTipo_transaccion(Transaccion.CODIGO_TIPO_TRANSACCION_CONSUMO);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        rcvServicio.setLayoutManager(layoutManager);

        servicioAdapter = new ServicioAdapter(this);

        rcvServicio.setAdapter(servicioAdapter);

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /**
     * Implementacion de metodo abstracto para manejo de presion de la tecla enter
     */
    public void enterPressed() {
        switch (pasoTransaccion) {
            case VALOR:
                registerValorConsumo();
                break;
            case DOCUMENTO:
                registerNumDoc();
                break;
            case VERIFICACION:
                verifyDatos();
                break;
            case STRIPE:
                stripe();
                break;
            case SERVICIO:
                /* Metodo sin usar */
                break;
            case PASS:
                registerPass();
                break;
            case EXITO:
            case ERROR:
            case FAIL_CARD:
            default:
                navigateToMain();
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */

    /**
     * Metodo para registrar el valor del consumo
     */
    @OnClick(R.id.btnValorConsumoAceptar)
    public void registerValorConsumo() {

        int valorConsumo = CofremUtils.cleanValor(edtValorConsumoValue.getText().toString());

        cleanEditText();

        if (valorConsumo >= 1 && valorConsumo <= 3000000) {

            //Registro de valor
            transaccion.setValor(valorConsumo);

            //Valor del consumo en Verificacion
            txvVerificacionConsumo.setText(PrintRow.numberFormat(transaccion.getValor()));

            //Valor del consumo en Stripe
            txvStripeConsumo.setText(PrintRow.numberFormat(transaccion.getValor()));

            //Cambio de paso
            pasoTransaccion = PASO_PROCESO.DOCUMENTO;

            //Avance de vista
            avanceView();

        } else if (valorConsumo == 0) {

            showMessage(getResources().getString(R.string.transaction_error_valor));

        } else if (valorConsumo > 3000000) {

            showMessage(getResources().getString(R.string.transaction_error_valor_monto_maximo));

        }
    }

    /**
     * Metodo para registrar el numero de documento
     */
    @OnClick(R.id.btnNumDocAceptar)
    public void registerNumDoc() {

        String numDoc = edtNumDocValue.getText().toString();

        cleanEditText();

        if (numDoc.length() > 0) {

            //Registro de numero de documento
            transaccion.setNumero_documento(numDoc);

            //Numero de documento en Verificacion
            txvVerificacionNumDoc.setText(String.valueOf(transaccion.getNumero_documento()));

            //Cambio de paso
            pasoTransaccion = PASO_PROCESO.VERIFICACION;

            //Avance de vista
            avanceView();

        } else {
            showMessage(getResources().getString(R.string.transaction_error_numero_documento));
        }
    }

    /**
     * Metodo para mostrar la información registrada
     */
    @OnClick(R.id.btnVerificacionAceptar)
    public void verifyDatos() {

        //Numero de documento en Stripe
        txvStripeNumDoc.setText(transaccion.getNumero_documento());

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.STRIPE;

        //Avance de vista
        avanceView();

        //Lectura de tarjeta
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stripe();
            }
        }, 200);
    }

    /**
     * Metodo para mostrar la orden de deslizar la tarjeta
     */
    private void stripe() {

        String[] readMagnetic = new MagneticHandler().readMagnetic();

        if (readMagnetic != null) {

            transaccion.setNumero_tarjeta(CofremUtils.cleanNumberCard(readMagnetic));

            readCardSuccess();

        } else {
            readCardError();
        }
    }

    /**
     * Metodo para mostrar la lectura correcta de tarjeta
     */
    private void readCardSuccess() {

        showProgress();

        presenter.verifyServicios(transaccion);

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.SERVICIO;

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo para mostrar la lectura erronea de tarjeta
     */
    private void readCardError() {

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.FAIL_CARD;

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo para registrar la contraseña del usuario
     */
    @OnClick(R.id.btnPassAceptar)
    public void registerPass() {

        String pass = edtPassValue.getText().toString();

        cleanEditText();

        if (pass.length() == 4) {

            showProgress();

            transaccion.setClave(Integer.valueOf(pass));

            presenter.registrarConsumo(transaccion);

        } else {
            showMessage(getResources().getString(R.string.transaction_error_format_clave_usuario));
        }
    }

    /**
     * Metodo que imprime la copia para el cliente
     */
    @OnClick(R.id.btnExitoImprimir)
    public void printRecibo() {

        showProgress();

        presenter.imprimirRecibo(getString(R.string.recibo_copia_cliente));

    }

    /**
     * @param saldoServicio
     */
    private void consumoMenorSaldo(String saldoServicio) {

        int saldoAcumuladoServicios = 0;

        int saldo = CofremUtils.cleanValor(saldoServicio);

        saldoAcumuladoServicios += saldo;

        if (transaccion.getValor() < saldoAcumuladoServicios) {

            //Cambio de paso
            pasoTransaccion = PASO_PROCESO.PASS;

            //Avance de vista
            avanceView();

        } else if (transaccion.getCantidadServicios() == transaccion.getListaServicios().size()) {
            onErrorSaldoInsuficiente();
        }
    }

    /**
     * Metodo para regresar a la ventana de transaccion
     */
    @OnClick({R.id.btnValorConsumoCancelar,
            R.id.btnNumDocCancelar,
            R.id.btnVerificacionCancelar,
            R.id.btnFailCardSalir,
            R.id.btnPassCancelar,
            R.id.btnErrorSalir,
            R.id.btnServicioCancelar,
            R.id.btnExitoSalir})
    public void navigateToMain() {
        super.navigateToMain(this);
    }

    /**
     * #############################################################################################
     * Metodos llamados desde el escuchador de click
     * #############################################################################################
     */

    /**
     * @param servicio
     * @param paramPosition
     */
    @Override
    public void onClickItemServicio(Servicio servicio, int paramPosition) {

        List<String> listaServicio = transaccion.getListaServicios();

        if (!listaServicio.isEmpty()) {

            if (!listaServicio.contains(servicio.getCodigo_servicio())) {

                transaccion.addServicio(servicio.getCodigo_servicio());

                consumoMenorSaldo(servicio.getSaldo());

            }

        } else {

            transaccion.addServicio(servicio.getCodigo_servicio());

            consumoMenorSaldo(servicio.getSaldo());

        }

    }

    /**
     * @param paramServicio
     * @param paramPosition
     */
    @Override
    public void onLongClickItemServicio(Servicio paramServicio, int paramPosition) {
        transaccion.removeServicio(paramServicio.getCodigo_servicio());
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */

    /**
     * @param paramServicio
     */
    @Override
    public void onVerifyServiciosSuccess(Servicio[] paramServicio) {
        hideProgress();
        servicioAdapter.swapData(paramServicio);
    }

    @Override
    public void onVerifyServiciosErrorNoServicios() {
        handlerErrorView("No se encontraron servicios asociados");
    }

    @Override
    public void onVerifyServiciosErrorDocumentoIncorrecto() {
        handlerErrorView("Documento no coincide con la tarjeta");
    }

    @Override
    public void onVerifyServiciosErrorCambioClave() {
        handlerErrorView("Se debe realizar un cambio de clave obligatorio");
    }

    @Override
    public void onRegistrarConsumoSuccess() {
        hideProgress();

        //Boton de copia cliente
        btnExitoImprimir.setText(getString(R.string.general_text_button_imprimir_copia));

        //Texto En Exito
        txvExitoMessage.setText("Consumo registrado correctamente");

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.EXITO;

        //Avance de vista
        avanceView();
    }

    @Override
    public void onRegistrarConsumoTerminalInactiva() {
        handlerErrorView("Terminal inactiva");
    }

    @Override
    public void onErrorSaldoInsuficiente() {
        handlerErrorView("Saldo insuficiente");
    }

    @Override
    public void onRegistrarConsumoError() {
        handlerErrorView("Error desconocido al registrar el consumo, intentelo nuevamente");
    }

    @Override
    public void onRegistrarConsumoDBError() {
        handlerErrorView("Error al registrar el consumo en la terminal");
    }

    @Override
    public void onErrorTarjetaInactiva() {
        handlerErrorView("Tarjeta se encuentra inactiva");
    }

    @Override
    public void onErrorTarjetaInvalida() {
        handlerErrorView("Tarjeta invalida");
    }


    @Override
    public void onImprimirReciboSuccess() {
        hideProgress();
    }

    @Override
    public void onImprimirReciboError(String errorMessage) {
        hideProgress();
        showMessage(errorMessage);
    }

    @Override
    public void onErrorConexion() {
        handlerErrorView(getString(R.string.configuration_text_estado_conexion));
    }

    /**
     * #############################################################################################
     * Metodo de manejos de vistas
     * #############################################################################################
     */
    /**
     * @param errorMessage
     */
    private void handlerErrorView(String errorMessage) {

        hideProgress();

        txvErrorMessage.setText(errorMessage);

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.ERROR;

        //Avance de vista
        avanceView();
    }

    private void avanceView() {
        cleanEditText();
        hideBody();
        switch (pasoTransaccion) {
            case VALOR:
                bodyCreditoValor.setVisibility(View.VISIBLE);
                break;
            case DOCUMENTO:
                bodyCreditoNumDoc.setVisibility(View.VISIBLE);
                break;
            case VERIFICACION:
                bodyCreditoVerificacion.setVisibility(View.VISIBLE);
                break;
            case STRIPE:
                bodyCreditoStripe.setVisibility(View.VISIBLE);
                break;
            case SERVICIO:
                bodyCreditoServicio.setVisibility(View.VISIBLE);
                break;
            case PASS:
                bodyCreditoPass.setVisibility(View.VISIBLE);
                break;
            case EXITO:
                rlyStripeConsumoContainer.setVisibility(View.VISIBLE);

                btnExitoImprimir.setVisibility(View.VISIBLE);

                bodyCreditoExito.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                bodyCreditoError.setVisibility(View.VISIBLE);
                break;
            case FAIL_CARD:
                bodyCreditoFailCard.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    @OnLongClick({R.id.edtValorConsumoValue,
            R.id.edtNumDocValue,
            R.id.edtPassValue
    })
    @OnTouch({R.id.edtValorConsumoValue,
            R.id.edtNumDocValue,
            R.id.edtPassValue
    })
    public boolean hideKeyBoardLong(View v) {
        hideKeyBoard(v);
        return true;
    }

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    @OnClick({R.id.edtValorConsumoValue,
            R.id.edtNumDocValue,
            R.id.edtPassValue
    })
    public void hideKeyBoard(View v) {
        super.hideKeyBoard(v);
    }

}