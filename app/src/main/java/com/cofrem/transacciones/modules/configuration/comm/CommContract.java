package com.cofrem.transacciones.modules.configuration.comm;

import com.cofrem.transacciones.modules.configuration.comm.event.CommEvent;

interface CommContract {
    interface view {
        void onTestComunicationSuccess();

        void onTestComunicationError(String error);
    }

    interface presenter {
        void onCreate();

        void onDestroy();

        void commTest();

        void onEventMainThread(CommEvent commEvent);
    }

    interface repository {
        void commTest();
    }
}
