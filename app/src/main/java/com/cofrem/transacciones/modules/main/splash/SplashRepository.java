package com.cofrem.transacciones.modules.main.splash;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.cofrem.transacciones.core.global.InfoGlobalSettings;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.core.lib.magneticcard.MagneticHandler;
import com.cofrem.transacciones.core.lib.printer.PrinterHandler;
import com.cofrem.transacciones.database.AppDatabase;
import com.cofrem.transacciones.modules.main.splash.event.SplashEvent;

import java.util.Arrays;

public class SplashRepository implements SplashContract.repository {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    private Context context;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     */
    public SplashRepository(Context context) {
        this.context = context;
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    /**
     * Metodo que verifica:
     * - Conexion a internet
     * - Existencia datos en DB interna
     * - Coherencia de datos con el servidor
     */
    @Override
    public void verifyHardware() {
        boolean deviceMagneticReader = verifyDeviceMagneticReader();
        boolean internetConnection = verifyInternetConnection();
        boolean devicePrinter = verifyDevicePrinter();
        boolean deviceNFC = verifyDeviceNFC();
        boolean device = verifyDevice();
        if ((deviceMagneticReader || deviceNFC) && device && devicePrinter && internetConnection) {
            if (verifyRegistrosIniciales()) {
                postEvent(SplashEvent.Type.onVerifyHardwareSuccess);
            } else {
                postEvent(SplashEvent.Type.onVerifyRegistrosInicialesError);
            }
        } else {
            if (!deviceMagneticReader) {
                postEvent(SplashEvent.Type.onMagneticReaderDeviceError);
            }
            if (!deviceNFC) {
                postEvent(SplashEvent.Type.onNFCDeviceError);
            }
            if (!devicePrinter) {
                postEvent(SplashEvent.Type.onPrinterDeviceError);
            }
            if (!internetConnection) {
                postEvent(SplashEvent.Type.onInternetConnectionError);
            }
            if (!device) {
                postEvent(SplashEvent.Type.onDeviceError);
            }
            postEvent(SplashEvent.Type.onVerifyHardwareError);
        }
    }

    /**
     * Metodo que verifica:
     * - La existencia de la configuración inicial
     * - En caso de no existir mostrará la vista de configuración
     * - En caso de existir validara el acceso
     */
    @Override
    public void verifyConfiguracion() {
        switch (AppDatabase.getInstance(context).conteoConfiguracion()) {
            case 0:
                postEvent(SplashEvent.Type.onVerifyConfiguracionNoExiste);
                break;
            case 1:
                postEvent(SplashEvent.Type.onVerifyConfiguracionExiste);
                break;
            default:
                postEvent(SplashEvent.Type.onVerifyConfiguracionNoValida);
                break;
        }
    }

    @Override
    public void getDataHeader() {
        if (AppDatabase.getInstance(context).getDataHeader()) {
            postEvent(SplashEvent.Type.onGetDataHeaderSuccess);
        } else {
            postEvent(SplashEvent.Type.onGetDataHeaderError);
        }
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    /**
     * Metodo que verifica la existencia del registro inicial
     *
     * @return
     */
    private boolean verifyRegistrosIniciales() {
        boolean verificaInicialProducto = false;
        boolean verificaInicialPrinter = false;
        // Validacion en caso de que no existan productos registrados en el sistema
        if (AppDatabase.getInstance(context).conteoProducto() == 0) {
            if (AppDatabase.getInstance(context).insertInicialProducto()) {
                verificaInicialProducto = true;
            }
        } else {
            verificaInicialProducto = true;
        }
        // Validacion en caso de que no exista configuracion inicial para la impresora
        if (AppDatabase.getInstance(context).conteoPrinter() == 0) {
            if (AppDatabase.getInstance(context).insertInicialPrinter()) {
                verificaInicialPrinter = true;
            }
        } else {
            verificaInicialPrinter = true;
        }
        return (verificaInicialProducto && verificaInicialPrinter);
    }

    /**
     * Metodo que verifica la existencia de conexion a internet en el dispositivo
     *
     * @return
     */
    private boolean verifyInternetConnection() {
        boolean resultVerifyInternetConnection = false;
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
        if (networkInfo != null && networkInfo.isConnected()) {
            resultVerifyInternetConnection = true;
        }
        return resultVerifyInternetConnection;
    }

    /**
     * Metodo que verifica la existencia de un lector NFC en el dispositivo
     *
     * @return
     */
    private boolean verifyDeviceNFC() {
        // TODO: Realizar proceso de comprobacion de dispositivo NFC / FASE 1
        return true;
    }

    /**
     * Metodo que verifica la existencia de un lector de banda ma en el dispositivo
     *
     * @return
     */
    private boolean verifyDeviceMagneticReader() {
        // Solicitud de prueba de dispositivo de lectura de banda magnetica
        return new MagneticHandler().testMagneticDevice();
    }

    /**
     * Metodo que verifica la existencia de una impresora en el dispositivo
     *
     * @return
     */
    private boolean verifyDevicePrinter() {
        return new PrinterHandler().testPrinterDevice();
    }

    /**
     * Metodo que verifica la existencia de una impresora en el dispositivo
     *
     * @return
     */
    private boolean verifyDevice() {
        boolean verifyDevice = false;
        if (Arrays.asList(InfoGlobalSettings.supportedDevices).contains(Build.MODEL)) {
            verifyDevice = true;
        }
        return verifyDevice;
    }

    private void postEvent(SplashEvent.Type type,
                           String errorMessage) {
        SplashEvent splashEvent = new SplashEvent();
        splashEvent.setType(type);
        if (errorMessage != null) {
            splashEvent.setErrorMessage(errorMessage);
        }
        EventBus eventBus = GreenRobotEventBus.getInstance();
        eventBus.post(splashEvent);
    }

    private void postEvent(SplashEvent.Type type) {
        postEvent(type, null);
    }
}
