package com.cofrem.transacciones.modules.report;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.models.Reports;
import com.cofrem.transacciones.modules.report.reimpresion.ReimpresionActivity;

public class ReportActivity
        extends BaseAppCompatActivity {

    @BindView(R.id.clyReport)
    CoordinatorLayout clyReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        coordinatorBase = clyReport;
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */

    @OnClick(R.id.btnReportUltimoRecibo)
    public void navigateToReportsReimpresion() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Reports.keyReport, Reports.TYPE_REPORT.ULTIMO_RECIBO);
        navigateTo(this, ReimpresionActivity.class, bundle);
    }

    @OnClick(R.id.btnReportNumCargo)
    public void navigateToReportsNumCargo() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Reports.keyReport, Reports.TYPE_REPORT.NUM_CARGO);
        navigateTo(this, ReimpresionActivity.class, bundle);
    }

    @OnClick(R.id.btnReportDetalle)
    public void navigateToReportsDetalle() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Reports.keyReport, Reports.TYPE_REPORT.DETALLE);
        navigateTo(this, ReimpresionActivity.class, bundle);
    }

    @OnClick(R.id.btnReportGeneral)
    public void navigateToReportsGeneral() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Reports.keyReport, Reports.TYPE_REPORT.GENERAl);
        navigateTo(this, ReimpresionActivity.class, bundle);
    }

    @OnClick(R.id.btnReportCierreLote)
    public void navigateToCierreLote() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Reports.keyReport, Reports.TYPE_REPORT.NUM_CARGO);
        navigateTo(this, ReimpresionActivity.class, bundle);
    }

    @OnClick(R.id.btnReportBack)
    public void navigateToMain() {
        super.navigateToMain(this);
    }
}