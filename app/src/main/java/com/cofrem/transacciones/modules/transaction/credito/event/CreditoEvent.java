package com.cofrem.transacciones.modules.transaction.credito.event;

import com.cofrem.transacciones.models.pojo.Servicio;

public class CreditoEvent {

    public enum Type {
        onVerifyServiciosSuccess,
        onVerifyServiciosErrorNoServicios,
        onVerifyServiciosErrorDocumentoIncorrecto,
        onVerifyServiciosErrorCambioClave, onRegistrarConsumoSuccess,
        onRegistrarConsumoTerminalInactiva,
        onErrorSaldoInsuficiente,
        onRegistrarConsumoError,
        onRegistrarConsumoDBError, onImprimirReciboSuccess,
        onImprimirReciboError, onErrorTarjetaInactiva,
        onErrorTarjetaInvalida, onErrorConexion,
    }

    private Type type;

    private String errorMessage;

    private Servicio[] servicio;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Servicio[] getServicio() {
        return servicio;
    }

    public void setServicio(Servicio[] servicio) {
        this.servicio = servicio;
    }
}
