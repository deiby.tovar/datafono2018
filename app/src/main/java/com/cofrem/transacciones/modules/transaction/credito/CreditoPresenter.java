package com.cofrem.transacciones.modules.transaction.credito;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.models.pojo.Servicio;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.credito.event.CreditoEvent;
import org.greenrobot.eventbus.Subscribe;

public class CreditoPresenter implements CreditoContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    Transaccion transaccionOLD;
    int intentos;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private CreditoContract.view view;

    private CreditoContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public CreditoPresenter(CreditoContract.view view) {
        this.view = view;
        this.repository = new CreditoRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    @Override
    public void verifyServicios(Transaccion transaccionOLD) {
        intentos = 1;
        this.transaccionOLD = transaccionOLD;
        repository.verifyServicios(transaccionOLD);
    }

    @Override
    public void registrarConsumo(Transaccion transaccionOLD) {
        this.transaccionOLD = transaccionOLD;
        repository.registrarConsumo(transaccionOLD);
    }

    /**
     * Metodo que imprime el recibo de la transaccionOLD
     */
    @Override
    public void imprimirRecibo(String stringCopia) {
        repository.imprimirRecibo(stringCopia);
    }

    @Override
    @Subscribe
    public void onEventMainThread(CreditoEvent creditoEvent) {
        switch (creditoEvent.getType()) {
            case onVerifyServiciosSuccess:
                onVerifyServiciosSuccess(creditoEvent.getServicio());
                break;
            case onVerifyServiciosErrorNoServicios:
                onVerifyServiciosErrorNoServicios();
                break;
            case onVerifyServiciosErrorDocumentoIncorrecto:
                onVerifyServiciosErrorDocumentoIncorrecto();
                break;
            case onVerifyServiciosErrorCambioClave:
                onVerifyServiciosErrorCambioClave();
                break;
            case onRegistrarConsumoSuccess:
                onRegistrarConsumoSuccess();
                break;
            case onRegistrarConsumoTerminalInactiva:
                onRegistrarConsumoTerminalInactiva();
                break;
            case onRegistrarConsumoError:
                onRegistrarConsumoError();
                break;
            case onRegistrarConsumoDBError:
                onRegistrarConsumoDBError();
                break;
            case onImprimirReciboSuccess:
                onImpresionReciboSuccess();
                break;
            case onImprimirReciboError:
                onImpresionReciboError(creditoEvent.getErrorMessage());
                break;
            case onErrorTarjetaInactiva:
                onErrorTarjetaInactiva();
                break;
            case onErrorTarjetaInvalida:
                onErrorTarjetaInvalida();
                break;
            case onErrorSaldoInsuficiente:
                onErrorSaldoInsuficiente();
                break;
            case onErrorConexion:
                onErrorConexion();
                break;
        }
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    /**
     * @param dataServicio
     */
    private void onVerifyServiciosSuccess(Servicio[] dataServicio) {
        if (view != null) {
            view.onVerifyServiciosSuccess(dataServicio);
        }
    }

    private void onVerifyServiciosErrorNoServicios() {
        if (view != null) {
            view.onVerifyServiciosErrorNoServicios();
        }
    }

    private void onVerifyServiciosErrorDocumentoIncorrecto() {
        if (view != null) {
            view.onVerifyServiciosErrorDocumentoIncorrecto();
        }
    }

    private void onVerifyServiciosErrorCambioClave() {
        if (view != null) {
            view.onVerifyServiciosErrorCambioClave();
        }
    }

    private void onRegistrarConsumoSuccess() {
        if (view != null) {
            view.onRegistrarConsumoSuccess();
        }
    }

    private void onRegistrarConsumoTerminalInactiva() {
        if (view != null) {
            view.onRegistrarConsumoTerminalInactiva();
        }
    }

    private void onErrorSaldoInsuficiente() {
        if (view != null) {
            view.onErrorSaldoInsuficiente();
        }
    }

    private void onRegistrarConsumoError() {
        if (view != null) {
            view.onRegistrarConsumoError();
        }
    }

    private void onRegistrarConsumoDBError() {
        if (view != null) {
            view.onRegistrarConsumoDBError();
        }
    }

    private void onImpresionReciboSuccess() {
        if (view != null) {
            view.onImprimirReciboSuccess();
        }
    }

    private void onImpresionReciboError(String errorMessage) {
        if (view != null) {
            view.onImprimirReciboError(errorMessage);
        }
    }

    private void onErrorTarjetaInactiva() {
        if (view != null) {
            view.onErrorTarjetaInactiva();
        }
    }

    private void onErrorTarjetaInvalida() {
        if (view != null) {
            view.onErrorTarjetaInvalida();
        }
    }

    private void onErrorConexion() {
        if (view != null) {
            view.onErrorConexion();
        }
    }
}
