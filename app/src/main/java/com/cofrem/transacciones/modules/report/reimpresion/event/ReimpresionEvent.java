package com.cofrem.transacciones.modules.report.reimpresion.event;

import com.cofrem.transacciones.models.pojo.Transaccion;

import java.util.ArrayList;

public class ReimpresionEvent {

    public enum Type {
        onVerifySuccess,
        onVerifyExistenceUltimoReciboSuccess,
        onVerifyExistenceUltimoReciboError,
        onImprimirUltimoReciboSuccess,
        onImprimirUltimoReciboError,
        onVerifyExistenceReciboPorNumCargoSuccess,
        onVerifyExistenceReciboPorNumCargoError,
        onImprimirReciboPorNumCargoSuccess,
        onImprimirReciboPorNumCargoError,
        onVerifyExistenceReporteDetalleSuccess,
        onVerifyExistenceReporteDetalleError,
        onImprimirReporteDetalleSuccess,
        onImprimirReporteDetalleError,
        onVerifyExistenceReporteGeneralSuccess,
        onVerifyExistenceReporteGeneralError,
        onImprimirReporteGeneralSuccess,
        onImprimirReporteGeneralError,
        onVerifyClaveAdministradorSuccess,
        onVerifyClaveAdministradorError, onCierreLoteSuccess,
        onCierreLoteError,
        onTransaccionError,
    }

    private Type type;

    private String errorMessage;

    private Transaccion modelTransaccionOLD;

    private ArrayList<Transaccion> listaTransacciones;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type =type;
}

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Transaccion getModelTransaccionOLD() {
        return modelTransaccionOLD;
    }

    public void setModelTransaccionOLD(Transaccion modelTransaccionOLD) {
        this.modelTransaccionOLD = modelTransaccionOLD;
    }

    public ArrayList<Transaccion> getListaTransacciones() {
        return listaTransacciones;
    }

    public void setListaTransacciones(ArrayList<Transaccion> listaTransacciones) {
        this.listaTransacciones = listaTransacciones;
    }
}
