package com.cofrem.transacciones.modules.transaction.anulacion;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.lib.magneticcard.MagneticHandler;
import com.cofrem.transacciones.core.utils.CofremUtils;
import com.cofrem.transacciones.models.PrintRow;
import com.cofrem.transacciones.models.pojo.Transaccion;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;

public class AnulacionActivity
        extends BaseAppCompatActivity
        implements AnulacionContract.view {

    /**
     * Enumerable de pasos  del proceso
     */
    enum PASO_PROCESO {
        PASS_ADMIN,
        DOCUMENTO,
        NUM_CARGO,
        VERIFICACION,
        STRIPE,
        PASS,
        EXITO,
        ERROR,
        FAIL_CARD,
    }

    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */

    @BindView(R.id.clyAnulacion)
    CoordinatorLayout clyAnulacion;

    //Elementos del modulo//////////////////////////////////////////////////////////////////////////
    @BindView(R.id.bodyAnulacionPassAdmin)
    View bodyAnulacionPassAdmin;

    @BindView(R.id.bodyAnulacionNumDoc)
    View bodyAnulacionNumDoc;

    @BindView(R.id.bodyAnulacionNumCargo)
    View bodyAnulacionNumCargo;

    @BindView(R.id.bodyAnulacionVerificacion)
    View bodyAnulacionVerificacion;

    @BindView(R.id.bodyAnulacionStripe)
    View bodyAnulacionStripe;

    @BindView(R.id.bodyAnulacionPass)
    View bodyAnulacionPass;

    @BindView(R.id.bodyAnulacionExito)
    View bodyAnulacionExito;

    @BindView(R.id.bodyAnulacionFailCard)
    View bodyAnulacionFailCard;

    @BindView(R.id.bodyAnulacionError)
    View bodyAnulacionError;

    //Paso PASS_ADMIN
    @BindView(R.id.edtPassAdminValue)
    EditText edtPassAdminValue;

    //Paso DOCUMENTO
    @BindView(R.id.edtNumDocValue)
    EditText edtNumDocValue;

    //Paso NUM_CARGO
    @BindView(R.id.edtNumCargoValue)
    EditText edtNumCargoValue;

    //Paso VERIFICACION
    @BindView(R.id.txvVerificacionConsumo)
    TextView txvVerificacionValor;

    @BindView(R.id.txvVerificacionNumDoc)
    TextView txvVerificacionNumDoc;

    @BindView(R.id.rlyVerificacionNumCargoContainer)
    RelativeLayout rlyVerificacionNumCargoContainer;

    @BindView(R.id.txvVerificacionNumCargo)
    TextView txvVerificacionNumCargo;

    //Paso STRIPE
    @BindView(R.id.txvStripeNumDoc)
    TextView txvStripeNumDoc;

    @BindView(R.id.txvStripeNumCargo)
    TextView txvStripeNumCargo;

    @BindView(R.id.rlyStripeConsumoContainer)
    RelativeLayout rlyStripeConsumoContainer;

    @BindView(R.id.txvStripeConsumo)
    TextView txvStripeConsumo;

    //Paso PASS
    @BindView(R.id.edtPassValue)
    EditText edtPassValue;

    //Paso EXITO
    @BindView(R.id.txvAnulacionExitoMessage)
    TextView txvAnulacionExitoMessage;

    //Paso ERROR
    @BindView(R.id.txvErrorMessage)
    TextView txvErrorMessage;

    Transaccion transaccion = new Transaccion();

    //Cambio de paso
    PASO_PROCESO pasoTransaccion = PASO_PROCESO.PASS_ADMIN;

    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private AnulacionContract.presenter presenter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transaction_module_anulacion);

        setupTitles(getString(R.string.transaction_text_title_anulacion));

        coordinatorBase = clyAnulacion;

        typeActivity = TIPO_ACT.MODULO;

        bodyProceso = new View[]{
                bodyAnulacionPassAdmin,
                bodyAnulacionNumDoc,
                bodyAnulacionNumCargo,
                bodyAnulacionVerificacion,
                bodyAnulacionStripe,
                bodyAnulacionPass,
                bodyAnulacionExito,
                bodyAnulacionFailCard,
                bodyAnulacionError,
        };
        edtProceso = new EditText[]{
                edtPassAdminValue,
                edtNumDocValue,
                edtNumCargoValue,
                edtPassValue,
        };
        //Instanciamiento e inicializacion del presentador
        presenter = new AnulacionPresenter(this);

        //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /**
     * Implementacion de metodo abstracto para manejo de presion de la tecla enter
     */
    @Override
    public void enterPressed() {
        switch (pasoTransaccion) {
            case PASS_ADMIN:
                validatePassAdmin();
                break;
            case DOCUMENTO:
                registerNumDoc();
                break;
            case NUM_CARGO:
                registerNumCargo();
                break;
            case VERIFICACION:
                verifyDatos();
                break;
            case STRIPE:
                stripe();
                break;
            case PASS:
                registerPass();
                break;
            case EXITO:
            default:
                navigateToMain();
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */

    /**
     * Metodo que valida la contraseña de administracion
     */
    @OnClick(R.id.btnPassAdminAceptar)
    public void validatePassAdmin() {

        String passAdmin = edtPassAdminValue.getText().toString();

        cleanEditText();

        if (passAdmin.length() == 4) {

            showProgress();

            presenter.validatePassAdmin(passAdmin);

        } else {
            showMessage(getString(R.string.transaction_error_format_clave_administrador));
        }
    }

    /**
     * Metodo para registrar el numero de documento
     */
    @OnClick(R.id.btnNumDocAceptar)
    public void registerNumDoc() {

        String numDoc = edtNumDocValue.getText().toString();

        cleanEditText();

        if (numDoc.length() > 0) {

            transaccion.setNumero_documento(numDoc);

            //Cambio de paso
            pasoTransaccion = PASO_PROCESO.NUM_CARGO;

            //Avance de vista
            avanceView();

        } else {
            showMessage(getResources().getString(R.string.transaction_error_numero_documento));
        }
    }

    /**
     * Metodo para registrar el numero de cargo
     */
    @OnClick(R.id.btnNumCargoAceptar)
    public void registerNumCargo() {

        String numCargo = edtNumCargoValue.getText().toString();

        cleanEditText();

        if (numCargo.length() > 0 && !numCargo.equals("0")) {

            transaccion.setNumero_transaccion(numCargo);

            showProgress();

            presenter.getValorTransaccion(transaccion.getNumero_transaccion());

        } else {
            showMessage(getResources().getString(R.string.transaction_error_format_numero_cargo));
        }
    }


    /**
     * Metodo para mostrar la información registrada
     */
    @OnClick(R.id.btnVerificacionAceptar)
    public void verifyDatos() {

        //Numero de documento
        txvStripeNumDoc.setText(String.valueOf(transaccion.getNumero_documento()));

        //Valor del consumo
        rlyStripeConsumoContainer.setVisibility(View.VISIBLE);

        txvStripeConsumo.setText(PrintRow.numberFormat(transaccion.getValor()));

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.STRIPE;

        //Avance de vista
        avanceView();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stripe();
            }
        }, 200);
    }

    /**
     * Metodo para mostrar la orden de deslizar la tarjeta
     */
    private void stripe() {

        String[] readMagnetic = new MagneticHandler().readMagnetic();

        if (readMagnetic != null) {

            transaccion.setNumero_tarjeta(CofremUtils.cleanNumberCard(readMagnetic));

            readCardSuccess();

        } else {
            readCardError();
        }
    }

    /**
     * Metodo para mostrar la lectura correcta de tarjeta
     */
    private void readCardSuccess() {
        showProgress();

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.PASS;

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo para mostrar la lectura erronea de tarjeta
     */
    private void readCardError() {

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.FAIL_CARD;

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo para registrar la contraseña del usuario
     */
    @OnClick(R.id.btnPassAceptar)
    public void registerPass() {
        String pass = edtPassValue.getText().toString();
        cleanEditText();
        if (pass.length() == 4) {
            showProgress();
            transaccion.setClave(Integer.valueOf(pass));
            presenter.registerAnulacion(transaccion);
        } else {
            showMessage(getResources().getString(R.string.transaction_error_format_clave_usuario));
        }
    }

    /**
     * Metodo que imprime la copia para el cliente
     */
    @OnClick(R.id.btnAnulacionExitoImprimir)
    public void printRecibo() {
        showProgress();
        presenter.imprimirRecibo(getString(R.string.recibo_copia_cliente));
    }

    /**
     * Metodo para regresar a la ventana de transaccion
     */
    @OnClick({R.id.btnPassAdminCancelar,
            R.id.btnNumDocCancelar,
            R.id.btnNumCargoCancelar,
            R.id.btnVerificacionCancelar,
            R.id.btnPassCancelar,
            R.id.btnAnulacionExitoSalir,
            R.id.btnFailCardSalir,
            R.id.btnErrorSalir})
    public void navigateToMain() {
        super.navigateToMain(this);
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */

    @Override
    public void onValidatePassAdminCorrecta() {

        hideProgress();

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.DOCUMENTO;

        //Avance de vista
        avanceView();
    }

    @Override
    public void onvalidatePassAdminError(String errorMessage) {
        handlerErrorView(errorMessage);
    }

    @Override
    public void onGetValorTransaccionSuccess(int valor) {

        hideProgress();

        transaccion.setValor(valor);

        txvVerificacionNumDoc.setText(transaccion.getNumero_documento());

        txvVerificacionValor.setText(String.valueOf(transaccion.getValor()));

        //Numero de cargo
        rlyVerificacionNumCargoContainer.setVisibility(View.VISIBLE);

        txvVerificacionNumCargo.setVisibility(View.VISIBLE);

        txvVerificacionNumCargo.setText(PrintRow.numberFormat(transaccion.getNumero_transaccion()));

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.VERIFICACION;

        //Avance de vista
        avanceView();
    }

    @Override
    public void onGetValorTransaccionError() {
        handlerErrorView(getString(R.string.transaction_error_numero_Cargo_no_relacionado));
    }

    /**
     * Metodo para manejar la transaccion exitosa
     */
    @Override
    public void onRegisterAnulacionSuccess() {

        hideProgress();

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.EXITO;

        //Avance de vista
        avanceView();

    }

    @Override
    public void onRegisterAnulacionError() {
        handlerErrorView("Error desconocido al registrar la anulacion, intentelo nuevamente");
    }

    @Override
    public void onImprimirReciboSuccess() {
        hideProgress();
    }

    @Override
    public void onImprimirReciboError(String errorMessage) {
        hideProgress();
        showMessage(errorMessage);
    }

    @Override
    public void onErrorConexion() {
        handlerErrorView(getString(R.string.configuration_text_estado_conexion));
    }

    /**
     * #############################################################################################
     * Metodo de manejos de vistas
     * #############################################################################################
     */
    /**
     * @param errorMessage
     */
    private void handlerErrorView(String errorMessage) {

        hideProgress();

        txvErrorMessage.setText(errorMessage);

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.ERROR;

        //Avance de vista
        avanceView();
    }

    private void avanceView() {

        cleanEditText();

        hideBody();

        switch (pasoTransaccion) {
            case PASS_ADMIN:
                bodyAnulacionPassAdmin.setVisibility(View.VISIBLE);
                break;
            case DOCUMENTO:
                bodyAnulacionNumDoc.setVisibility(View.VISIBLE);
                break;
            case NUM_CARGO:
                bodyAnulacionNumCargo.setVisibility(View.VISIBLE);
                break;
            case VERIFICACION:
                bodyAnulacionVerificacion.setVisibility(View.VISIBLE);
                break;
            case STRIPE:
                bodyAnulacionStripe.setVisibility(View.VISIBLE);
                break;
            case PASS:
                bodyAnulacionPass.setVisibility(View.VISIBLE);
                break;
            case EXITO:
                bodyAnulacionExito.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                bodyAnulacionError.setVisibility(View.VISIBLE);
                break;
            case FAIL_CARD:
                bodyAnulacionFailCard.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    @OnLongClick({R.id.edtPassAdminValue,
            R.id.edtNumDocValue,
            R.id.edtNumCargoValue,
            R.id.edtPassValue})
    @OnTouch({R.id.edtPassAdminValue,
            R.id.edtNumDocValue,
            R.id.edtNumCargoValue,
            R.id.edtPassValue})
    public boolean hideKeyBoardLong(View v) {
        hideKeyBoard(v);
        return true;
    }

    /**
     * Metodo que oculta el teclado al presionar el EditText
     */
    @OnClick({R.id.edtPassAdminValue,
            R.id.edtNumDocValue,
            R.id.edtNumCargoValue,
            R.id.edtPassValue})
    public void hideKeyBoard(View v) {
        super.hideKeyBoard(v);
    }
}
