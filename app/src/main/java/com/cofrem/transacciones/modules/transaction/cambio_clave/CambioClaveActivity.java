package com.cofrem.transacciones.modules.transaction.cambio_clave;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.lib.magneticcard.MagneticHandler;
import com.cofrem.transacciones.core.utils.CofremUtils;
import com.cofrem.transacciones.models.Tarjeta;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;

public class CambioClaveActivity
        extends BaseAppCompatActivity
        implements CambioClaveContract.view {

    /**
     * Enumerable de pasos  del proceso
     */
    enum PASO_PROCESO {
        DOCUMENTO,
        STRIPE,
        PASS_ACTUAL,
        PASS_NUEVA,
        EXITO,
        ERROR,
        FAIL_CARD,
    }

    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    @BindView(R.id.clyCambioClave)
    CoordinatorLayout clyCambioClave;

    //Elementos del modulo//////////////////////////////////////////////////////////////////////////
    @BindView(R.id.bodyCambioClaveNumDoc)
    View bodyCambioClaveNumDoc;

    @BindView(R.id.bodyCambioClaveStripe)
    View bodyCambioClaveStripe;

    @BindView(R.id.bodyCambioClavePass)
    View bodyCambioClavePass;

    @BindView(R.id.bodyCambioClavePassNueva)
    View bodyCambioClavePassNueva;

    @BindView(R.id.bodyCambioClaveExito)
    View bodyCambioClaveExito;

    @BindView(R.id.bodyCambioClaveError)
    View bodyCambioClaveError;

    @BindView(R.id.bodyCambioClaveFailCard)
    View bodyCambioClaveFailCard;

    //Paso DOCUMENTO////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtNumDocValue)
    EditText edtNumDocValue;

    //Paso STRIPE///////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvStripeNumDoc)
    TextView txvStripeNumDoc;

    //Paso PASS ACTUAL//////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtPassValue)
    EditText edtPassValue;

    //Paso PASS NUEVA///////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.edtPassNueva)
    EditText edtPassNueva;

    @BindView(R.id.edtPassNuevaConfirmacion)
    EditText edtPassNuevaConfirmacion;

    //Paso EXITO////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvExitoMessage)
    TextView txvExitoMessage;

    //Paso ERROR////////////////////////////////////////////////////////////////////////////////////
    @BindView(R.id.txvErrorMessage)
    TextView txvErrorMessage;

    //Cambio de paso
    private PASO_PROCESO pasoTransaccion = PASO_PROCESO.DOCUMENTO;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    Tarjeta tarjeta = new Tarjeta();

    private CambioClaveContract.presenter presenter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transaction_module_cambio_clave);

        setupTitles(getString(R.string.transaction_text_title_cambio_clave));

        coordinatorBase = clyCambioClave;

        typeActivity = TIPO_ACT.MODULO;

        bodyProceso = new View[]{
                bodyCambioClaveNumDoc,
                bodyCambioClaveStripe,
                bodyCambioClavePass,
                bodyCambioClavePassNueva,
                bodyCambioClaveExito,
                bodyCambioClaveError,
                bodyCambioClaveFailCard
        };

        edtProceso = new EditText[]{
                edtNumDocValue,
                edtPassValue,
                edtPassNueva,
                edtPassNuevaConfirmacion
        };

        //Instanciamiento e inicializacion del presentador
        presenter = new CambioClavePresenter(this);

        //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();

        //Avance de vista
        avanceView();

    }

    /**
     * #############################################################################################
     * Metodos sobrecargados del sistema
     * #############################################################################################
     */

    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /**
     * Implementacion de metodo abstracto para manejo de presion de la tecla enter
     */
    public void enterPressed() {
        switch (pasoTransaccion) {
            case DOCUMENTO:
                registerNumDoc();
                break;
            case STRIPE:
                stripe();
                break;
            case PASS_ACTUAL:
                registerPass();
                break;
            case PASS_NUEVA:
                registerPassNueva();
                break;
            case EXITO:
            case ERROR:
            case FAIL_CARD:
            default:
                navigateToMain();
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */

    /**
     * Metodo para registrar el numero de documento
     */
    @OnClick(R.id.btnNumDocAceptar)
    public void registerNumDoc() {

        String numDoc = edtNumDocValue.getText().toString();

        cleanEditText();

        if (numDoc.length() > 0) {

            //Registro de valor
            tarjeta.setNumDocumento(numDoc);

            //Numero de documento en Stripe
            txvStripeNumDoc.setText(tarjeta.getNumDocumento());

            //Cambio de paso
            pasoTransaccion = PASO_PROCESO.STRIPE;

            //Avance de vista
            avanceView();

            //Lectura de tarjeta
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    stripe();
                }
            }, 200);

        } else {
            showMessage(getResources().getString(R.string.transaction_error_numero_documento));
        }
    }

    /**
     * Metodo para mostrar la orden de deslizar la tarjeta
     */
    private void stripe() {

        String[] readMagnetic = new MagneticHandler().readMagnetic();

        if (readMagnetic != null) {

            tarjeta.setNumTarjeta(CofremUtils.cleanNumberCard(readMagnetic));

            readCardSuccess();

        } else {
            readCardError();
        }
    }

    /**
     * Metodo para mostrar la lectura correcta de tarjeta
     */
    private void readCardSuccess() {

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.PASS_ACTUAL;

        //Avance de vista
        avanceView();
    }

    /**
     * Metodo para mostrar la lectura erronea de tarjeta
     */
    private void readCardError() {

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.FAIL_CARD;

        //Avance de vista
        avanceView();
    }

    /**
     * Registra la constraseña actual
     */
    @OnClick(R.id.btnPassAceptar)
    public void registerPass() {

        String pass = edtPassValue.getText().toString();

        cleanEditText();

        if (pass.length() == 4) {

            showProgress();

            tarjeta.setPassActual(pass);

            showProgress();

            presenter.verifyPassActual(tarjeta);

        } else {
            showMessageError(getString(R.string.transaction_error_format_clave_usuario));
        }
    }

    /**
     * Registra la nueva contraseña
     */
    @OnClick(R.id.btnPassNuevaAceptar)
    public void registerPassNueva() {

        String passNueva = edtPassNueva.getText().toString();

        String passNuevaConfirmacion = edtPassNuevaConfirmacion.getText().toString();

        cleanEditText();

        if (passNueva.length() == 4) {

            if (passNueva.equals(passNuevaConfirmacion)) {

                tarjeta.setPassNueva(passNueva);

                showProgress();

                presenter.verifyPassNueva(tarjeta);

            } else {
                showMessageError(getString(R.string.transaction_error_clave_no_coincide));
            }
        } else {
            showMessageError(getString(R.string.transaction_error_format_clave_usuario));
        }
    }

    @OnClick({
            R.id.btnNumDocCancelar,
            R.id.btnPassCancelar,
            R.id.btnPassNuevaCancelar,
            R.id.btnFailCardSalir,
            R.id.btnErrorSalir,
            R.id.btnExitoSalir
    })
    public void navigateToMain() {
        super.navigateToMain(this);
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    @Override
    public void onValidarPassActualSuccess() {

        hideProgress();

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.PASS_NUEVA;

        //Avance de vista
        avanceView();
    }

    @Override
    public void onVerifyPassActualErrorPassIncorrecta() {
        handlerErrorView("Contraseña incorrecta");
    }

    @Override
    public void onVerifyPassActualErrorDocumentoIncorrecto() {
        handlerErrorView("Documento incorrecto");
    }

    @Override
    public void onVerifyPassActualErrorTarjetaInactiva() {
        handlerErrorView("Tarjeta inactiva");
    }

    @Override
    public void onVerifyPassActualErrorTarjetaInvalida() {
        handlerErrorView("Tarjeta inactiva");
    }

    @Override
    public void onVerifyPassNuevaSuccess() {

        hideProgress();

        txvExitoMessage.setText("La contraseña se ha actualizado correctamente.");

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.EXITO;

        //Avance de vista
        avanceView();
    }

    @Override
    public void onVerifyPassNuevaErrorPassIncorrecta() {
        handlerErrorView("Contraseña incorrecta");
    }

    @Override
    public void onVerifyPassNuevaErrorDocumentoIncorrecto() {
        handlerErrorView("Documento incorrecto");
    }

    @Override
    public void onVerifyPassNuevaErrorPassFormatoIncorrecto() {
        handlerErrorView("Formato de contraseña incorrecto");
    }

    @Override
    public void onVerifyPassNuevaErrorTarjetaInactiva() {
        handlerErrorView("Tarjeta inactiva");
    }

    @Override
    public void onVerifyPassNuevaErrorTarjetaInvalida() {
        handlerErrorView("Tarjeta invalida");
    }

    @Override
    public void onErrorConexion() {
        handlerErrorView(getString(R.string.configuration_text_estado_conexion));
    }

    /**
     * #############################################################################################
     * Metodo de manejos de vistas
     * #############################################################################################
     */
    /**
     * @param errorMessage
     */
    private void handlerErrorView(String errorMessage) {

        hideProgress();

        txvErrorMessage.setText(errorMessage);

        //Cambio de paso
        pasoTransaccion = PASO_PROCESO.ERROR;

        //Avance de vista
        avanceView();
    }

    private void avanceView() {

        cleanEditText();

        hideBody();

        switch (pasoTransaccion) {
            case DOCUMENTO:
                bodyCambioClaveNumDoc.setVisibility(View.VISIBLE);
                break;
            case STRIPE:
                bodyCambioClaveStripe.setVisibility(View.VISIBLE);
                break;
            case PASS_ACTUAL:
                bodyCambioClavePass.setVisibility(View.VISIBLE);
                break;
            case PASS_NUEVA:
                bodyCambioClavePassNueva.setVisibility(View.VISIBLE);
                break;
            case EXITO:
                bodyCambioClaveExito.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                bodyCambioClaveError.setVisibility(View.VISIBLE);
                break;
            case FAIL_CARD:
                bodyCambioClaveFailCard.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * @param v
     * @return
     */
    @OnLongClick({
            R.id.edtNumDocValue,
            R.id.edtPassValue,
            R.id.edtPassNueva,
            R.id.edtPassNuevaConfirmacion
    })
    @OnTouch({
            R.id.edtNumDocValue,
            R.id.edtPassValue,
            R.id.edtPassNueva,
            R.id.edtPassNuevaConfirmacion
    })
    public boolean hideKeyBoardLong(View v) {
        hideKeyBoard(v);
        return true;
    }

    /**
     * @param v
     */
    @OnClick({
            R.id.edtNumDocValue,
            R.id.edtPassValue,
            R.id.edtPassNueva,
            R.id.edtPassNuevaConfirmacion
    })
    public void hideKeyBoard(View v) {
        super.hideKeyBoard(v);
    }
}