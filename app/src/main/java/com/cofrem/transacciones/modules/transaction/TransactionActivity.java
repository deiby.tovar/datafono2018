package com.cofrem.transacciones.modules.transaction;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.modules.transaction.anulacion.AnulacionActivity;
import com.cofrem.transacciones.modules.transaction.cambio_clave.CambioClaveActivity;
import com.cofrem.transacciones.modules.transaction.credito.CreditoActivity;
import com.cofrem.transacciones.modules.transaction.saldo.SaldoActivity;

public class TransactionActivity
        extends BaseAppCompatActivity {
    //Control coordinador de elementos
    @BindView(R.id.clyTransaction)
    CoordinatorLayout clyTransaction;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        coordinatorBase = clyTransaction;
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    /**
     * Metodo para navegar a la ventana Credito
     */
    @OnClick(R.id.btnTransCredito)
    public void navigateToCredito() {
        navigateTo(this, CreditoActivity.class);
    }

    /**
     * Metodo para navegar a la ventana Anulacion
     */
    @OnClick(R.id.btnTransAnulacion)
    public void navigateToAnulacion() {
        navigateTo(this, AnulacionActivity.class);
    }

    /**
     * Metodo para navegar a la ventana Saldo
     */
    @OnClick(R.id.btnTransSaldo)
    public void navigateToSaldo() {
        navigateTo(this, SaldoActivity.class);
    }

    /**
     * Metodo para navegar a la ventana Cambio de Clave
     */
    @OnClick(R.id.btnTransCambioClave)
    public void navigateToCambioClave() {
        navigateTo(this, CambioClaveActivity.class);
    }

    /**
     * Metodo para navegar a la ventana principal
     */
    @OnClick(R.id.btnTransBack)
    public void navigateToMain() {
        super.navigateToMain(this);
    }
}