package com.cofrem.transacciones.modules.transaction.credito.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.models.Servicio_OLD;

import java.util.ArrayList;

/**
 * Created by luisp on 17/10/2017.
 */
public class servicio_old extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<Servicio_OLD> items;

    public servicio_old(Activity activity, ArrayList<Servicio_OLD> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<Servicio_OLD> servicioOLDS) {
        for (int i = 0; i < servicioOLDS.size(); i++) {
            items.add(servicioOLDS.get(i));
        }
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_servicio, null);
        }
        Servicio_OLD servicioOLD = items.get(position);
        TextView texto = v.findViewById(R.id.txvServicioDescripcion);
        texto.setText(servicioOLD.getDescripcion());
        TextView valor = v.findViewById(R.id.txvServicioSaldo);
        valor.setText(servicioOLD.getValor());
        ImageView imagen = v.findViewById(R.id.imvServicioIcono);
        imagen.setImageDrawable(servicioOLD.getImagen());
        return v;
    }
}
