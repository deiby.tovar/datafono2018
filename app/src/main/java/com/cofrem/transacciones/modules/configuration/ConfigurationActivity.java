package com.cofrem.transacciones.modules.configuration;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.core.global.InfoGlobalValues;
import com.cofrem.transacciones.modules.configuration.comm.CommActivity;
import com.cofrem.transacciones.modules.configuration.printer.PrinterActivity;
import com.cofrem.transacciones.modules.configuration.register.RegisterActivity;

public class ConfigurationActivity
        extends BaseAppCompatActivity {
    //Control coordinador de elementos
    @BindView(R.id.clyConfiguration)
    CoordinatorLayout clyConfiguration;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        coordinatorBase = clyConfiguration;
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    @OnClick(R.id.btnConfigRegister)
    public void navigateToConfigRegister() {
        Bundle bundle = new Bundle();
        bundle.putInt(InfoGlobalValues.KEY_CONFIGURATION, InfoGlobalValues.configuracionRegistrar);
        bundle.putInt(InfoGlobalValues.KEY_PASO_PASS, InfoGlobalValues.configuracionPassServer);
        navigateTo(this, RegisterActivity.class, bundle);
    }

    @OnClick(R.id.btnConfigPrinter)
    public void navigateToPrinter() {
        navigateTo(this, PrinterActivity.class);
    }

    @OnClick(R.id.btnConfigCommTest)
    public void navigateToCommTest() {
        navigateTo(this, CommActivity.class);
    }

    @OnClick(R.id.btnConfigBack)
    public void navigateToMain() {
        super.navigateToMain(this);
    }
}