package com.cofrem.transacciones.modules.transaction.saldo;

import com.cofrem.transacciones.models.pojo.Saldo;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.saldo.event.SaldoEvent;

public interface SaldoContract {
    interface view {
        void onConsultarSaldoSuccess(Saldo[] saldos);

        void onConsultarSaldoError(String errorMessage);

        void onImprimirReciboSuccess();

        void onImprimirReciboError(String errorMessage);

        void onErrorConexion();
    }

    interface presenter {
        void onCreate();

        void onDestroy();

        void consultarSaldo(Transaccion transaccion);

        void imprimirRecibo(Transaccion transaccion, Saldo[] saldos);

        void onEventMainThread(SaldoEvent saldoEvent);

    }

    interface repository {
        void consultarSaldo(Transaccion transaccion);

        void imprimirRecibo(Transaccion transaccion, Saldo[] saldos);
    }
}
