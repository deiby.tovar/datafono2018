package com.cofrem.transacciones.modules.configuration.printer.event;

import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;

public class PrinterEvent {

    public enum Type {
        onVerifyConfigPrinterInitialSuccess,
        onVerifyConfigPrinterInitialError,
        onSaveConfigPrinterSuccess,
        onSaveConfigPrinterError,
        onPrintTestSuccess,
        onPrintTestError,
    }

    private Type type;

    private String errorMessage;

    private ConfigurationPrinter configurationPrinter;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ConfigurationPrinter getConfigurationPrinter() {
        return configurationPrinter;
    }

    public void setConfigurationPrinter(ConfigurationPrinter configurationPrinter) {
        this.configurationPrinter = configurationPrinter;
    }
}
