package com.cofrem.transacciones.modules.report.reimpresion;

import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.report.reimpresion.event.ReimpresionEvent;

public interface ReimpresionContract {
    interface view {
        void onVerifyExistenceUltimoReciboSuccess(Transaccion modelTransaccionOLD);

        void onVerifyExistenceUltimoReciboError();

        void onVerifyExistenceReciboPorNumCargoSuccess(Transaccion modelTransaccionOLD);

        void onVerifyExistenceReciboPorNumCargoError();

        void onVerifyClaveAdministradorSuccess();

        void onVerifyClaveAdministradorError();

        void onVerifyExistenceReporteDetalleSuccess();

        void onVerifyExistenceReporteDetalleError();

        void onImprimirUltimoReciboSuccess();

        void onImprimirUltimoReciboError(String error);

        void onImprimirReciboPorNumCargoSuccess();

        void onImprimirReciboPorNumCargoError(String error);

        void onImprimirReporteDetalleSuccess();

        void onImprimirReporteDetalleError(String error);

        void onImprimirReporteGeneralSuccess();

        void onImprimirReporteGeneralError(String error);

        void onCierreDeLoteSuccess();

        void onCierreDeLoteError(String error);

        void onTransaccionWSConexionError(String error);
    }

    interface presenter {
        void VerifySuccess();

        void onCreate();

        void onDestroy();

        void validarExistenciaUltimoRecibo();

        void imprimirUltimoRecibo();

        void validarExistenciaReciboConNumCargo(String numCargo);

        void imprimirReciboConNumCargo();

        void validarClaveAdministrador(String clave);

        void validarExistenciaDetalleRecibos();

        void imprimirReporteDetalle();

        void imprimirReporteGeneral();

        void cierreDeLote();

        void imprimirCierreLote();

        void onEventMainThread(ReimpresionEvent reimpresionEvent);
    }

    interface repository {
        void validateAcces();

        void validarExistenciaUltimoRecibo();

        void imprimirUltimoRecibo();

        void validarExistenciaReciboConNumCargo(String numCargo);

        void imprimirReciboConNumCargo();

        void validarClaveAdministrador(String clave);

        void validarExistenciaDetalleRecibos();

        void imprimirReporteDetalle();

        void imprimirReporteGeneral();

        void cierreDeLote();

        void imprimirCierreLote();
    }
}
