package com.cofrem.transacciones.modules.configuration.printer;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;
import com.cofrem.transacciones.modules.configuration.printer.event.PrinterEvent;
import org.greenrobot.eventbus.Subscribe;

public class PrinterPresenter implements PrinterContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */
    private PrinterContract.view view;
    private PrinterContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public PrinterPresenter(PrinterContract.view view) {
        this.view = view;
        this.repository = new PrinterRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    /**
     * metodo que se encarga de verifyDatos la existencia de la configuracion de la impresora
     */
    @Override
    public void verifyConfigPrinterInitial() {
        repository.verifyConfigPrinterInitial();
    }

    /**
     * metodo que se encarga guardar la configuracion de la impresora
     */
    @Override
    public void saveConfigPrinter(ConfigurationPrinter configurationPrinter) {
        repository.saveConfigPrinter(configurationPrinter);
    }

    @Override
    public void printTest(int gray) {
        repository.printTest(gray);
    }

    @Override
    @Subscribe
    public void onEventMainThread(PrinterEvent printerEvent) {
        switch (printerEvent.getType()) {
            case onVerifyConfigPrinterInitialSuccess:
                onVerifyConfigPrinterInitialSuccess(printerEvent.getConfigurationPrinter());
                break;
            case onVerifyConfigPrinterInitialError:
                onVerifyConfigPrinterInitialError();
                break;
            case onSaveConfigPrinterSuccess:
                onSaveConfigPrinterSuccess();
                break;
            case onSaveConfigPrinterError:
                onSaveConfigPrinterError();
                break;
            case onPrintTestSuccess:
                onPrintTestSuccess();
                break;
            case onPrintTestError:
                onPrintTestError(printerEvent.getErrorMessage());
                break;
        }
    }
    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    /**
     * @param configuration
     */
    private void onVerifyConfigPrinterInitialSuccess(ConfigurationPrinter configuration) {
        if (view != null) {
            view.onVerifyConfigPrinterInitialSuccess(configuration);
        }
    }

    private void onVerifyConfigPrinterInitialError() {
        if (view != null) {
            view.onVerifyConfigPrinterInitialError();
        }
    }

    /**
     * Metodo para manejar el insert de la configuracion exitosa
     */
    private void onSaveConfigPrinterSuccess() {
        if (view != null) {
            view.onSaveConfigPrinterSuccess();
        }
    }

    /**
     * Metodo para manejar el insert de la configuracion con error
     */
    private void onSaveConfigPrinterError() {
        if (view != null) {
            view.onSaveConfigPrinterError();
        }
    }

    /**
     * Metodo para manejar la impression de prueba exitosa
     */
    private void onPrintTestSuccess() {
        if (view != null) {
            view.onPrintTestSuccess();
        }
    }

    /**
     * Metodo para manejar la impression de prueba con error
     */
    private void onPrintTestError(String messageError) {
        if (view != null) {
            view.onPrintTestError(messageError);
        }
    }
}
