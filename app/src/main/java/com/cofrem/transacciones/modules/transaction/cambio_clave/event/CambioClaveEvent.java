package com.cofrem.transacciones.modules.transaction.cambio_clave.event;


public class CambioClaveEvent {

    public enum Type {
        onVerifyPassActualSuccess, onVerifyPassActualErrorPassIncorrecta,
        onVerifyPassActualErrorDocumentoIncorrecto,
        onVerifyPassActualErrorTarjetaInactiva,
        onVerifyPassActualErrorTarjetaInvalida, onVerifyPassNuevaSuccess,
        onVerifyPassNuevaErrorPassIncorrecta,
        onVerifyPassNuevaErrorDocumentoIncorrecto,
        onVerifyPassNuevaErrorPassFormatoIncorrecto,
        onVerifyPassNuevaErrorTarjetaInactiva,
        onVerifyPassNuevaErrorTarjetaInvalida, onErrorConexion,
    }

    private Type type;

    private String errorMessage;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
