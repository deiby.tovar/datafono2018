package com.cofrem.transacciones.modules.configuration.printer;

import com.cofrem.transacciones.models.pojo.ConfigurationPrinter;
import com.cofrem.transacciones.modules.configuration.printer.event.PrinterEvent;

public interface PrinterContract {

    interface view {
        void onVerifyConfigPrinterInitialSuccess(ConfigurationPrinter configuration);

        void onVerifyConfigPrinterInitialError();

        void onSaveConfigPrinterSuccess();

        void onSaveConfigPrinterError();

        void onPrintTestSuccess();

        void onPrintTestError(String messageError);
    }

    interface presenter {
        void onCreate();

        void onDestroy();

        void verifyConfigPrinterInitial();

        void saveConfigPrinter(ConfigurationPrinter configurationPrinter);

        void printTest(int gray);

        void onEventMainThread(PrinterEvent printerEvent);
    }

    interface repository {
        void verifyConfigPrinterInitial();

        void saveConfigPrinter(ConfigurationPrinter configurationPrinter);

        void printTest(int gray);
    }
}
