package com.cofrem.transacciones.modules.configuration.register;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.models.pojo.Configuracion;
import com.cofrem.transacciones.models.pojo.Terminal;
import com.cofrem.transacciones.modules.configuration.register.event.RegisterEvent;
import org.greenrobot.eventbus.Subscribe;

public class RegisterPresenter implements RegisterContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private RegisterContract.view view;

    private RegisterContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public RegisterPresenter(RegisterContract.view view) {
        this.view = view;
        this.repository = new RegisterRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    /**
     * Valida el acceso a la configuracion del dispositivo mediante contraseña
     *
     * @param pass
     */
    @Override
    public void verifyPassLocal(String pass) {
        repository.verifyPassLocal(pass);
    }

    /**
     * Valida el acceso a la configuracion del dispositivo mediante contraseña
     *
     * @param pass
     */
    @Override
    public void verifyPassServer(String pass) {
        repository.verifyPassServer(pass);
    }

    /**
     * Metodo encargado de validar la existencia de una terminal
     *
     * @param configuracion
     */
    @Override
    public void verifyTerminal(Configuracion configuracion) {
        repository.verifyTerminal(configuracion);
    }

    /**
     * Registra los parametros de conexion del dispositivo
     *
     * @param configuracion
     */
    @Override
    public void registerData(Configuracion configuracion, Terminal terminal) {
        repository.registerData(configuracion, terminal);
    }

    @Override
    @Subscribe
    public void onEventMainThread(RegisterEvent registerEvent) {
        switch (registerEvent.getType()) {
            case onVerifyPassLocalValida:
                onVerifyPassLocalValida();
                break;
            case onVerifyPassLocalNoValida:
                onVerifyPassLocalNoValida();
                break;
            case onVerifyPassServerValida:
                onVerifyPassServerValida();
                break;
            case onVerifyPassServerNoValida:
                onVerifyPassServerNoValida();
                break;
            case onVerifyTerminalSuccess:
                onVerifyTerminalSuccess(registerEvent.getTerminal());
                break;
            case onVerifyTerminalError:
                onVerifyTerminalError(registerEvent.getErrorMessage());
                break;
            case onRegisterDataServerSuccess:
                onRegisterDataServerSuccess();
                break;
            case onRegisterDataServerError:
                onRegisterDataServerError(registerEvent.getErrorMessage());
                break;
            case onRegisterDataDBError:
                onRegisterDataDBError(registerEvent.getErrorMessage());
                break;
            case onGetDataHeaderSuccess:
                onGetDataHeaderSuccess();
                break;
            case onGetDataHeaderError:
                onGetDataHeaderError();
                break;
            case onErrorConexion:
                onErrorConexion();
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void onVerifyPassLocalValida() {
        if (view != null) {
            view.onVerifyPassLocalValida();
        }
    }

    private void onVerifyPassLocalNoValida() {
        if (view != null) {
            view.onVerifyPassLocalNoValida();
        }
    }

    private void onVerifyPassServerValida() {
        if (view != null) {
            view.onVerifyPassServerValida();
        }
    }

    private void onVerifyPassServerNoValida() {
        if (view != null) {
            view.onVerifyPassServerNoValida();
        }
    }

    /**
     * @param terminal
     */
    private void onVerifyTerminalSuccess(Terminal terminal) {
        if (view != null) {
            view.onVerifyTerminalSuccess(terminal);
        }
    }

    /**
     * @param errorMessage
     */
    private void onVerifyTerminalError(String errorMessage) {
        if (view != null) {
            view.onVerifyTerminalError(errorMessage);
        }
    }

    private void onRegisterDataServerSuccess() {
        if (view != null) {
            view.onRegisterDataServerSuccess();
        }
    }

    /**
     * @param errorMessage
     */
    private void onRegisterDataServerError(String errorMessage) {
        if (view != null) {
            view.onRegisterDataServerError(errorMessage);
        }
    }

    /**
     * @param errorMessage
     */
    private void onRegisterDataDBError(String errorMessage) {
        if (view != null) {
            view.onRegisterDataDBError(errorMessage);
        }
    }

    private void onGetDataHeaderSuccess() {
        if (view != null) {
            view.onGetDataHeaderSuccess();
        }
    }

    private void onGetDataHeaderError() {
        if (view != null) {
            view.onGetDataHeaderError();
        }
    }

    private void onErrorConexion() {
        if (view != null) {
            view.onErrorConexion();
        }
    }
}
