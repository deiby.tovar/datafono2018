package com.cofrem.transacciones.modules.transaction.saldo;

import android.content.Context;
import com.cofrem.transacciones.core.lib.eventBus.EventBus;
import com.cofrem.transacciones.core.lib.eventBus.GreenRobotEventBus;
import com.cofrem.transacciones.models.pojo.Saldo;
import com.cofrem.transacciones.models.pojo.Transaccion;
import com.cofrem.transacciones.modules.transaction.saldo.event.SaldoEvent;
import org.greenrobot.eventbus.Subscribe;

public class SaldoPresenter implements SaldoContract.presenter {
    /**
     * #############################################################################################
     * Declaracion de controles y variables
     * #############################################################################################
     */
    //Declaracion del bus de eventos
    EventBus eventBus;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private SaldoContract.view view;

    private SaldoContract.repository repository;

    /**
     * #############################################################################################
     * Constructor de la clase
     * #############################################################################################
     *
     * @param view
     */
    public SaldoPresenter(SaldoContract.view view) {
        this.view = view;
        this.repository = new SaldoRepository((Context) view);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    @Override
    public void consultarSaldo(Transaccion transaccion) {
        repository.consultarSaldo(transaccion);
    }


    @Override
    public void imprimirRecibo(Transaccion transaccion, Saldo[] saldos) {
        repository.imprimirRecibo(transaccion, saldos);
    }

    @Override
    @Subscribe
    public void onEventMainThread(SaldoEvent saldoEvent) {
        switch (saldoEvent.getType()) {
            case onConsultarSaldoSuccess:
                onConsultarSaldoSuccess(saldoEvent.getSaldos());
                break;
            case onConsultarSaldoError:
                onConsultarSaldoError(saldoEvent.getErrorMessage());
                break;
            case onErrorConexion:
                onErrorConexion();
                break;
            case onImprecionReciboSuccess:
                onImprimirSuccess();
                break;
            case onImprecionReciboError:
                onImprimirError(saldoEvent.getErrorMessage());
                break;
        }
    }

    /**
     * #############################################################################################
     * Metodo propios de la clase
     * #############################################################################################
     */
    private void onConsultarSaldoSuccess(Saldo[] saldos) {
        if (view != null) {
            view.onConsultarSaldoSuccess(saldos);
        }
    }

    private void onConsultarSaldoError(String errorMessage) {
        if (view != null) {
            view.onConsultarSaldoError(errorMessage);
        }
    }

    private void onErrorConexion() {
        if (view != null) {
            view.onErrorConexion();
        }
    }

    private void onImprimirSuccess() {
        if (view != null) {
            view.onImprimirReciboSuccess();
        }
    }

    private void onImprimirError(String errorMessage) {
        if (view != null) {
            view.onImprimirReciboError(errorMessage);
        }
    }
}