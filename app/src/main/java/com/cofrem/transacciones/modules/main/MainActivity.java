package com.cofrem.transacciones.modules.main;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.modules.configuration.ConfigurationActivity;
import com.cofrem.transacciones.modules.report.ReportActivity;
import com.cofrem.transacciones.modules.transaction.TransactionActivity;

public class MainActivity
        extends BaseAppCompatActivity {
    //Control coordinador de elementos
    @BindView(R.id.clyMain)
    CoordinatorLayout clyMain;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coordinatorBase = clyMain;
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados del sistema
     * #############################################################################################
     */
    /**
     * Metodo que interfiere la presion del boton "Back"
     */
    @Override
    public void onBackPressed() {
        //NO ACTION
    }

    /**
     * Metodo para navegar a la ventana de transacciones
     */
    @OnClick(R.id.btnMainTransaction)
    public void navigateToTransaction() {
        navigateTo(this, TransactionActivity.class);
    }

    /**
     * Metodo para navegar a la ventana de reportes
     */
    @OnClick(R.id.btnMainReport)
    public void navigateToReport() {
        navigateTo(this, ReportActivity.class);
    }

    /**
     * Metodo para navegar a la ventana de configuraciones
     */
    @OnClick(R.id.btnMainConfiguration)
    public void navigateToConfigurationScreen() {
        navigateTo(this, ConfigurationActivity.class);
    }
}