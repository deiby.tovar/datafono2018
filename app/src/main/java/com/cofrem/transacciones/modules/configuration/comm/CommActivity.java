package com.cofrem.transacciones.modules.configuration.comm;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.cofrem.transacciones.R;
import com.cofrem.transacciones.core.Activity.BaseAppCompatActivity;
import com.cofrem.transacciones.modules.configuration.ConfigurationActivity;

public class CommActivity
        extends BaseAppCompatActivity
        implements CommContract.view {
    /**
 * #############################################################################################
 * Declaracion de controles y variables
 * #############################################################################################
 */
    /**
     * Declaracion de los Contoles
     */
    @BindView(R.id.clyConfigurationTest)
    CoordinatorLayout clyConfigurationTest;
    @BindView(R.id.txvConfigTestResult)
    TextView txvTestResult;
    /**
     * #############################################################################################
     * Instanciamientos de las clases
     * #############################################################################################
     */

    private CommContract.presenter presenter;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_module_test);
        coordinatorBase = clyConfigurationTest;
    //Instanciamiento e inicializacion del presentador
        presenter = new CommPresenter(this);
    //Llamada al metodo onCreate del presentador para el registro del bus de datos
        presenter.onCreate();
        presenter.commTest();
    }
    /**
     * #############################################################################################
     * Metodos sobrecargados del sistema
     * #############################################################################################
     */
    /**
     * Metodo sobrecargado de la vista para la destruccion del activity
     */
    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /**
     * #############################################################################################
     * Metodos sobrecargados de la interface
     * #############################################################################################
     */
    @Override
    public void onTestComunicationSuccess() {
        txvTestResult.setText(R.string.configuration_text_exitosa);
    }

    /**
     * @param error
     */
    @Override
    public void onTestComunicationError(final String error) {
        txvTestResult.setText(error);
    }

    @OnClick(R.id.btnConfigTestSalir)
    void navigateToConfigurationScreen() {
        navigateTo(this, ConfigurationActivity.class);
    }
}
